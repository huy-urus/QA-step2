
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.FDP.FRAC.ADD.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.NA.FDP.FRAC.ADD.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": ""
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "x2",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[2, 4, 8]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "x1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "x2-1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y2",
                "value": {
                  "#type": "expression",
                  "value": "x2"
                }
              },
              {
                "#type": "variable",
                "name": "y1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "y2-1"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "x1*y2+x2*y1 >= x2*y2"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "x2",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "[2, 3, 4, 5, 6, 8, 10, 12]"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "x1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "x2-1"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "y2",
                    "value": {
                      "#type": "expression",
                      "value": "x2"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "y1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "y2-1"
                      }
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "x1*y2+x2*y1 >= x2*y2"
                }
              }
            ],
            "else": [
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "x2",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "[2, 3, 4, 5, 6, 8, 10, 12, 15, 30, 60, 100]"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "x1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "x2-1"
                      }
                    }
                  },
                  {
                    "#type": "do_while_block",
                    "do": [
                      {
                        "#type": "variable",
                        "name": "y2",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "expression",
                            "value": "[2, 3, 4, 5, 6, 8, 10, 12, 15, 30, 60, 100]"
                          }
                        }
                      }
                    ],
                    "while": {
                      "#type": "expression",
                      "value": "y2 == x2"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "y1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "y2-1"
                      }
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "x1*y2+x2*y1 >= x2*y2"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function UCLN(a, b){\n    var max, min, temp;\n    if(a > b) {max =a; min =b;}\n    else {max =b; min =a;}\n    while(max != min) {\n        temp = max - min;\n        if(temp >= min){\n            max =temp;\n        }\n        else {\n            max = min;\n            min = temp;\n        }\n    }\n    return max;\n}\n\nvar n1 = (x1*y2 + x2*y1) / (UCLN(x1*y2 + x2*y1, x2*y2));\nvar n2 = (x2*y2) / (UCLN(x1*y2 + x2*y1, x2*y2));"
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Complete the addition of the fractions as a SIMPLIFIED fraction."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "5"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "700"
            },
            "height": {
              "#type": "expression",
              "value": "140"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "[1, 2, 3, 4, 5]"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "3"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "140"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "[0, 1, 2]"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 0"
                        },
                        "then": [
                          {
                            "#type": "list_shape",
                            "#props": [
                              {
                                "#type": "prop_list_direction",
                                "#prop": "",
                                "dir": "horizontal"
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              }
                            ],
                            "items": [
                              {
                                "#type": "json",
                                "#props": [
                                  {
                                    "#type": "prop_list_align",
                                    "#prop": "",
                                    "align": "middle"
                                  },
                                  {
                                    "#type": "prop_list_item_source",
                                    "#prop": "",
                                    "source": {
                                      "#type": "func",
                                      "name": "charactersOf",
                                      "args": [
                                        {
                                          "#type": "expression",
                                          "value": "x1"
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "template": {
                                  "#callback": "$item",
                                  "variable": "$item",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 1"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "line.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}line.png"
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 2"
                        },
                        "then": [
                          {
                            "#type": "list_shape",
                            "#props": [
                              {
                                "#type": "prop_list_direction",
                                "#prop": "",
                                "dir": "horizontal"
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              }
                            ],
                            "items": [
                              {
                                "#type": "json",
                                "#props": [
                                  {
                                    "#type": "prop_list_align",
                                    "#prop": "",
                                    "align": "middle"
                                  },
                                  {
                                    "#type": "prop_list_item_source",
                                    "#prop": "",
                                    "source": {
                                      "#type": "func",
                                      "name": "charactersOf",
                                      "args": [
                                        {
                                          "#type": "expression",
                                          "value": "x2"
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "template": {
                                  "#callback": "$item",
                                  "variable": "$item",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 1",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "plus.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}plus.png"
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 2",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "3"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "140"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "[0, 1, 2]"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 0"
                        },
                        "then": [
                          {
                            "#type": "list_shape",
                            "#props": [
                              {
                                "#type": "prop_list_direction",
                                "#prop": "",
                                "dir": "horizontal"
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              }
                            ],
                            "items": [
                              {
                                "#type": "json",
                                "#props": [
                                  {
                                    "#type": "prop_list_align",
                                    "#prop": "",
                                    "align": "middle"
                                  },
                                  {
                                    "#type": "prop_list_item_source",
                                    "#prop": "",
                                    "source": {
                                      "#type": "func",
                                      "name": "charactersOf",
                                      "args": [
                                        {
                                          "#type": "expression",
                                          "value": "y1"
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "template": {
                                  "#callback": "$item",
                                  "variable": "$item",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 1"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "line.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}line.png"
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 2"
                        },
                        "then": [
                          {
                            "#type": "list_shape",
                            "#props": [
                              {
                                "#type": "prop_list_direction",
                                "#prop": "",
                                "dir": "horizontal"
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              }
                            ],
                            "items": [
                              {
                                "#type": "json",
                                "#props": [
                                  {
                                    "#type": "prop_list_align",
                                    "#prop": "",
                                    "align": "middle"
                                  },
                                  {
                                    "#type": "prop_list_item_source",
                                    "#prop": "",
                                    "source": {
                                      "#type": "func",
                                      "name": "charactersOf",
                                      "args": [
                                        {
                                          "#type": "expression",
                                          "value": "y2"
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "template": {
                                  "#callback": "$item",
                                  "variable": "$item",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 3",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "equal.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}equal.png"
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 4",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "3"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "140"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "[0, 1, 2]"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 0"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "shape.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}shape.png"
                              }
                            ]
                          },
                          {
                            "#type": "choice_input_shape",
                            "#props": [
                              {
                                "#type": "prop_value",
                                "#prop": "",
                                "value": {
                                  "#type": "expression",
                                  "value": "n1"
                                }
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_size",
                                "#prop": "",
                                "width": {
                                  "#type": "expression",
                                  "value": "106"
                                },
                                "height": {
                                  "#type": "expression",
                                  "value": "61"
                                }
                              },
                              {
                                "#type": "prop_input_keyboard",
                                "#prop": "",
                                "keyboard": "numbers1"
                              },
                              {
                                "#type": "prop_input_max_length",
                                "#prop": "",
                                "maxLength": {
                                  "#type": "expression",
                                  "value": "n1.toString().length"
                                }
                              },
                              {
                                "#type": "prop_input_result_position",
                                "#prop": "",
                                "resultPosition": "right"
                              },
                              {
                                "#type": "prop_tab_order",
                                "#prop": "",
                                "tabOrder": {
                                  "#type": "expression",
                                  "value": "0"
                                }
                              },
                              {
                                "#type": "prop_stroke",
                                "#prop": "stroke"
                              },
                              {
                                "#type": "prop_fill",
                                "#prop": "fill"
                              },
                              {
                                "#prop": "",
                                "style": {
                                  "#type": "json",
                                  "base": "text",
                                  "#props": [
                                    {
                                      "#type": "prop_text_style_font_size",
                                      "#prop": "",
                                      "fontSize": {
                                        "#type": "expression",
                                        "value": "36"
                                      }
                                    },
                                    {
                                      "#type": "prop_text_style_fill",
                                      "#prop": "",
                                      "fill": "black"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke",
                                      "#prop": "",
                                      "stroke": "white"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke_thickness",
                                      "#prop": "",
                                      "strokeThickness": {
                                        "#type": "expression",
                                        "value": "2"
                                      }
                                    }
                                  ]
                                }
                              }
                            ],
                            "#init": "algorithmic_input"
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 1"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "line.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}line.png"
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "if_then_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.row == 2"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "shape.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}shape.png"
                              }
                            ]
                          },
                          {
                            "#type": "choice_input_shape",
                            "#props": [
                              {
                                "#type": "prop_value",
                                "#prop": "",
                                "value": {
                                  "#type": "expression",
                                  "value": "n2"
                                }
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_size",
                                "#prop": "",
                                "width": {
                                  "#type": "expression",
                                  "value": "106"
                                },
                                "height": {
                                  "#type": "expression",
                                  "value": "61"
                                }
                              },
                              {
                                "#type": "prop_input_keyboard",
                                "#prop": "",
                                "keyboard": "numbers1"
                              },
                              {
                                "#type": "prop_input_max_length",
                                "#prop": "",
                                "maxLength": {
                                  "#type": "expression",
                                  "value": "n2.toString().length"
                                }
                              },
                              {
                                "#type": "prop_input_result_position",
                                "#prop": "",
                                "resultPosition": "right"
                              },
                              {
                                "#type": "prop_tab_order",
                                "#prop": "",
                                "tabOrder": {
                                  "#type": "expression",
                                  "value": "0"
                                }
                              },
                              {
                                "#type": "prop_stroke",
                                "#prop": "stroke"
                              },
                              {
                                "#type": "prop_fill",
                                "#prop": "fill"
                              },
                              {
                                "#prop": "",
                                "style": {
                                  "#type": "json",
                                  "base": "text",
                                  "#props": [
                                    {
                                      "#type": "prop_text_style_font_size",
                                      "#prop": "",
                                      "fontSize": {
                                        "#type": "expression",
                                        "value": "36"
                                      }
                                    },
                                    {
                                      "#type": "prop_text_style_fill",
                                      "#prop": "",
                                      "fill": "black"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke",
                                      "#prop": "",
                                      "stroke": "white"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke_thickness",
                                      "#prop": "",
                                      "strokeThickness": {
                                        "#type": "expression",
                                        "value": "2"
                                      }
                                    }
                                  ]
                                }
                              }
                            ],
                            "#init": "algorithmic_input"
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Familiarise yourself with adding fractions.</u></br></br>\n<style>\n  #ALL {width: 950px; background: rgb(150, 200, 250, 0.3)}\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n<center>\n  <table id='ALL'>\n  <tr>\n  <td id='TD'>Example 1:</br>\n<table style='margin: auto;'><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>7</td></tr><tr><td style='color: green'>10</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: orange'>1</td></tr><tr><td style='color: green'>5</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>7</td></tr><tr><td style='color:'>10</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: orange'>2</td></tr><tr><td style='color:'>10</td></tr></table>\n</td></tr><tr><td></td><td></td><td>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>9</td></tr><tr><td style='color:'>10</td></tr></table>\n</td></tr></table>\n  </td>\n\n  <td id='TD'>Example 2:</br>\n<table style='margin: auto'><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: red'>1</td></tr><tr><td style='color: blue'>3</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: red'>1</td></tr><tr><td style='color: blue'>2</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: red'>2</td></tr><tr><td style='color:'>6</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color: red'>3</td></tr><tr><td style='color:'>6</td></tr></table>\n</td></tr><tr><td></td><td></td><td>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>5</td></tr><tr><td style='color:'>6</td></tr></table>\n</td></tr></table>\n  </td>\n</tr>\n<tr>\n  <td id='TD'><p style='color: green; display: inline-block'>Hint</p>: What is the lowest common multiple of denominator 10 and 5?</br>\n    <p style='color: orange; display: inline-block'>Note</p>: Numerators 1 has been changed to 2 to show equivalent fractions.\n  </td>\n\n<td id='TD'><p style='color: blue; display: inline-block'>Hint</p>: What is the lowest common multiple of denominator 3 and 2?</br>\n    <p style='color: red; display: inline-block'>Note</p>: Numerators 1 and 1 have been changed to 2 and 3 to show equivalent fractions.\n  </td>\n</tr>\n</table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 4"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Let's do fraction addition.</u></br></br>\n\n<style>\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n\n<table style=''><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1}</td></tr><tr><td style='color:'>${x2}</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${y1}</td></tr><tr><td style='color:'>${y2}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1+y1}</td></tr><tr><td style='color:'>${x2}</td></tr></table>\n</td>\n</tr></table>\n\n<table style=''><tr><td>Is</td><td><table id='PS'><tr><td style='border-bottom: 3px solid black; color:'><b>${x1+y1}</b></td></tr><tr><td style='color:'><b>${x2}</b></td></tr></table></td><td>a simplified fraction?</td>\n  </tr></table>\n\n${UCLN(x1+y1, x2) == 1? 'Yes':'No'}."
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "UCLN(x1+y1, x2) != 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 3: Simplify the fraction.</u></br></br>\n\n<style>\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n\n<table style=''><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1+y1}</td></tr><tr><td style='color:'>${x2}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1+y1} &divide; ${UCLN(x1+y1, x2)}</td></tr><tr><td style='color:'>${y2} &divide; ${UCLN(x1+y1, x2)}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${n1}</td></tr><tr><td style='color:'>${n2}</td></tr></table>\n</td>\n</tr></table>\n\n<table style=''><tr><td>Is</td><td><table id='PS'><tr><td style='border-bottom: 3px solid black; color:'><b>${n1}</b></td></tr><tr><td style='color:'><b>${n2}</b></td></tr></table></td><td>a simplified fraction?</td>\n  </tr></table>\n\nYes."
                ]
              },
              {
                "#type": "func",
                "name": "endPartialExplanation",
                "args": []
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Identify the lowest common multiple of the denominators in both fractions.</u></br></br>\n\nThe lowest common multiple of ${x2} and ${y2} is: <b>${x2*y2/UCLN(x2, y2)}</b>."
            ]
          },
          {
            "#type": "variable",
            "name": "BCNN",
            "value": {
              "#type": "expression",
              "value": "x2*y2/UCLN(x2, y2)"
            }
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 3: Convert both fractions to common denominator ${BCNN}.</u></br></br>\n*Do not forget to multiply the numerator too*\n</br>\n\n<style>\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n\n<table style=''><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1} x ${BCNN/x2}</td></tr><tr><td style='color:'>${x2} x ${BCNN/x2}</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${y1} x ${BCNN/y2}</td></tr><tr><td style='color:'>${y2} x ${BCNN/y2}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1 * BCNN / x2}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${y1 * BCNN / y2}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</td>\n</tr></table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 4: Finally, add both fractions together.</u></br>\n\n<style>\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n\n<table style=''><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1} x ${BCNN/x2}</td></tr><tr><td style='color:'>${x2} x ${BCNN/x2}</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${y1} x ${BCNN/y2}</td></tr><tr><td style='color:'>${y2} x ${BCNN/y2}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${x1 * BCNN / x2}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</td><td>+</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${y1 * BCNN / y2}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</td></tr><tr><td></td><td></td><td>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${(x1 * BCNN / x2) + (y1 * BCNN / y2)}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</tr></table>\n  \n  <table style=''><tr><td>Is</td><td><table id='PS'><tr><td style='border-bottom: 3px solid black; color:'><b>${(x1 * BCNN / x2) + (y1 * BCNN / y2)}</b></td></tr><tr><td style='color:'><b>${BCNN}</b></td></tr></table></td><td>a simplified fraction?</td>\n  </tr></table>\n\n${UCLN((x1 * BCNN / x2) + (y1 * BCNN / y2), BCNN) == 1? 'Yes':'No'}."
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "UCLN((x1 * BCNN / x2) + (y1 * BCNN / y2), BCNN) != 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 5: Simplify the fraction.</u></br>\n\n<style>\n  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}\n  #TD {text-align: center; border: 1px solid black; padding: 5px;}\n  </style>\n\n<table style=''><tr><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${(x1 * BCNN / x2) + (y1 * BCNN / y2)}</td></tr><tr><td style='color:'>${BCNN}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${(x1 * BCNN / x2) + (y1 * BCNN / y2)} &divide; ${UCLN((x1 * BCNN / x2) + (y1 * BCNN / y2), BCNN)}</td></tr><tr><td style='color:'>${BCNN} &divide; ${UCLN((x1 * BCNN / x2) + (y1 * BCNN / y2), BCNN)}</td></tr></table>\n</td><td>=</td><td>\n<table id='PS'><tr><td style='border-bottom: 2px solid black; color:'>${n1}</td></tr><tr><td style='color:'>${n2}</td></tr></table>\n</td>\n</tr></table>\n\n<table style=''><tr><td>Is</td><td><table id='PS'><tr><td style='border-bottom: 3px solid black; color:'><b>${n1}</b></td></tr><tr><td style='color:'><b>${n2}</b></td></tr></table></td><td>a simplified fraction?</td>\n  </tr></table>\n\nYes."
                ]
              },
              {
                "#type": "func",
                "name": "endPartialExplanation",
                "args": []
              }
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="218" y="106">
    <field name="name">Y6.NA.FDP.FRAC.ADD.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.NA.FDP.FRAC.ADD.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="variable" id="weElLx3.8SB.)STr377[">
                        <field name="name">loadAssets</field>
                        <value name="value">
                          <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                            <field name="link">${image_path}</field>
                            <field name="images"></field>
                          </block>
                        </value>
                        <next>
                          <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                            <statement name="#props">
                              <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                                <field name="#prop"></field>
                                <value name="key">
                                  <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                    <field name="value">bg${drop_background}.png</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                    <field name="#prop"></field>
                                    <value name="src">
                                      <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                        <field name="value">${background_path}bg${drop_background}.png</field>
                                      </block>
                                    </value>
                                  </block>
                                </next>
                              </block>
                            </statement>
                            <next>
                              <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                                <field name="name">numberOfCorrect</field>
                                <value name="value">
                                  <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                    <field name="name">numberOfIncorrect</field>
                                    <value name="value">
                                      <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                        <field name="value">0</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                        <field name="name">range</field>
                                        <value name="value">
                                          <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                            <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="@1ZFBBTnIU41VLYd%sr)p">
                                            <value name="if">
                                              <block type="expression" id="8Xli+l{mXhGQ@1qSU?yMV">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="do_while_block" id="9eNuel^;c(dMzwc9Hgv#">
                                                <statement name="do">
                                                  <block type="variable" id="J_J$Q$xO=5#SMP}cGZyI">
                                                    <field name="name">x2</field>
                                                    <value name="value">
                                                      <block type="random_one" id="xOD^T-ZE$hPQCS=3+Uv]">
                                                        <value name="items">
                                                          <block type="expression" id="9`va7oj2(Q0?oOd$wpMu">
                                                            <field name="value">[2, 4, 8]</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="gQ5MQ(hBT!L8+obBRN~A">
                                                        <field name="name">x1</field>
                                                        <value name="value">
                                                          <block type="random_number" id="^o8Q^HT`IjcF,?v:w{f:">
                                                            <value name="min">
                                                              <block type="expression" id="~PULAiEB%#Oy2/`+mX+B">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="_]2q0Qr6ax2C@1Ctn^c(r">
                                                                <field name="value">x2-1</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="B7?(pf1V%o1ntY`{F|vz">
                                                            <field name="name">y2</field>
                                                            <value name="value">
                                                              <block type="expression" id="]C;X2k$D]rV@2C.1$07Gw">
                                                                <field name="value">x2</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="-@2/50rN0+|^y$^Ahyhpo">
                                                                <field name="name">y1</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="o)Ld^nxb$;0?F3-+u,Ak">
                                                                    <value name="min">
                                                                      <block type="expression" id="f@1XyX8/^r|@1H935/-R`#">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="DDEm;^}$p9Jl18@1i?u@2a">
                                                                        <field name="value">y2-1</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <value name="while">
                                                  <block type="expression" id="D,?+#w,v#$WMqdZ0=YGd">
                                                    <field name="value">x1@2y2+x2@2y1 &gt;= x2@2y2</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id="HCF8G(SSF9U~00FiSu28">
                                                <value name="if">
                                                  <block type="expression" id="!`-]D+qULp-qrot{@2$)7">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="do_while_block" id="q=0XXbc_tf)q6(1P^-0M">
                                                    <statement name="do">
                                                      <block type="variable" id="e}.)fCS}ri%q(}v~Pzn/">
                                                        <field name="name">x2</field>
                                                        <value name="value">
                                                          <block type="random_one" id="~wZ5!Kc(+9ZZ8$Y]-EdI">
                                                            <value name="items">
                                                              <block type="expression" id="/BkTIp%.0]pSusI:UY5t">
                                                                <field name="value">[2, 3, 4, 5, 6, 8, 10, 12]</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="m)6k4D9mqCMp),G1r^ve">
                                                            <field name="name">x1</field>
                                                            <value name="value">
                                                              <block type="random_number" id="EoLY[nl8l?AG(B3r:7lY">
                                                                <value name="min">
                                                                  <block type="expression" id="kiuwyP=AP2YvjjE86Uh=">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id=")Vt$o6eLqFl@19I.7)KGc">
                                                                    <field name="value">x2-1</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="PM!^vzVp44_+=f?Bs+jL">
                                                                <field name="name">y2</field>
                                                                <value name="value">
                                                                  <block type="expression" id="mP7M_XcSp)mHSY2Op]Ac">
                                                                    <field name="value">x2</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="RVpyO[I(6_Hc-Gnm.}?{">
                                                                    <field name="name">y1</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="fbr4gTg^EYqM@2MByLFAM">
                                                                        <value name="min">
                                                                          <block type="expression" id="}gyjs!{x,P=[fLi{UM-)">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="j7iw;{|627@1Pdvr}Ryyi">
                                                                            <field name="value">y2-1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <value name="while">
                                                      <block type="expression" id="dt9.)sfS+/v;B{v:Y%:~">
                                                        <field name="value">x1@2y2+x2@2y1 &gt;= x2@2y2</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="do_while_block" id="y.LBqk~vocmaC.5O(_%9">
                                                    <statement name="do">
                                                      <block type="variable" id="_/K/Mq9u/O%MdVE.)E-C">
                                                        <field name="name">x2</field>
                                                        <value name="value">
                                                          <block type="random_one" id="/Znv7}AT%^EOZ@1mFj`q7">
                                                            <value name="items">
                                                              <block type="expression" id="11/sxjU53Qb4LnvWT!YN">
                                                                <field name="value">[2, 3, 4, 5, 6, 8, 10, 12, 15, 30, 60, 100]</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="PH$AI/pfst_RJJb:/;@1;">
                                                            <field name="name">x1</field>
                                                            <value name="value">
                                                              <block type="random_number" id="ylwro95Zgb7-vug032X=">
                                                                <value name="min">
                                                                  <block type="expression" id="Sb:;$riKzpRTIT@26COrx">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="NbNgJr:nDnROw`HXDPv7">
                                                                    <field name="value">x2-1</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="do_while_block" id="9OsZFYlw5hS.FC7[])~3">
                                                                <statement name="do">
                                                                  <block type="variable" id="9ewkKd9ly^Ka]0zgk6O/">
                                                                    <field name="name">y2</field>
                                                                    <value name="value">
                                                                      <block type="random_one" id="Ld?@2Ve{f=;^~A:T0Qv.C">
                                                                        <value name="items">
                                                                          <block type="expression" id="AAs0i)KxPoylN03b7RB0">
                                                                            <field name="value">[2, 3, 4, 5, 6, 8, 10, 12, 15, 30, 60, 100]</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </statement>
                                                                <value name="while">
                                                                  <block type="expression" id="kPn^^@18,7GZoejYq{~[F">
                                                                    <field name="value">y2 == x2</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="[ef:FHo~--X/C~0=6r9~">
                                                                    <field name="name">y1</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="][XZJaF2t%B/{#Todizi">
                                                                        <value name="min">
                                                                          <block type="expression" id="LeeC_fxd6B$~HxWE:J@1w">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="djNh~7^@2,j3/g#B8#m_)">
                                                                            <field name="value">y2-1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <value name="while">
                                                      <block type="expression" id=",l$c]SaN^Qso}p~0pqOZ">
                                                        <field name="value">x1@2y2+x2@2y1 &gt;= x2@2y2</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="statement" id="6gY%6iyDm1c#^}s;V}xr">
                                                <field name="value">function UCLN(a, b){
    var max, min, temp;
    if(a &gt; b) {max =a; min =b;}
    else {max =b; min =a;}
    while(max != min) {
        temp = max - min;
        if(temp &gt;= min){
            max =temp;
        }
        else {
            max = min;
            min = temp;
        }
    }
    return max;
}

var n1 = (x1@2y2 + x2@2y1) / (UCLN(x1@2y2 + x2@2y1, x2@2y2));
var n2 = (x2@2y2) / (UCLN(x1@2y2 + x2@2y1, x2@2y2));
                                                </field>
                                                <next>
                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                    <statement name="#props">
                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                        <field name="#prop"></field>
                                                        <value name="x">
                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                            <field name="value">400</field>
                                                          </block>
                                                        </value>
                                                        <value name="y">
                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                            <field name="#prop">anchor</field>
                                                            <value name="x">
                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                <field name="value">0.5</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                <field name="#prop"></field>
                                                                <value name="contents">
                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                    <field name="value">Complete the addition of the fractions as a SIMPLIFIED fraction.</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                    <field name="base">text</field>
                                                                    <statement name="#props">
                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                        <field name="#prop"></field>
                                                                        <value name="fontSize">
                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                            <field name="value">36</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                            <field name="#prop"></field>
                                                                            <value name="fill">
                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                <field name="value">black</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                <field name="#prop"></field>
                                                                                <value name="stroke">
                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                    <field name="value">white</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="strokeThickness">
                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                        <field name="value">2</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="grid_shape" id=".$]up;g.RdV@2{v3`pW3}">
                                                        <statement name="#props">
                                                          <block type="prop_grid_dimension" id="Xv/0_Pkk6-48y#@2~Qkjn">
                                                            <field name="#prop"></field>
                                                            <value name="rows">
                                                              <block type="expression" id="F@2$1@2j4Hyuh3QLy;GP6G">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="cols">
                                                              <block type="expression" id="xd3`D2cC.RymZMZdy6d1">
                                                                <field name="value">5</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                    <field name="value">220</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="sfI;e#%l;3mZUGiL6^Pi">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id="GHZ]T)k#OypH$nEj0Y1q">
                                                                        <field name="value">700</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="?m`9JO-K@2yQIJ]ruD!]A">
                                                                        <field name="value">140</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="|;{5)0(y,y1_2~t(@1/(e">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="6t3SD(t4FMn9FO0%nJC:">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="|(B@1gV`^uk%1vHjZ]BWk">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_grid_cell_source" id="svM542u;BufZUHO~l2uc">
                                                                            <field name="#prop">cell.source</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="odoO?V2Do@1b-B?D[W=">
                                                                                <field name="value">[1, 2, 3, 4, 5]</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_grid_random" id=";MoMf59z/2b.Sr5RoU8{">
                                                                                <field name="#prop"></field>
                                                                                <field name="random">FALSE</field>
                                                                                <next>
                                                                                  <block type="prop_grid_show_borders" id="tmib/xh6?[L@18xn3!.]@1">
                                                                                    <field name="#prop">#showBorders</field>
                                                                                    <field name="value">FALSE</field>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_template_for" id="f@1S8cJDn?`/1d6KzCBx+">
                                                                                        <field name="variable">$cell</field>
                                                                                        <field name="condition">$cell.col == 0</field>
                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                        <field name="#callback">$cell</field>
                                                                                        <statement name="body">
                                                                                          <block type="grid_shape" id="za%@2U7,7Q;+!mmq$xs5v">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_grid_dimension" id="z#g206JXIEHK_pA:X6_u">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="rows">
                                                                                                  <block type="expression" id="~Nmr/`%O|F%o[Xgz{2}k">
                                                                                                    <field name="value">3</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="cols">
                                                                                                  <block type="expression" id="},$ECmKt(m2II8%fSn/A">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="fH$!w9O;mQ4kQ7TiER8=">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="O}d`Q.kL]#@2#/+T4%Y?P">
                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="?r}uXF2Yifdy-#N%2pEc">
                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="+f1yf_b?!ga.VB_:4Vb/">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="Uy+n+c,/vrr=?{2)gceg">
                                                                                                            <field name="value">100</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="EUuv@1R]upgIxxY9407E/">
                                                                                                            <field name="value">140</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="@2Ej+c2G(Dm]k25RZ@1:d}">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="h^nD)YJ+blcBhn17dL0#">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="8}U+h+MZ?#%t.0v0a9HI">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_source" id="(@2yBq^w@2)Zu@2qyUpoywb">
                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="y3fE]b6qWDxGCgOD.!rq">
                                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_random" id="r8mS88os`@2E7|z`?aOuK">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="random">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_show_borders" id=")td6Cm,~@2@1,8zA]xf3[3">
                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                        <field name="value">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_template" id="rxG]Mw1=p6sIF5n/g(0L">
                                                                                                                            <field name="variable">$cell</field>
                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                            <statement name="body">
                                                                                                                              <block type="if_then_block" id="@2iU=7wF6=qCV.DstZ6j3">
                                                                                                                                <value name="if">
                                                                                                                                  <block type="expression" id="fuNDuR+0uLv#.i26VW(U">
                                                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="then">
                                                                                                                                  <block type="list_shape" id="mnaF-Nz)IMs~aL/vNTGk">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_list_direction" id="e_vJTdY!@2?q2.KC?@1c}L">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_position" id="$_X4@2`Wj9zY)drt0d0C{">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="ekIo8H,:`87D/9w:ih}m">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="?;x1c#f{-!Xp$X_6i%G!">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id="P0;kqkOd4[RITn4~}!X9">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="`w{V=nl)|IW@1up=A[|30">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="^/kVrvhs5z{kBD@2=%)(u">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <statement name="items">
                                                                                                                                      <block type="list_item_shape" id="O}8(TGk}#oTGsc19dgf[">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_list_align" id="$9{:WL6T~Ya7Y~|m/Zm)">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="align">middle</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_list_item_source" id="0E-4RiRaOU$Y`6r@2847v">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="source">
                                                                                                                                                  <block type="func_characters_of" id="5_fF@2I1Nj^-S`~UGCh@1:" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="}L+)[LLKMbpN;slb]z8e">
                                                                                                                                                        <field name="value">x1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <statement name="template">
                                                                                                                                          <block type="image_shape" id="!Zh;F$Dt|cvdOMmoDUHT">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="5CL_.]R7=vKMJ`C,%mpr">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="uRA|Bhf]sLFq,sx=b:14">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="0yIUVvAz.[)qy?7sQu#h">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id="bNJWii:[@2udF(sPZ-=h1">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="R^B_TwVgQ3wfwOy!?XUo">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id=")=5E(mt5$EIxHG5q55-N">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="0l6@1u;=47._$f{Sr{V;?">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="OfBe)Un1($mW8{gj!Y}T">
                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="[gNQ}[T$Q+.pGasHZ#d3">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="DB)RcScpdbFR5FDR|m9U">
                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="if_then_block" id="$hgIlm4uopZeIQ`%u:^Q">
                                                                                                                                    <value name="if">
                                                                                                                                      <block type="expression" id="6`VsxT}nr8)T_,k%7.-U">
                                                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="then">
                                                                                                                                      <block type="image_shape" id="EY9/@2^dRtZz@24sjeZ@28?">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="(ADWm+A?(Z]G`45v|j2U">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="tZSBM$)3^J|E!`,hU7?y">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="p|nbvX4echM!v@2`nJWen">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="(+{YwIS/q[;/6.$+Ai5t">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="VMcXXxB/}9q(i;tdPz+q">
                                                                                                                                                    <field name="value">line.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="uO7@1+o/Mc?6YVRsk@2@25h">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="p@2-?~sJa@1~7jv+t%|Xs+">
                                                                                                                                                        <field name="value">${image_path}line.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="if_then_block" id="OShS{EhObYlsM0LAR;zN">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="A_zwL)ft5-wM2/Op-rh;">
                                                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="list_shape" id="E0aR2wSlp0JaQ^2@2KU1s">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_list_direction" id="{DJ4~_H3JvSfU@2;Ve++_">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="H5+%u$Blfp.G#ZPg}@1F.">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="rsiHLQw@1:76u;E4GeM@2-">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="a[htQF..3e-#7ck{uF2-">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_anchor" id="Yum)Xr=jMAd;@23[;?@1!R">
                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="JS},.#xyk[v26FxL+=ql">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="xVM1P%gu:c)!)!SS)aZS">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <statement name="items">
                                                                                                                                              <block type="list_item_shape" id="wG//@1f|Ix{kd@1w^7kJqJ">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_list_align" id="A6h0WFz0h$tXePaXEPK@1">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_list_item_source" id="$x|qPM$F.zx,,P.Os(+`">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="source">
                                                                                                                                                          <block type="func_characters_of" id="H@1,:rRhKY#y0H]FHh`?p" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="D$4)Et-jKqUzRC}CjfL4">
                                                                                                                                                                <field name="value">x2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="template">
                                                                                                                                                  <block type="image_shape" id="eryE@1hcyFB5Dz9~.oltb">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="b)!Q+PlI}LoVsOr{G5u|">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="XT9WyIo)%Z^ggw[LkNPq">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="r(U~,;#}lnQs%O]i3K5s">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="xKWXINK^kB%^)NP@1,1Ny">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="@2y]%;?L(3k!o}K=-M6;2">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id="MEzD9h+Op;6`,a3q`V[T">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="RScd)h)EoJ@1iD{IKXy_m">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id=":yKOf+IMKlJ6Q@1Wi.2yM">
                                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="!soW@1qNjB.$`nTF)xqPI">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="6i6C1`:C:g)_]-42(U?j">
                                                                                                                                                                        <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_template_for" id="y5C+9.u_QJap-vcLVG=d">
                                                                                            <field name="variable">$cell</field>
                                                                                            <field name="condition">$cell.col == 1</field>
                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                            <field name="#callback">$cell</field>
                                                                                            <statement name="body">
                                                                                              <block type="image_shape" id="n@2]_,vyG~:I?;y^LuVEK">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="NKGP7bH3RCmgWC1;D0#`">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="w#Q8fg1plt.T3CKK0RV1">
                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="?tCVV-Ut8Caf+_uBcK2O">
                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id=",jqD|pb_i-VUvWkG7`EW">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="_w{4/SM,;^t?WogfPl1r">
                                                                                                            <field name="value">plus.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id=",-|_bJ0|uuq^mcF[CU5[">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="A/I@1qDniB0Y6_$w-;Hy6">
                                                                                                                <field name="value">${image_path}plus.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template_for" id="4Y5fIb!K!zi#mNS94@2{2">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="condition">$cell.col == 2</field>
                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="grid_shape" id="Pcbc+l{HTlK9}+BO|x}-">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_grid_dimension" id="@1)u;!k2A#g._TZg/zq90">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="rows">
                                                                                                          <block type="expression" id="M#z),A2V-EpXyy!j?Bmw">
                                                                                                            <field name="value">3</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="cols">
                                                                                                          <block type="expression" id="$TyhaveX07rQQYI+IKwv">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_position" id="A5,T}`RB`v#`[P1T1c]R">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id=";3KT9f#0CY;)Tl5sk/%x">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="(y]DoK9-x7+q]5FRq4?B">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="B(7y6jaYL7Db$aTN[KE=">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="Xuc:+(~@2f!f|c%[=@1cvX">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="WFO)~fFGUoorF5YYn=}c">
                                                                                                                    <field name="value">140</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="a@1Y!3v__}@14)O`%WrJTw">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="JO8n):o,.!4)]]A8_#PH">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="`sZKYs{`#gB:JJe6yjAO">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_source" id="e^?4pFG@2Q9_LNSn#-#XZ">
                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="~v~Y}@2(]wY~:D/92W=k{">
                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_random" id="_[jA+F5.9z{@1N}$LszSl">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="random">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_show_borders" id="5^%OkyJ~$MbxNu_Fh4Y.">
                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_template" id="#o8=3]|7dp~}fSAQAH+|">
                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                    <statement name="body">
                                                                                                                                      <block type="if_then_block" id="II?3em$$Ohf|@2c+Ds=ke">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="Y}(W@22xrOw[x0CeXF|q}">
                                                                                                                                            <field name="value">$cell.row == 0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="list_shape" id="GH!{-040`%[v64A.$];C">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_list_direction" id="e`r$MbK+h/Nhl8_/c|vz">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id=":wio?.YG2{V%N6m$Y+?n">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="er9ar+l,:Xx~}xDeIM7N">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="V@1ziZ))Fjy,g)mTBhDlY">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_anchor" id="K-Trn`/SDrpi@2h-[,:jj">
                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="lm6ePxhuv]I@2O..Uzl,;">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="Oip,u;ep`GVL:Vi,Qxml">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <statement name="items">
                                                                                                                                              <block type="list_item_shape" id="4}yu)LD=IEGn6??@1J9/(">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_list_align" id="}QOgjoJZRQmV.`5u16{j">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_list_item_source" id="PE{5WqZQQk8WY7Yp]S@1z">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="source">
                                                                                                                                                          <block type="func_characters_of" id="E#,Ausr1|=iGkCyDHt~0" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="9d$LA$ZV]5Lvhg]d[,,l">
                                                                                                                                                                <field name="value">y1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="template">
                                                                                                                                                  <block type="image_shape" id="?855MkK/j|ih]dF?X6sd">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id=";eIlK+SdXsRjnVS[5%cK">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="`U%z-#]V)6O3-PaWeWNF">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="+-kAy0]6=iXr~Lvr^j3F">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="_Al#9_q^eKFVN=IEmV(f">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="10pNfkg$H?[%IlvE(=QX">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id="piGdgy`(?=._k81k4-^`">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="/O!31e_)k+LQ9Rf6iLrk">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="~2s(b2JN]uyk-n8std2|">
                                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="x$s!.`o:Tz~6}4U)m%HB">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="q}~ZR.vf[nFAlb4wQ(Mf">
                                                                                                                                                                        <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="if_then_block" id="m$|u67E[5Nl^9Rp$cw`g">
                                                                                                                                            <value name="if">
                                                                                                                                              <block type="expression" id="v1lOv/fzK7Ejrw`QA@2su">
                                                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="then">
                                                                                                                                              <block type="image_shape" id="dX!z/?VG@2~}z}iQ@2ejm6">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id=".;9XHt9DmqU5CYSMqAWT">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="iWI7||8DZkkMS|rPb6;@2">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="f01{hc_2(z4S(AT`mhwA">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="og~arJJ8v9{,J6}6GFgI">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="{sMf]Y@2,A~(X_F.9/#SZ">
                                                                                                                                                            <field name="value">line.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="Q4e9X.oV/|NAT@2^kj2$1">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="@1oE$i^6%,Th$04vnL=ll">
                                                                                                                                                                <field name="value">${image_path}line.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="if_then_block" id="{R?r9r)oT{iBKI1Y9)nY">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="Dy{^lNRJoB:Nx3~NBoL8">
                                                                                                                                                    <field name="value">$cell.row == 2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="list_shape" id="16@1t0`CE$ux$k2hE``7-">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_list_direction" id="?XYi2#QUOzh3Qa@1#[BKP">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_position" id="%)`m~$`9dp5p34N_5~J)">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="L]#Cr~0(70k(P`$sxtF-">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="7wF%rkEZ=JULEGV{7z5U">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_anchor" id="JgrykOClgE@25a/iz]z:t">
                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="]8-b?Y6l;4}tTe|$6I!}">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="/DfG1h[]b%RBNJE+D0k=">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="items">
                                                                                                                                                      <block type="list_item_shape" id="TC[^GYT=aF6BRk^5TRK^">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_list_align" id="cFV;vH7KH/b:BI!U/QeN">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_list_item_source" id="F=`!?[w`O@1sT8Tef9;Yn">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="source">
                                                                                                                                                                  <block type="func_characters_of" id="YU:G%Z5pVh|l+5s|$!$v" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="SEy6U9h}$@2M)1Y[r?(fu">
                                                                                                                                                                        <field name="value">y2</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="template">
                                                                                                                                                          <block type="image_shape" id="bCXf@2K4zzcM[clQua4dh">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="YL3k.zNpVWJc1|w?)k8:">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="%IonrB#~?``p+5gBA5|Q">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="s@2Rryu2p{%_x3^,`se/@1">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="f,cu~kH_1`R__%.l-Sa%">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="ujX{7/:+,Ij6{_{FEUjn">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="u1`cdWZ]W9b(,+B@2ASAs">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="Tgw;!;{b=om/k?OU%a_q">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="rp9IcGb#8;DS_xA3@2ed:">
                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="vpaaaGcpOh@27=p,KgrE!">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="X2534zK1Tu$)?T5~5j9@2">
                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template_for" id="0,TkUU4c@2~$5aud4XY^x">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="condition">$cell.col == 3</field>
                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="/xpI4C=hM}2dLa]^96N:">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="9R|7-W,4YXm;Z=5J^s}z">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="3A{h(dYe#]n#%O4q$%iV">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="3mGbH%4`I`=zsM%/xIvI">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="W}s)T`(Q@2?+d4Anq{r2N">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="UYr1|.Iu=%;Ps2B~x#">
                                                                                                                    <field name="value">equal.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="7vQuR}x_045sk@2y4{$e/">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="!#:8lV}8IVGlrW7goL+T">
                                                                                                                        <field name="value">${image_path}equal.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template_for" id="E;g@2qG4.~w/z0UoxIQWL">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="condition">$cell.col == 4</field>
                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="rows">
                                                                                                                  <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                    <field name="value">3</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="cols">
                                                                                                                  <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                    <field name="value">1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="!P!d`XcnrZxX|-7?h.l8">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="?-^mL.lH=qaL1[2h8Ci8">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="dP!NWz:#MEizzS;,[l5Z">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                            <field name="value">100</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                            <field name="value">140</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_template" id="Bthn1FuL6/+ai.Z.1:Ur">
                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                            <statement name="body">
                                                                                                                                              <block type="if_then_block" id="+=u8VtT8s)R!(=07~u.j">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="0PBv.YL;@1T+Zcg~3P!f%">
                                                                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="image_shape" id="`I-sHWyw84@1lkdM6AaA^">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="A(S_J5o?|jmgy#),{9|V">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="TJ#OiweCAOWEP4{4`BvT">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="Ov[WGAGN?vУgimWX{">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="!O@1+rn;I=d2P]^)Y=yHl">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="OK5$sGi`/-7O}y{`S{FF">
                                                                                                                                                                <field name="value">shape.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="xmTW|^TsnY|RCsCJht1F">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="z6C@2iqkxlI9jvCN2m!4?">
                                                                                                                                                                    <field name="value">${image_path}shape.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="algorithmic_input_shape" id="72WTWABkm`[Drvusj.Ye">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_value" id="IPbAEU28/R-~ic,ce=uU">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id=";Z?]o^ln]s|U6Lt}?kuq">
                                                                                                                                                                <field name="value">n1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_position" id="=]en+z+0Siuyr,e@1haK=">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="u$9J5FLDw%SX%[q)x`]m">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="Ge3;hQV,GtIJS#b}tPJJ">
                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="2NJdt4:6$[+gjUzps`M+">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="+zY(!tKjFC~Mb,o(nRBv">
                                                                                                                                                                        <field name="value">106</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="-tg$uM|FFHN-gjg!J)Vl">
                                                                                                                                                                        <field name="value">61</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_input_keyboard" id="f/K=Xp=Nq{NMG0+F`x#@2">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_input_max_length" id="+upn/Fx|,x8QxmHkx,PS">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="maxLength">
                                                                                                                                                                              <block type="expression" id="YyCWL[ccrMRHTIn=uoZb">
                                                                                                                                                                                <field name="value">n1.toString().length</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_input_result_position" id="Qu[Vn=OUd!770yVV@2?~^">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="resultPosition">right</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_tab_order" id="w+@2yDIK_?7gK3zYCdG59">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="tabOrder">
                                                                                                                                                                                      <block type="expression" id="RhOJvG%:Se^@1x%s7S!:c">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_stroke" id=")s5SaU?.4S3BPg:=Y?7T">
                                                                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_fill" id="P{9]i{Ilw{8$Du8#tb?t">
                                                                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style" id="ic}c8R=`:06A)ZFIC4p?">
                                                                                                                                                                                                <field name="base">text</field>
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_text_style_font_size" id="RAM]nG.zRSoOs4JeED66">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="fontSize">
                                                                                                                                                                                                      <block type="expression" id="s-hcRe4_4l`+_I,.K!.h">
                                                                                                                                                                                                        <field name="value">36</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style_fill" id="aePOXJ%)b8gro0R`G,`}">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="fill">
                                                                                                                                                                                                          <block type="string_value" id="qbKj|_wAQYd7bP;nx|lq">
                                                                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style_stroke" id="(A[QTHCcq]%zPX;IQPgK">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="stroke">
                                                                                                                                                                                                              <block type="string_value" id="SyZb|hP~$:9N?8/u(rlT">
                                                                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="~.N2BdD4cA`=HZ}=}QKk">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                                                                  <block type="expression" id="=U{bR(zoW_;5]lF$)WM2">
                                                                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="if_then_block" id="u7tivOlukN0(6Av0HB@2?">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="F;=V#s7J:JkAh4VYkZsi">
                                                                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="image_shape" id="{9?Z``WIMEZB@1i8p{0%f">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="ff@1oJHy@2$2FV-r07TOUl">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="+6KYVV%+a@181#J_!@2ehd">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="jAo^qvf%[1!Xrx7Fd9Hz">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="%S[ea$5C5k4˴#sSjV">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="@149d48{?0gc[y,e-Wj@2X">
                                                                                                                                                                    <field name="value">line.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="~1QijG`s/tI`kXIpXZt~">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="y=BfAe(8zBe-GwroQH[d">
                                                                                                                                                                        <field name="value">${image_path}line.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="if_then_block" id="srN@1XjKd,0{)_b}4E{o?">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="%o!C!b.ukA)A@2UTp!!fV">
                                                                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="image_shape" id="[B{S3rI@1uOp/mLeSQ@236">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="KdM{p3FR#$cFCejf[ClB">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="xO%YC_i}tNz~zKe)a2r}">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="|nBK{oufNkI)|R7_Zz^Y">
                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_key" id="%5)Hpl0z-]MSb.nu0C.o">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="key">
                                                                                                                                                                      <block type="string_value" id="#L4~QXo/i6Z}@1hh_jndW">
                                                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_src" id="Br8E8LD;q:2Ne0?VzB,_">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="src">
                                                                                                                                                                          <block type="string_value" id="y2AL+=5lP/W8un!CbI1_">
                                                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="algorithmic_input_shape" id="Rf6z;}O@1j=`jnHb:@1)(1">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_value" id=":hs5m(Qz%T-@1AcH!{=2n">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="PQ7Fu8?%M@22TikSzz3e`">
                                                                                                                                                                        <field name="value">n2</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_position" id="f;^NIFlZH7n:3Wz;EzxY">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="?@28R}mfTCD@1jKes8gj#@1">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="b:`%qbTDfJrZ-/r16xR|">
                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_size" id="=Z=3p@2{Z.8L}yqunQFr7">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="width">
                                                                                                                                                                              <block type="expression" id="jZ=Ew/f-;L6Euxq!qG[O">
                                                                                                                                                                                <field name="value">106</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="height">
                                                                                                                                                                              <block type="expression" id="Q))(pydoJwDd%!Fko5aG">
                                                                                                                                                                                <field name="value">61</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_input_keyboard" id="v!Rl^Y$@1wAPWoZA_f)@1w">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_input_max_length" id="VFB@2)^7xY]{vYrcE|]^n">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="maxLength">
                                                                                                                                                                                      <block type="expression" id="V~_n,PuKEetlV,m9GEdn">
                                                                                                                                                                                        <field name="value">n2.toString().length</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_input_result_position" id="~{(=P,(Fk.`$p6@1WQ7wA">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <field name="resultPosition">right</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_tab_order" id="AeCR0:d,Hmzw_]Z0]Tw~">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="tabOrder">
                                                                                                                                                                                              <block type="expression" id="|cS-,YTb$=/t(+z[oGOT">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_stroke" id="6rj|G);f6QE+ncjohC}7">
                                                                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_fill" id="7p!5I:E`tSTc6jNgla5k">
                                                                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style" id="t@13!Kuiu8R`hT/`q84G{">
                                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                          <block type="prop_text_style_font_size" id=",ajkBL:OVUPuzPhmE~z4">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                                              <block type="expression" id="Wr28:]K%#:Y9MN1GPjiA">
                                                                                                                                                                                                                <field name="value">36</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_text_style_fill" id="FWW|X%4^-YsX/YVHQZ0M">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                                  <block type="string_value" id="wZ]9_)DesT9-4YxvP5S(">
                                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="(.;iiRI,1v+UJHH[PZm=">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                                      <block type="string_value" id="jzfH(a(q@1$$[$36bL@2t5">
                                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="](jg,uXBF+TGNB_IkM;g">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                                          <block type="expression" id="G~ISu}!94,7w@10,hq~?j">
                                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="partial_explanation" id="HRMi1H9%s@1gMmw#gFPtU" inline="true">
                                                            <value name="value">
                                                              <block type="string_value" id="UI0}8PpN)=i6,{hW,_$X">
                                                                <field name="value">&lt;u&gt;Step 1: Familiarise yourself with adding fractions.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
  #ALL {width: 950px; background: rgb(150, 200, 250, 0.3)}
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table id='ALL'&gt;
  &lt;tr&gt;
  &lt;td id='TD'&gt;Example 1:&lt;/br&gt;
&lt;table style='margin: auto;'&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;7&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color: green'&gt;10&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: orange'&gt;1&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color: green'&gt;5&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;7&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;10&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: orange'&gt;2&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;10&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;9&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;10&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/td&gt;

  &lt;td id='TD'&gt;Example 2:&lt;/br&gt;
&lt;table style='margin: auto'&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: red'&gt;1&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color: blue'&gt;3&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: red'&gt;1&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color: blue'&gt;2&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: red'&gt;2&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;6&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color: red'&gt;3&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;6&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;5&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;6&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td id='TD'&gt;&lt;p style='color: green; display: inline-block'&gt;Hint&lt;/p&gt;: What is the lowest common multiple of denominator 10 and 5?&lt;/br&gt;
    &lt;p style='color: orange; display: inline-block'&gt;Note&lt;/p&gt;: Numerators 1 has been changed to 2 to show equivalent fractions.
  &lt;/td&gt;

&lt;td id='TD'&gt;&lt;p style='color: blue; display: inline-block'&gt;Hint&lt;/p&gt;: What is the lowest common multiple of denominator 3 and 2?&lt;/br&gt;
    &lt;p style='color: red; display: inline-block'&gt;Note&lt;/p&gt;: Numerators 1 and 1 have been changed to 2 and 3 to show equivalent fractions.
  &lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                </field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="end_partial_explanation" id="mHvDkCtm{Z-F6-,8^;X|">
                                                                <next>
                                                                  <block type="if_then_else_block" id="A:i@1{6qxvWw)B4B^m{--">
                                                                    <value name="if">
                                                                      <block type="expression" id="(63s8xZd3?@2j1_a4]/^r">
                                                                        <field name="value">range &lt; 4</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="partial_explanation" id="_I5d@2ycP2W#j)kMc^k#S" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="FO:9pi{S}Zj,GOlGoIks">
                                                                            <field name="value">&lt;u&gt;Step 2: Let's do fraction addition.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${x2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${y1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${y2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1+y1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${x2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;Is&lt;/td&gt;&lt;td&gt;&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 3px solid black; color:'&gt;&lt;b&gt;${x1+y1}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;&lt;b&gt;${x2}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/td&gt;&lt;td&gt;a simplified fraction?&lt;/td&gt;
  &lt;/tr&gt;&lt;/table&gt;

${UCLN(x1+y1, x2) == 1? 'Yes':'No'}.
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="p6[RN(z9A|9=IG/7!@1{3">
                                                                            <next>
                                                                              <block type="if_then_block" id="e:e,t8Me3R!gsMF9u}xe">
                                                                                <value name="if">
                                                                                  <block type="expression" id="-7:@1Lom=Fcru+ynDS.0%">
                                                                                    <field name="value">UCLN(x1+y1, x2) != 1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="partial_explanation" id="^t3~Zr@1FQ}H]qJ!XyA,b" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="Xmxtx-WE1}.vKyuWJm7M">
                                                                                        <field name="value">&lt;u&gt;Step 3: Simplify the fraction.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1+y1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${x2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1+y1} &amp;divide; ${UCLN(x1+y1, x2)}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${y2} &amp;divide; ${UCLN(x1+y1, x2)}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${n1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${n2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;Is&lt;/td&gt;&lt;td&gt;&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 3px solid black; color:'&gt;&lt;b&gt;${n1}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;&lt;b&gt;${n2}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/td&gt;&lt;td&gt;a simplified fraction?&lt;/td&gt;
  &lt;/tr&gt;&lt;/table&gt;

Yes.
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="S!{pQ!#A|^U2%x?^i%H4"></block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <statement name="else">
                                                                      <block type="partial_explanation" id="2LjYHjjbEttLwG2H9LVB" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="#zP-BhtnBb8|IK!KU@1VV">
                                                                            <field name="value">&lt;u&gt;Step 2: Identify the lowest common multiple of the denominators in both fractions.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

The lowest common multiple of ${x2} and ${y2} is: &lt;b&gt;${x2@2y2/UCLN(x2, y2)}&lt;/b&gt;.
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="F6PIv`b7GACMc+J[wJN7">
                                                                            <field name="name">BCNN</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="?_gbD|ijBRTGb5@1/|3F@1">
                                                                                <field name="value">x2@2y2/UCLN(x2, y2)</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="end_partial_explanation" id="AojMEh)Zw3sXaXxCOqX4">
                                                                                <next>
                                                                                  <block type="partial_explanation" id="b_tp+uE@1}8sm8c$Sor1F" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id=".fYLx@2MOj7:CCUqUq+S5">
                                                                                        <field name="value">&lt;u&gt;Step 3: Convert both fractions to common denominator ${BCNN}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
@2Do not forget to multiply the numerator too@2
&lt;/br&gt;

&lt;style&gt;
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1} x ${BCNN/x2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${x2} x ${BCNN/x2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${y1} x ${BCNN/y2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${y2} x ${BCNN/y2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1 @2 BCNN / x2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${y1 @2 BCNN / y2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id=",:yReX4n`]v7])cpj@2a6">
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="0MFu;6rPK.Z}:x9YNFsN" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="JQ^M:kKmikC1}u(gFyVT">
                                                                                                <field name="value">&lt;u&gt;Step 4: Finally, add both fractions together.&lt;/u&gt;&lt;/br&gt;

&lt;style&gt;
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1} x ${BCNN/x2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${x2} x ${BCNN/x2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${y1} x ${BCNN/y2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${y2} x ${BCNN/y2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${x1 @2 BCNN / x2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;+&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${y1 @2 BCNN / y2}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${(x1 @2 BCNN / x2) + (y1 @2 BCNN / y2)}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/tr&gt;&lt;/table&gt;
  
  &lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;Is&lt;/td&gt;&lt;td&gt;&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 3px solid black; color:'&gt;&lt;b&gt;${(x1 @2 BCNN / x2) + (y1 @2 BCNN / y2)}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;&lt;b&gt;${BCNN}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/td&gt;&lt;td&gt;a simplified fraction?&lt;/td&gt;
  &lt;/tr&gt;&lt;/table&gt;

${UCLN((x1 @2 BCNN / x2) + (y1 @2 BCNN / y2), BCNN) == 1? 'Yes':'No'}.
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="u_Fd0mpebn,ieme`}@2Ro">
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="q1N^1J4E8UNZ,Klk$yE{">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="Zg#4:+{Ir4sKsQc^V%{+">
                                                                                                        <field name="value">UCLN((x1 @2 BCNN / x2) + (y1 @2 BCNN / y2), BCNN) != 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="partial_explanation" id="(W@2Ui=80c@2AV@1AB:p.vp" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="_]7r5GD6!+:zJI;3pUlQ">
                                                                                                            <field name="value">&lt;u&gt;Step 5: Simplify the fraction.&lt;/u&gt;&lt;/br&gt;

&lt;style&gt;
  #PS {display: inline-block; text-align: center; margin-right: 10px; margin-left: 10px;  margin-top: 15px}
  #TD {text-align: center; border: 1px solid black; padding: 5px;}
  &lt;/style&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${(x1 @2 BCNN / x2) + (y1 @2 BCNN / y2)}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${(x1 @2 BCNN / x2) + (y1 @2 BCNN / y2)} &amp;divide; ${UCLN((x1 @2 BCNN / x2) + (y1 @2 BCNN / y2), BCNN)}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${BCNN} &amp;divide; ${UCLN((x1 @2 BCNN / x2) + (y1 @2 BCNN / y2), BCNN)}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;
&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid black; color:'&gt;${n1}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;${n2}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;

&lt;table style=''&gt;&lt;tr&gt;&lt;td&gt;Is&lt;/td&gt;&lt;td&gt;&lt;table id='PS'&gt;&lt;tr&gt;&lt;td style='border-bottom: 3px solid black; color:'&gt;&lt;b&gt;${n1}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style='color:'&gt;&lt;b&gt;${n2}&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/td&gt;&lt;td&gt;a simplified fraction?&lt;/td&gt;
  &lt;/tr&gt;&lt;/table&gt;

Yes.
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="end_partial_explanation" id="rUuWx;hXV46HeN$fJgE}"></block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */