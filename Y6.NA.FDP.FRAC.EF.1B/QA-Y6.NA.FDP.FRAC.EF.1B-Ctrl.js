
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.FDP.FRAC.EF.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.NA.FDP.FRAC.EF.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "tem",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "1"
          }
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          },
          {
            "#type": "variable",
            "name": "b",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "a+1"
              },
              "max": {
                "#type": "expression",
                "value": "6"
              }
            }
          },
          {
            "#type": "variable",
            "name": "c",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "2"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "8"
                  }
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "b",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "a+1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "12"
                      }
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "b == 7 || b == 9 || b == 11"
                }
              },
              {
                "#type": "variable",
                "name": "c",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "2"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "6"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "8"
                  }
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "b",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "[8, 10, 12, 14, 16, 18]"
                      }
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "a == b"
                }
              },
              {
                "#type": "variable",
                "name": "c",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "2"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "8"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function print_frac(a, b){\n    return \"<table class='frac'><tr><td style='border-bottom: 2px solid #6d4115'>\" + a + \"</td></tr><tr><td>\" + b + \"</td></tr></table>\";\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "resp_x",
                  "value": "cx"
                },
                "y": {
                  "#type": "expression",
                  "value": "60"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Complete the below equivalent fraction."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "150"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 0"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "${tem}_${a}.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_${a}.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "line.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_line.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 2"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              },
                              {
                                "#type": "prop_list_item_source",
                                "#prop": "",
                                "source": {
                                  "#type": "func",
                                  "name": "charactersOf",
                                  "args": [
                                    {
                                      "#type": "expression",
                                      "value": "b"
                                    }
                                  ]
                                }
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$item.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${$item.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "240"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${tem}_mul.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${tem}_mul.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "330"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 0"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${tem}_${c}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${c}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "line.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_line.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 2"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${tem}_${c}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${c}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "450"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "equal.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_equal.png"
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${tem}_ans.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${tem}_ans.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a*c"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "170"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "130"
                },
                "height": {
                  "#type": "expression",
                  "value": "70"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "(a*c).toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "right"
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "42"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "b*c"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "280"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "130"
                },
                "height": {
                  "#type": "expression",
                  "value": "70"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "(b*c).toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "right"
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "42"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>We find an equivalent fraction by multiplying the numerator and denominator by the same number</u></br>\n\n  <style>\n  .frac {margin-top: 15px;}\n  table{display: inline-block;}\ntd{text-align: center; padding-right: 10px;}\n  </style>\nWe multiply the numerator and denominator by ${c} </br>\n\n${a} x ${c} = ${a*c}</br>\n${b} x ${c} = ${b*c}</br>\n\n<table><tr><td>\nSo the new fraction is:</td><td>${print_frac(a*c, b*c)}\n</td></tr></table>\n</br>\n\n<table><tr><td>\nSo the equivalent fraction of </td><td>${print_frac(a, b)}</td><td>is</td><td>${print_frac(a*c, b*c)}\n</td></tr></table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ],
        "else": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "resp_x",
                  "value": "cx"
                },
                "y": {
                  "#type": "expression",
                  "value": "60"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Find the correct multiplication to complete the equivalent fraction."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "150"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 0"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "${tem}_${a}.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_${a}.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "line.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_line.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 2"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              },
                              {
                                "#type": "prop_list_item_source",
                                "#prop": "",
                                "source": {
                                  "#type": "func",
                                  "name": "charactersOf",
                                  "args": [
                                    {
                                      "#type": "expression",
                                      "value": "b"
                                    }
                                  ]
                                }
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$item.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${$item.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "240"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${tem}_mul.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${tem}_mul.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "360"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 0"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          },
                          {
                            "#type": "prop_spacing",
                            "#prop": "",
                            "spacing": {
                              "#type": "expression",
                              "value": "5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "shape.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}shape.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      },
                      {
                        "#type": "choice_input_shape",
                        "#props": [
                          {
                            "#type": "prop_value",
                            "#prop": "",
                            "value": {
                              "#type": "expression",
                              "value": "c"
                            }
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "120"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "70"
                            }
                          },
                          {
                            "#type": "prop_input_keyboard",
                            "#prop": "",
                            "keyboard": "numbers1"
                          },
                          {
                            "#type": "prop_input_max_length",
                            "#prop": "",
                            "maxLength": {
                              "#type": "expression",
                              "value": "c.toString().length"
                            }
                          },
                          {
                            "#type": "prop_input_result_position",
                            "#prop": "",
                            "resultPosition": "right"
                          },
                          {
                            "#type": "prop_tab_order",
                            "#prop": "",
                            "tabOrder": {
                              "#type": "expression",
                              "value": "1"
                            }
                          },
                          {
                            "#type": "prop_stroke",
                            "#prop": "stroke"
                          },
                          {
                            "#type": "prop_fill",
                            "#prop": "fill"
                          },
                          {
                            "#prop": "",
                            "style": {
                              "#type": "json",
                              "base": "text",
                              "#props": [
                                {
                                  "#type": "prop_text_style_font_size",
                                  "#prop": "",
                                  "fontSize": {
                                    "#type": "expression",
                                    "value": "42"
                                  }
                                },
                                {
                                  "#type": "prop_text_style_fill",
                                  "#prop": "",
                                  "fill": "black"
                                },
                                {
                                  "#type": "prop_text_style_stroke",
                                  "#prop": "",
                                  "stroke": "white"
                                },
                                {
                                  "#type": "prop_text_style_stroke_thickness",
                                  "#prop": "",
                                  "strokeThickness": {
                                    "#type": "expression",
                                    "value": "2"
                                  }
                                }
                              ]
                            }
                          }
                        ],
                        "#init": "algorithmic_input"
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "180"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "line.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_line.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 2"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          },
                          {
                            "#type": "prop_spacing",
                            "#prop": "",
                            "spacing": {
                              "#type": "expression",
                              "value": "5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "shape.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}shape.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      },
                      {
                        "#type": "choice_input_shape",
                        "#props": [
                          {
                            "#type": "prop_value",
                            "#prop": "",
                            "value": {
                              "#type": "expression",
                              "value": "c"
                            }
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "120"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "70"
                            }
                          },
                          {
                            "#type": "prop_input_keyboard",
                            "#prop": "",
                            "keyboard": "numbers1"
                          },
                          {
                            "#type": "prop_input_max_length",
                            "#prop": "",
                            "maxLength": {
                              "#type": "expression",
                              "value": "c.toString().length"
                            }
                          },
                          {
                            "#type": "prop_input_result_position",
                            "#prop": "",
                            "resultPosition": "right"
                          },
                          {
                            "#type": "prop_tab_order",
                            "#prop": "",
                            "tabOrder": {
                              "#type": "expression",
                              "value": "2"
                            }
                          },
                          {
                            "#type": "prop_stroke",
                            "#prop": "stroke"
                          },
                          {
                            "#type": "prop_fill",
                            "#prop": "fill"
                          },
                          {
                            "#prop": "",
                            "style": {
                              "#type": "json",
                              "base": "text",
                              "#props": [
                                {
                                  "#type": "prop_text_style_font_size",
                                  "#prop": "",
                                  "fontSize": {
                                    "#type": "expression",
                                    "value": "42"
                                  }
                                },
                                {
                                  "#type": "prop_text_style_fill",
                                  "#prop": "",
                                  "fill": "black"
                                },
                                {
                                  "#type": "prop_text_style_stroke",
                                  "#prop": "",
                                  "stroke": "white"
                                },
                                {
                                  "#type": "prop_text_style_stroke_thickness",
                                  "#prop": "",
                                  "strokeThickness": {
                                    "#type": "expression",
                                    "value": "2"
                                  }
                                }
                              ]
                            }
                          }
                        ],
                        "#init": "algorithmic_input"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "500"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "equal.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_equal.png"
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "resp_y",
                  "value": "cy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "50"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "mix",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "3"
                    },
                    {
                      "#type": "expression",
                      "value": "[1]"
                    },
                    {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "expression",
                          "value": "$item"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 0"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              },
                              {
                                "#type": "prop_list_item_source",
                                "#prop": "",
                                "source": {
                                  "#type": "func",
                                  "name": "charactersOf",
                                  "args": [
                                    {
                                      "#type": "expression",
                                      "value": "a*c"
                                    }
                                  ]
                                }
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$item.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${$item.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 1"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_size",
                            "#prop": "",
                            "width": {
                              "#type": "expression",
                              "value": "0"
                            },
                            "height": {
                              "#type": "expression",
                              "value": "0"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "line.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}${tem}_line.png"
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.row == 2"
                    },
                    "then": [
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              },
                              {
                                "#type": "prop_list_item_source",
                                "#prop": "",
                                "source": {
                                  "#type": "func",
                                  "name": "charactersOf",
                                  "args": [
                                    {
                                      "#type": "expression",
                                      "value": "b*c"
                                    }
                                  ]
                                }
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$item.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${tem}_${$item.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>We find an equivalent fraction by multiplying the numerator and denominator by the same number.</u></br>\n\n  <style>\n  .frac {margin-top: 15px;}\n  table{display: inline-block;}\ntd{text-align: center; padding-right: 10px;}\n  </style>\n\nTo find the correct multiple, we divide the larger equivalent fraction. </br>\n\nNumerator: ${a*c} : ? = ${a}</br>\nDenominator: ${b*c} : ? = ${b}</br>\n\nFrom here, we know the correct multiple is ${c}.\n</br></br>\nSo the answer is:</br>\n<table><tr><td>\n${print_frac(a, b)}</td><td>x</td><td>${print_frac(c, c)}</td><td>=</td><td>${print_frac(a*c, b*c)}\n</td></tr></table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="-10" y="182">
    <field name="name">Y6.NA.FDP.FRAC.EF.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.NA.FDP.FRAC.EF.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="_?%b;2rN-zX:$|?R2Oiz">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="LO(^C}SSI,^uC@15_5!y:">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id=".y)kFy{|WF/SsajD!%yY">
                                        <field name="name">type</field>
                                        <value name="value">
                                          <block type="random_number" id="@1bR,%1Pu9m9Yf=/kHmJn">
                                            <value name="min">
                                              <block type="expression" id="q@1Gx),vn/KyZVBRD:~_+">
                                                <field name="value">1</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id="nCFSLO,w5^guOeaFP?f!">
                                                <field name="value">2</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="a|GkD?g@26/?=Rp{jHdcf">
                                            <field name="name">tem</field>
                                            <value name="value">
                                              <block type="random_number" id="P].6b@2iP^1,@25(Yn9e;8">
                                                <value name="min">
                                                  <block type="expression" id="5o}Zz[d$A5u-i=@1pxuLj">
                                                    <field name="value">0</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="A0^T?ht]-@1Q6;~0mckBr">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="if_then_else_block" id="Svcz{xHONEWutZ@2[vm^i">
                                                <value name="if">
                                                  <block type="expression" id="j1sAg0a;m!Sjt=M:zx$(">
                                                    <field name="value">range &lt; 0</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="PXps[C=D9(SW,fFw#o)O">
                                                    <field name="name">a</field>
                                                    <value name="value">
                                                      <block type="random_number" id="0GL9Rc[DREvKc76/ysGe">
                                                        <value name="min">
                                                          <block type="expression" id="เน{QfRPeO)VYA+fRMt.">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="0EpOn07a.Dh|OGvtydg|">
                                                            <field name="value">5</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="y@1!gv;Cwi^mO$:,4;Hg!">
                                                        <field name="name">b</field>
                                                        <value name="value">
                                                          <block type="random_number" id="s@2e~W_7:c98-Tazf%}~4">
                                                            <value name="min">
                                                              <block type="expression" id="l@2Q/f7n=(:Sj5}O2hBWJ">
                                                                <field name="value">a+1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="}-OXCtwHL(te{_v}?Ona">
                                                                <field name="value">6</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="r+H4@2~eYJMYs1$JyCy-Q">
                                                            <field name="name">c</field>
                                                            <value name="value">
                                                              <block type="random_number" id="`d_l8R+|29t5e,Fnd#6e">
                                                                <value name="min">
                                                                  <block type="expression" id="Byj5wgi7iE#NW$B6[!~E">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="~OH+i=;}^]@2_.nWlv;+1">
                                                                    <field name="value">5</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="if_then_else_block" id="P$}HY+/jT%L.GNM}G,4y">
                                                    <value name="if">
                                                      <block type="expression" id="Ft#Fkovd}oq(gsa+ihGw">
                                                        <field name="value">range &lt; 4</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="{_,K|CD3v%^[#eWN)Y0c">
                                                        <field name="name">a</field>
                                                        <value name="value">
                                                          <block type="random_number" id=".Q,I?y@2ZWuC|iXvX^]xz">
                                                            <value name="min">
                                                              <block type="expression" id="q2!cX?$q?T=_]tuqAIio">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="7s[b+{vN.x-Iej`n`?@1W">
                                                                <field name="value">8</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="do_while_block" id="gi}}UZ`aRa0$6fy`|6D@1">
                                                            <statement name="do">
                                                              <block type="variable" id="46O46L;8bWgAZUUTTX@1T">
                                                                <field name="name">b</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="ZWm9z0dQ|wC6ul@2U{gHG">
                                                                    <value name="min">
                                                                      <block type="expression" id="rpHhIE(/lSlQYX}F_;4]">
                                                                        <field name="value">a+1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="I9@1b.66)jjlJY[LR1X%g">
                                                                        <field name="value">12</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <value name="while">
                                                              <block type="expression" id="j$xL4:2Dcs[K%,gJ(|c4">
                                                                <field name="value">b == 7 || b == 9 || b == 11</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="!1y@1XrgHG?@16MGA%sT?5">
                                                                <field name="name">c</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="KrDsvG/=jqo`6Wo+@2(IW">
                                                                    <value name="min">
                                                                      <block type="expression" id="Tti2_|FJ_=/;g2l@1EC@1V">
                                                                        <field name="value">2</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="L8=|+c;WTNbVb@2[dfm6I">
                                                                        <field name="value">6</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="+k`xXr%A$2,^uP$kt-0;">
                                                        <field name="name">a</field>
                                                        <value name="value">
                                                          <block type="random_number" id="T)wmL]:8!Ds~X3;DWaoq">
                                                            <value name="min">
                                                              <block type="expression" id="Q(3:$viXk]M@1a=@2IwhAG">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="K.DQJC=hhrUt5xHBp@1b]">
                                                                <field name="value">8</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="do_while_block" id="i1PVMqa(EMzQMA#bn]Mn">
                                                            <statement name="do">
                                                              <block type="variable" id="qyrio6q|O,{5nr^^yx+{">
                                                                <field name="name">b</field>
                                                                <value name="value">
                                                                  <block type="random_one" id="EtJQqhX(9Qe|=uYT]-~M">
                                                                    <value name="items">
                                                                      <block type="expression" id="!uX;T,q/f5?!0Z@2;NF7G">
                                                                        <field name="value">[8, 10, 12, 14, 16, 18]</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <value name="while">
                                                              <block type="expression" id="X?gxF_4!7o~D{.j=KZu=">
                                                                <field name="value">a == b</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="1r}B8N.FUT/ozqv37+aO">
                                                                <field name="name">c</field>
                                                                <value name="value">
                                                                  <block type="random_number" id=",s{_tQi5rHXH(}D_P;qX">
                                                                    <value name="min">
                                                                      <block type="expression" id="rj.L;H$|(MGx7b9J@1O2m">
                                                                        <field name="value">2</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="=MQdjxc,F!GhPlkyV4S-">
                                                                        <field name="value">8</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </statement>
                                                <next>
                                                  <block type="statement" id=")Kl@1QE6;t8I-@2y(n}h{b">
                                                    <field name="value">function print_frac(a, b){
    return "&lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid #6d4115'&gt;" + a + "&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;" + b + "&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;";
}
                                                    </field>
                                                    <next>
                                                      <block type="if_then_else_block" id="t)hX|uIAnNhNjG]Bktcd">
                                                        <value name="if">
                                                          <block type="expression" id="a~b0z(xi4Lx~mgNsU@1y5">
                                                            <field name="value">type == 1</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="text_shape" id="o/NCpMgBj]ooM8g!/8)c">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="-9KpR$SDTeLLXOuT::M2">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="resp_x" id="mmHVUtB~#YyX0-7Df7vc">
                                                                    <field name="value">cx</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="-4[0q4I.UN6JWY49}G_1">
                                                                    <field name="value">60</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_contents" id="2PmC2V4XPiG}{E$+f+3m">
                                                                    <field name="#prop"></field>
                                                                    <value name="contents">
                                                                      <block type="string_value" id="}(B#=U7;MPlm.$J2e8/[">
                                                                        <field name="value">Complete the below equivalent fraction.</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style" id="QW,I?DkagIFY]LEiubr_">
                                                                        <field name="base">text</field>
                                                                        <statement name="#props">
                                                                          <block type="prop_text_style_font_size" id=";72R)@1cNn33S|e,y(XEe">
                                                                            <field name="#prop"></field>
                                                                            <value name="fontSize">
                                                                              <block type="expression" id="TUE@2XqYrhRifeYk@1~U@2;">
                                                                                <field name="value">36</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_fill" id="OiPd[i@27(F2My!}N[(L:">
                                                                                <field name="#prop"></field>
                                                                                <value name="fill">
                                                                                  <block type="string_value" id="#+h{zWW)%bKW];)v_^e9">
                                                                                    <field name="value">black</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="stroke">
                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                        <field name="value">white</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="strokeThickness">
                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id=")KYRsb@1rn1Uvvr#bb}/f">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="c,aiyKt2X/8Z8GWTYhu8">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="(mVGAl~`Ok~Q/9y}i(1/">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="DsRr.hD~9)=3t#c:Hj}r">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="pYec!O@2.@1wjf/AVP8vmy">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="vEF.mth[fWiCM5.N$x$D">
                                                                            <field name="value">150</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="resp_y" id="HcO@1Yuu_j(rMUi7}^JYr">
                                                                            <field name="value">cy</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="nF,XWDT^NjT?P9hh:TVU">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="}q)uRmw3O~lq^a|m#Kls">
                                                                                <field name="value">50</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="3qlQ+j~hly+(lW,,Pj)k">
                                                                                <field name="value">150</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_grid_cell_source" id="yIH4G_91N/mEwx@1vt^dQ">
                                                                                <field name="#prop">cell.source</field>
                                                                                <value name="value">
                                                                                  <block type="func_mix" id="fSRl:~5~BXc,IR:SznF9" inline="true">
                                                                                    <field name="collections">1</field>
                                                                                    <field name="variable">$item</field>
                                                                                    <field name="count">3</field>
                                                                                    <field name="mixer">$item</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="`B!SF,lVpoOw]C|#uhh-">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="uBHTj^v%0!?B~!O@1_Fae">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="@2:MHb^mg4a!E};)(Z0^G">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_show_borders" id=";e[ngzJm!VJr^fky2qXd">
                                                                                        <field name="#prop">#showBorders</field>
                                                                                        <field name="value">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_template" id="9TV0ZhLFZ^c7Zs@228C5[">
                                                                                            <field name="variable">$cell</field>
                                                                                            <field name="#prop">cell.template</field>
                                                                                            <field name="#callback">$cell</field>
                                                                                            <statement name="body">
                                                                                              <block type="if_then_block" id="}9b)CmRFE:F$IJML{l(A">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="4!xjN!3G8O6@1LK0#|7w1">
                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="image_shape" id="eP(@2xT|.(%2/M|Aic[mI">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="eE0{MVF2T:_jG^Yu2;O=">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="sH51RS+MFCLU#xLD4lyW">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="{f^TnLQRylsB{nR;T8,(">
                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="K2.,i@2p|OFfH{AQP%=X|">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="y-NeU$%ph2@2JIf%P_;.|">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="BT=Q4X$EZF^l${7FpHQn">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="Cs?(:E#`a!K2Or)7|o@2(">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="E!6TWtH!b0rM#FpY(%.p">
                                                                                                                    <field name="value">${tem}_${a}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="/=}TWupmc];@1PJ?!4Na3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="Ra]L[n`FwhFNb06}Jb[0">
                                                                                                                        <field name="value">${image_path}${tem}_${a}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="XMS_xz+cBbH|Z7Uom6Sj">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="t5AIMDLYD|Utn{%1N/Hq">
                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="image_shape" id="p@1QBisiJ];)gsgL%g_%M">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="L[H@2BV689.Y%2Kj8,2An">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="^G7{As=TeT5.$njG|y#U">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="9CTMNo~%:v1L4.qO(Pkg">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="z1AoS%p(YmG$UKhJd5z|">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="EbGFcwZdA+l[R$h`5x@1H">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="t^HiUEM@1OCp~v5nksp2@2">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id=",Y/8dXiaJ1x52}QRwCFB">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="70-MPMqtW6@2W%`LVY]DD">
                                                                                                                        <field name="value">line.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="g/V}_c5R-uVy@1(2)+Bn|">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="tFMveqjQlJ.(tQ}YtIQ0">
                                                                                                                            <field name="value">${image_path}${tem}_line.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="if_then_block" id="3lcdn9vkh_y,},PJNu.B">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="l_ZFR|iniF?pu-WC$|g5">
                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="list_shape" id="em(`h@2#TbGB46eKQJ-kZ">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_direction" id="7TLK%jS;tq/70p8G6p.B">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="dir">horizontal</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="R?$r,b.O-.vzr@2KYK3bX">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="g_Io@2M.sT2.TI^QO!;tw">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="/qKK9,V/vdhxHew7^nf^">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="Tlm9Ld}M1RL:~tkcL:S+">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="1Iq(Xn4c/M:Hp~SAhley">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="_n8bH=RGRC^L,5lRxwtu">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="items">
                                                                                                              <block type="list_item_shape" id="h7]Wjfr-Q{PSUPq7kYFC">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_align" id="{{+qrV1TmOjKP=:7@2L3g">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="align">middle</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_list_item_source" id="9([=K_R1d7{fk,Jc_^|A">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="source">
                                                                                                                          <block type="func_characters_of" id="WDAPX0xQF=r!an{)]BA8" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="/78F=1VVfHs?GrmL$APu">
                                                                                                                                <field name="value">b</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="template">
                                                                                                                  <block type="image_shape" id="08xZRD|-zjTT^-8I)7Vo">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="ccGV{5$DYw%{9trwBCY.">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id=";62z7b@19=8K.)#H0b?_]">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id=".+KEKiE4=ub,~(V#D`O$">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="_[1bCSDv;R{@20hMq9}l%">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="rRlM.+PwmoE@1:DXDe!;E">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="K28HUrW]Ex_.j+r9$}fH">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="T%FxoR46!ctJBQ@2%n]#N">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="5/?f6K6Cqn5bFgqB%@2zp">
                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="@13~iHFX%`KD]w2@2#R-?;">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="wZ9:@1yMS(t??[w/GZ:ZI">
                                                                                                                                        <field name="value">${image_path}${tem}_${$item.data}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="image_shape" id="V!GeVTU|s8QX,FrM{9XE">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="v`@2@2GR6U9UmbnH:m`,;q">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="hA~u=`CSz3VUQ,2RsI[-">
                                                                            <field name="value">240</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="resp_y" id="LnjoPuYpY3852ho!}w(j">
                                                                            <field name="value">cy</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="SrNW)Z)s(B@2I1rwLW#`-">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="nh-q1,v3}iqw52Yoj@2YJ">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="Vd$2]c_F[TE,0}{z(YOv">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_image_key" id="5Cg+p@1uyhs(V_rtWT1}.">
                                                                                <field name="#prop"></field>
                                                                                <value name="key">
                                                                                  <block type="string_value" id="6xZ8:i8BzRZp)vA5Ytt!">
                                                                                    <field name="value">${tem}_mul.png</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_src" id="Z2R[N_f{8}yec@2v7$K+w">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="src">
                                                                                      <block type="string_value" id="(k/l(?mCvxZX#,,k/-^I">
                                                                                        <field name="value">${image_path}${tem}_mul.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="bDLAn@1:Ou:8l_=yG5u.r">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="r{HlN,7(;D%eugG+Df0%">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="z|#j!CYD_M!ypKB5T9H$">
                                                                                <field name="value">3</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="yP(8Ikw6#(h5x:??+OHQ">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="A{((;fN$1X]0LU_{I}Pl">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="XBadmCBp9Pl?Dl1xD6zl">
                                                                                    <field name="value">330</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="resp_y" id="1TfK%-0AU!rBaxN$b5a[">
                                                                                    <field name="value">cy</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="B(8n@12y;2gVl{qa+nDGK">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="NRT~#Jm{kNrDipAzh4{l">
                                                                                        <field name="value">50</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="Xe_9pJV,%Zf%9@1j.px_N">
                                                                                        <field name="value">150</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="G_TPe3Lr^@2,I|6@1)4p19">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="func_mix" id="VGgQ)QdMvi!{C6J#`%Yb" inline="true">
                                                                                            <field name="collections">1</field>
                                                                                            <field name="variable">$item</field>
                                                                                            <field name="count">3</field>
                                                                                            <field name="mixer">$item</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id=":(~=|5Q-|yrR!)Pt%?|Q">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="DHZ5pV^}{$3CMv?]e9]^">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id=":tdEH.~[j%m$)?tLO}a?">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id="eTXZgm.18?S%,^dkyOq#">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="b=yrrY0y~5F]lEl;t,6k">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="if_then_block" id="o-HMdRxcqaA3zWyi23I[">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="x?cu`=Sb]Rz{l3e3:10]">
                                                                                                            <field name="value">$cell.row == 0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="list_shape" id="_S|~h^!R-n+j4ez)2;Re">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_direction" id="qbWKG.A|sjPfIY-I3=:c">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="dir">horizontal</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="x5uN@25Tx]:82#U-X)0n|">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="H,-^;U)d.DbOXX}^[?tg">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="10R:^Is`!X_%Mm#0s6GP">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id=".1u4=h9M`%ALx[LRpHBt">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="T9^Q14K]IRe|;eS(^Q)[">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="bj(!%3Ry1m_Nm(.4.4@2S">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="items">
                                                                                                              <block type="list_item_shape" id="o;p,h`r~iT=%}3y$9z6`">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_align" id="8]A.rjzz@2/J)~X~TED9Q">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="align">middle</field>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="template">
                                                                                                                  <block type="image_shape" id="8W(G$@2{s_cO=185r,}uz">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="sn?9FmY(OuW)b+ilpA]=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id=".AevzikAu`BoVv0w+@2h6">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="Yxsc@2o0Z)Y,X^GvG]3#y">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="{?=uMQ!DuL~7%`=M$o[z">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="Nz@2tatT`cK/rK;1K[YE^">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="qFwnj7?!zxNNYRolyY;n">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="UEKw0,Sy_#;sDL0?=?5B">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="~:%{(rKr3r1@2nb{8Wo{X">
                                                                                                                                    <field name="value">${tem}_${c}.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="y|QIifGu~O+HAN-UXc:6">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="t;RXrx|HDr5`!XQ#~_Fe">
                                                                                                                                        <field name="value">${image_path}${tem}_${c}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="if_then_block" id="(X%;/.s[nNryc;AR~=~;">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="E{GD0]$$WO1?5:^5-hF{">
                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="image_shape" id="Dx|${MxA@1|f+ABhM{0Sv">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="8I?`P7fyV3JhL:0kz6Ln">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="/rc4m%LgdHbA,=zcNMp/">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="zt9@1f{zq,?j(_bZA1LoC">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="lrk%wwDT{p=.!nPVH.sR">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="v~y_MU|Q{/yaWW_6|Hk+">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="-Z~[q[vW3d:_{wQJk,gc">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="IJYMVckQkTZ8@1@2r+i=}_">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="3FwUJkg#FFH,1)l-.E6k">
                                                                                                                                <field name="value">line.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="MhXXvAaruQ9oBp}Y`OQb">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="@1uSOO@1KS{|3v13Ic3WR?">
                                                                                                                                    <field name="value">${image_path}${tem}_line.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_block" id="Xx1KV[6{D_uKL)1l2#Cc">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="`(d}6m0)`$r^`IjZ(P|w">
                                                                                                                    <field name="value">$cell.row == 2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="list_shape" id="af8P!!065I]PL#4oR}Xp">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_direction" id="Ke@1rmYfP|^LGEXqTNq1E">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="?EI3:5{(9cE]~:vjCo#K">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="]X]2KENz5eorRjRc@1BIq">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="ho7oUXFqkvA.9eUaadW$">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="cnfdSlFS~%[d+mRWA`Z~">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="Cf]VRGH~?hs2MR8cF=b(">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id=",4.O-hkC`X^(RkC=/~l;">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="items">
                                                                                                                      <block type="list_item_shape" id="=7BEM.pKgs:PS+OBO;)p">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="zJ9;h?1{P.4Y:5RK%tr=">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id="Yco2Z45fQ^?!Myx$BFES">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="54qF(OmRNSiseC+i3L^Y">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="]b4R3T;OevXYTyoTh[FC">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="8q+IY)hvzrvio=M:i3_f">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="7Wq]WX@2NT2Y[PLXb6}(F">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id=";SLEEiioROc3a`qZxN+1">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="[^-~M9#4_A)r?2+Kpjc7">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="Ea/]=yhODVWWFH6chF@1@1">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="^`RV=VoWYpB}=6Ivs+!m">
                                                                                                                                            <field name="value">${tem}_${c}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="bKAU).%z1j!-1}kP]qea">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="[EjFyvG@1d(2xwW;|[X`M">
                                                                                                                                                <field name="value">${image_path}${tem}_${c}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="VucuwY:[PS-`$IW,r^%X">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="XzWT}ZW72q%!OX5.W-==">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id="]!rOzwMHfp.LWbvdMSL=">
                                                                                    <field name="value">3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="ojDe@1zwh76L@2c0K$_O[p">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="|+crb#cKBM[_:n~4SyjI">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="xA4;?Lcwc^p/eu:yMp0?">
                                                                                        <field name="value">450</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="resp_y" id="ryk+cmwV]j64m+kxbK9x">
                                                                                        <field name="value">cy</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id=",h2VktHOg$@1[fWnjT=QM">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="E?%CNiS_Q`JUrOV5jp@2)">
                                                                                            <field name="value">50</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="SZ0QLQ$0UmtIhysROX:y">
                                                                                            <field name="value">150</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="K`1+iq[hi8?W%Dok|dS#">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="func_mix" id="=hpYdNm_xD9+Ud|{.pGH" inline="true">
                                                                                                <field name="collections">1</field>
                                                                                                <field name="variable">$item</field>
                                                                                                <field name="count">3</field>
                                                                                                <field name="mixer">$item</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="?e},umPUzjxj)[V2J-SB">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="c?x-%WLQ5]Yy~$Fg]8Ae">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="Q+rdZA@1wh^P`Ui!pPTnE">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="e2BeInQNf!.tvpl79F:c">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="Ro-;:U~~7lnc88d)oJm~">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="if_then_block" id="z7-E2B0JSkF_j+$@2:i3l">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="5+Lc+tYVw##xLSH}L@1J+">
                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="image_shape" id="9[G%k#~@1gkXuK,`MSYZh">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="vr2COf=R~L}Za4QHPn-$">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="0WnB!@1%$btZcN$IU8[Mi">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="2xesN:y-v0j!B,GcWthA">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="i@1?8gATu3{~8._W7z|F,">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="LbEUTA+LHK!Bt=Z4_+NB">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id=";OCLwbEC4zj#+L;$^GA]">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="~Sw~U}A`ww|batIJ4@1U0">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id=")Fy2DfJ@2X=[^Mเธฃ?(iK">
                                                                                                                                <field name="value">equal.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="gh.09=F~Un8-GyemYau}">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="1h7b!}_zEAmKR8=r5y^@1">
                                                                                                                                    <field name="value">${image_path}${tem}_equal.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="Tw9ElA8xJQqQq$OgCkBt">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="@1Xp.!DR8gT{TCJ4tQU@2h">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="_3FZpkdnRLALl`1$lsWt">
                                                                                        <field name="value">600</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="resp_y" id="^aRxDk._+0tLo8BJ$TB:">
                                                                                        <field name="value">cy</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="WT2`k(+JY]#-%1s7MSk0">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="uU0nS4Sn_H=fF)@1SVk">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="9C8qm]g|^S(ik54[_)4y">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="Uhq#nv+?|HbMW.bJa:?a">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="k;exbXN[`OLlbfiMSG#i">
                                                                                                <field name="value">${tem}_ans.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="HWCZLpUGTCTTR5z@1uS2]">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="Pfe.X1B2sQT%A}t$15jF">
                                                                                                    <field name="value">${image_path}${tem}_ans.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="algorithmic_input_shape" id="PmO|e_seWJ0pUAhq/w@1g">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_value" id=")V[-J@1elcc(UtM8:X5JX">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id=",^6O-RfD]@1fnDFK9|A1d">
                                                                                            <field name="value">a@2c</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="s:ST[1X|%:8xr.ShtDcV">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="K3=LJ1jE:-qw3X?c|?7e">
                                                                                                <field name="value">600</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="@1_2%~/y{2`iJN$u].Z9?">
                                                                                                <field name="value">170</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="UPD:u?_2^OR-psU}syO3">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="ArpO[CDac4{599eaH.Nh">
                                                                                                    <field name="value">130</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="V5L4XwSq7GPsfB{a^%JA">
                                                                                                    <field name="value">70</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_input_keyboard" id="Ayj^uxy|}v[H4AhWr?rF">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                    <next>
                                                                                                      <block type="prop_input_max_length" id="@2TCw(@1C[/}(iJWXb_cNN">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="maxLength">
                                                                                                          <block type="expression" id="E3N_p_7N-~#!V3!WdnI4">
                                                                                                            <field name="value">(a@2c).toString().length</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_input_result_position" id="/o7BRubEKC:2.n)DXleN">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="resultPosition">right</field>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="bs9#:ZbtM-k5gx$M%~oz">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="3d[hae5Ui%y)b5sNFK~i">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="l@1c-(EBMiaC#SXLQxNTT">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="OrERx!Bc)GEoPHp2(]">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="k$sSdq=#xG[/#R,/a3Wk">
                                                                                                                                <field name="value">42</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="6Z/1!Q}$JKmU0BU|5]gR">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id=".]:[`l`Y2^-uSb%dR^A]">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="?]b4^B+Bcla%wKi2EYL5">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="jc5uS8ae~6tLrRa59ME-">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="T-C[O0f4](f89l1ihf`J">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="}r5f+WWJAFSz`q6I;{.$">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="algorithmic_input_shape" id="Rm-ogPWKM$eGF0Ui5:v}">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_value" id="e;={lyDPvseYVx4|Qy@24">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="K^W^iLw6@2.=V@2=?2h8Lv">
                                                                                                <field name="value">b@2c</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_position" id="TWeq@2|%LJz1qWm?x.sTd">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="~aG6p?Qm;YEf1q0Al(41">
                                                                                                    <field name="value">600</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="i8q^U-_zrF;5HeM]Cj@1v">
                                                                                                    <field name="value">280</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="u`gD@2S|MjU#j_?Of:ygd">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="gDJF@2B#;kzc#2}_qC{@2N">
                                                                                                        <field name="value">130</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="h_R-FH_l)`wIY_Z?1C=H">
                                                                                                        <field name="value">70</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_keyboard" id="^3]eyVKXE5x[0)n^e3W0">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                        <next>
                                                                                                          <block type="prop_input_max_length" id="lRbP@1!VDe`n!FP#ukEux">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="maxLength">
                                                                                                              <block type="expression" id="vi$FzzJQZj}$lNVi6)oW">
                                                                                                                <field name="value">(b@2c).toString().length</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_input_result_position" id="ylkD[km+`N;u@1)pvHM2Z">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="resultPosition">right</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_stroke" id="P4L4vmAs^aQqjifuvRev">
                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_fill" id="-7klcDGCp=[Df!6v!iDk">
                                                                                                                        <field name="#prop">fill</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="z`_J{lu-^[8%fRH`LLtU">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="ACvN[D^Nfewg4EFC:tdF">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="19n-`YODSO,7{gye~(PY">
                                                                                                                                    <field name="value">42</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="r:#InPIs+9H@2^6@1zhS_N">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="]^SJ#wzWQpJq0+p`Srk,">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="3=U|I%@2}45c:@2BPjXy8w">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="L@1QAiFt[=F.|GjRE%Wc9">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id=",dtu`dh`YE.P_jSSU)D@1">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="l7;h-xqtB:KK7?/RxjI3">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="^D7410Itqi7A+o.4pk3G">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="CtN/[t8.B`~J|yWUASqC">
                                                                                                <field name="value">&lt;u&gt;We find an equivalent fraction by multiplying the numerator and denominator by the same number&lt;/u&gt;&lt;/br&gt;

  &lt;style&gt;
  .frac {margin-top: 15px;}
  table{display: inline-block;}
td{text-align: center; padding-right: 10px;}
  &lt;/style&gt;
We multiply the numerator and denominator by ${c} &lt;/br&gt;

${a} x ${c} = ${a@2c}&lt;/br&gt;
${b} x ${c} = ${b@2c}&lt;/br&gt;

&lt;table&gt;&lt;tr&gt;&lt;td&gt;
So the new fraction is:&lt;/td&gt;&lt;td&gt;${print_frac(a@2c, b@2c)}
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/br&gt;

&lt;table&gt;&lt;tr&gt;&lt;td&gt;
So the equivalent fraction of &lt;/td&gt;&lt;td&gt;${print_frac(a, b)}&lt;/td&gt;&lt;td&gt;is&lt;/td&gt;&lt;td&gt;${print_frac(a@2c, b@2c)}
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="wNY2TOOiize.`8aUn4tT"></block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <statement name="else">
                                                          <block type="text_shape" id="8AZO@10?7X|a;IG%SS[Mt">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="๏ฃ~yv.FG[9nJiEF}5^8">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="resp_x" id="C1ydCc|O6]#P,/$$DSsF">
                                                                    <field name="value">cx</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="LB}9l#hiV=|Pi2|hDT+V">
                                                                    <field name="value">60</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_contents" id="86T1@1BJ-CvUxQg%-gP(w">
                                                                    <field name="#prop"></field>
                                                                    <value name="contents">
                                                                      <block type="string_value" id="W-q37qS_^??N8~0$X);8">
                                                                        <field name="value">Find the correct multiplication to complete the equivalent fraction.</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style" id="P,wf#:J].1z(.zxiS}G+">
                                                                        <field name="base">text</field>
                                                                        <statement name="#props">
                                                                          <block type="prop_text_style_font_size" id="p=U5kH@2YNqA@2SQvXD/l8">
                                                                            <field name="#prop"></field>
                                                                            <value name="fontSize">
                                                                              <block type="expression" id="X0z_YNt@2kIRuLk4HbIhq">
                                                                                <field name="value">36</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_fill" id="Iaif1`de4b;)RH,,kzpQ">
                                                                                <field name="#prop"></field>
                                                                                <value name="fill">
                                                                                  <block type="string_value" id="APT2?V:CfJZaJqan0FBo">
                                                                                    <field name="value">black</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke" id=";~rc@1JP;zp^L(@1`T..jY">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="stroke">
                                                                                      <block type="string_value" id="cX5JUE8OH5ZV0rq35KU4">
                                                                                        <field name="value">white</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke_thickness" id="h6acnWSKm/;@1dvMQ/~9E">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="strokeThickness">
                                                                                          <block type="expression" id="%3)$Z,ZfQ78+Zr!FE.f_">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id="1mN(C`Y|=XE/BLIcgPYi">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="GuL]g1(T6vsaZ`^f}e,Y">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="[=SDpSKo[W@1#k|[U=Am:">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="#a^(=P}]H}{!$iy+uo{V">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="UFO}):3iMq=G/Mqc0okm">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="Vk.MYWy@1UHU%-E8jO|s)">
                                                                            <field name="value">150</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="resp_y" id="s-_e@2^zoH@2]pHV%{otdh">
                                                                            <field name="value">cy</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="$fx.DzWNqK.#-14:$:Td">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="2XtTIZiosB]{ze){=}$F">
                                                                                <field name="value">50</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="eyVT.nSauJ1b3b@1uo,qY">
                                                                                <field name="value">150</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_grid_cell_source" id="IL]Hc`!Nw;7t9w|)sXw.">
                                                                                <field name="#prop">cell.source</field>
                                                                                <value name="value">
                                                                                  <block type="func_mix" id="G3ZZL,io@2/[!)[X9Kl.]" inline="true">
                                                                                    <field name="collections">1</field>
                                                                                    <field name="variable">$item</field>
                                                                                    <field name="count">3</field>
                                                                                    <field name="mixer">$item</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="x[az/Duuo#lHv+d1{m7)">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="a-b2E`VN!yz!8{t:j?vT">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="2X$$#;pHVXhq^Aq(2NWu">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_show_borders" id="TkV%c@1KsO8yX}PS^:6Mh">
                                                                                        <field name="#prop">#showBorders</field>
                                                                                        <field name="value">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_template" id="BOe#Ep$g;:]t|g}dEVGj">
                                                                                            <field name="variable">$cell</field>
                                                                                            <field name="#prop">cell.template</field>
                                                                                            <field name="#callback">$cell</field>
                                                                                            <statement name="body">
                                                                                              <block type="if_then_block" id="J42khou1nMOCKjV08,@1f">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="NmNL[Pm6-[{Md4H+C+CI">
                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="image_shape" id="$DW+G]zTEJ!fu|4H,JLN">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="7เธF2Iz~=D/qvlR}Psv">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="u09n$Vo]RTl_Z|AwIf7G">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="O^^Cr3B@2yy_.|f@2=J">
                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="MLf50VHew-`xMNv7jOKK">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="JJ,t9#PKG3DufWYg6/=[">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="4xqAput]]1m4T-1rro9z">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="z9N|?A0U?GGwu,eGs_?3">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="TA(yQK@2E^8Z4BsmFt{to">
                                                                                                                    <field name="value">${tem}_${a}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="dpg2+@1Y!%L9@1Pmdzewjr">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id=")}OyF}erL7Ej#|.PV$Su">
                                                                                                                        <field name="value">${image_path}${tem}_${a}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="$vaWT:|cFd./fsbwkd37">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="Jorr~?Jq]i?|QgX}s_X5">
                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="image_shape" id="T[dbS7Xml-Tlg6M2-e}$">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="pwyXzsT%fSr5}DDKp}^f">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="x`7tQcpop`dmg,^Gy:BF">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="[PdQcQAFDHr-}DLqtO76">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="NEOmO#]P-#$rk?.J[ri/">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="LnE=.#5`2BUS5LL:Wwed">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="5p1+oz5rZb#-uGQuR=#k">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="u;sFmDMU-i~a`UWNF.|3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="J!]1z(CGZZV?R8PebZBI">
                                                                                                                        <field name="value">line.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="|C-{}wCQ$x$k~bKognZd">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id=";MW,zJKcR73RXW?Vp|1K">
                                                                                                                            <field name="value">${image_path}${tem}_line.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="if_then_block" id="y,14N@1)1$v6~clW}3BZp">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="-8oz61)LJZDJ?fOVBmi:">
                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="list_shape" id="Sh0IV?=,abutv2ZjwxGi">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_direction" id="bJ9:%kv;e|(A(/la/Q%u">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="dir">horizontal</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="o[AY8C/edRaYuty$03k[">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="x,v%BUI9o6bISANBAA$d">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="YKELN8e-B5EzU:,@2LN$@2">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="NO:w%NMV@1G5^EtxwC^fc">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="Ic~nwkD!Yz.Q`B2$$bO/">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="z$=%s#Zเน‘cMD.]aaeKL">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="items">
                                                                                                              <block type="list_item_shape" id="+yjDMK}Do0Y6+.WjeNTo">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_align" id="L96l1Km?@2_vpoo23;1HP">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="align">middle</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_list_item_source" id="|6@1:lKIN{~[.VpDs.eg=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="source">
                                                                                                                          <block type="func_characters_of" id="HS6-Zx+1Z+LjKjXNY1c0" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="xp=6,R7+RoMNCV?1o[kN">
                                                                                                                                <field name="value">b</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="template">
                                                                                                                  <block type="image_shape" id="X?KtP7`1),HVe;TQ~{^V">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="^@2~n,j7-,{d3aoY4vDVC">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="D@1D9~R]TOpMr#/[bDd)m">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id=".`4l68WP58)6iE0S8:mr">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="80^6T+Wt](o6f^AP3h+U">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="[QHYzGlRZ@1s1q|}hha7@2">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="Gu^oh^sk^LN^=:2R9)--">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="`jI;;v4@1BILJMGCdsssn">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="n.0sf_Wx5SsDbl)XC)lF">
                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="qE;c:2YRT@14!oPV#t91$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="L@1.5mF{c?z:lOCtUS^8d">
                                                                                                                                        <field name="value">${image_path}${tem}_${$item.data}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="image_shape" id="8AJ}DNw%wn5R2uJOd;IG">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="4~}@2O~gr1r%-=m}?L}Yb">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="|Odyk+I?rdKwj,f8VI7T">
                                                                            <field name="value">240</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="resp_y" id="7[_rtP.Xg+p(b@1e};$Z!">
                                                                            <field name="value">cy</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="@1_|@1Sp/!09;kxBj}O)fR">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="XA4=F@1hss[pu1b?C+`xJ">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="uU(|Kbh){nH^/[#h}fZ@1">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_image_key" id="`Qri@1JVWY/@1XC.0ikQf2">
                                                                                <field name="#prop"></field>
                                                                                <value name="key">
                                                                                  <block type="string_value" id="2h;OtC6JR-{=6C2T+G]k">
                                                                                    <field name="value">${tem}_mul.png</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_src" id="0UtxjYS%@1%S$?#p+3)st">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="src">
                                                                                      <block type="string_value" id="{cC}Aj;c)4@2UjW9tk2g(">
                                                                                        <field name="value">${image_path}${tem}_mul.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="N9sCuhjdQX-v=:kgQ9|+">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="$c%XXNW!r5zz:n3%+I4N">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id=";ZZ6kfT[B^@1x6TE:FD5d">
                                                                                <field name="value">3</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="N}zbmoDu+q7Bn;OXld(p">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="vCSg^+i16!915@163=l}Q">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="]nnB,bcQCv_nKGT_9A^[">
                                                                                    <field name="value">360</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="resp_y" id="dSw,N#IF/2Al|IJI1^bA">
                                                                                    <field name="value">cy</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="@1f+:Usv8qG/J){$,WbEz">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id=",SYNkPIf5;LRNIgg)lPq">
                                                                                        <field name="value">50</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="4eHCBhNRAI5D.JwADM9}">
                                                                                        <field name="value">150</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="+,{{v(v/u.5UYw6/Ck`p">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="func_mix" id="|4MGcn2~^E5;@2j}H=3X:" inline="true">
                                                                                            <field name="collections">1</field>
                                                                                            <field name="variable">$item</field>
                                                                                            <field name="count">3</field>
                                                                                            <field name="mixer">$item</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="/0}O7zsMjteI[HJJc2eH">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="@1q]Dd$L(Wm:@2[eP[s?4q">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="h#^@2@2h1[d?|e#D๏ฃc:z">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id="@2j:|8DY$3@2ADFP$)Ia.n">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="A@2OS-x:ZaUz+wY{pyn%o">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="if_then_block" id="%BOn.:a$!J)MZ;c.r@2O?">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="7.^y0f1ZJ[W1TiDyL78F">
                                                                                                            <field name="value">$cell.row == 0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="list_shape" id="9[HzRn^qnJn6p`1d@1BwI">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_direction" id="O219P/:9gVh7CH(eUXs1">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="dir">horizontal</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="j?[;@1$If(;OJ(/!8zUg%">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="FQ`)}z^JqWHTLaT$P$:_">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="1XMunXF//QX^H6o3){GD">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="9gzXA:NbtX.)3zRE^!~u">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="L4czc?tK)dO0x|XMh)a6">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="Jเน•HdT7Tfx5!qopaL$m">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_spacing" id="X8Cc]YTN!L4)MyGiw;ul">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="spacing">
                                                                                                                              <block type="expression" id="Q.GAaa;m0!sV0w,JS]Tu">
                                                                                                                                <field name="value">5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="items">
                                                                                                              <block type="list_item_shape" id="dW,~nr_9,8s8TT(FY/2f">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_align" id="DV^|z^ptOyVEo_;ac;fD">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="align">middle</field>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="template">
                                                                                                                  <block type="image_shape" id="sol00FB=oalM]Qv%^3y-">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="Z7ยx,+EZ^Nio5(oD^/">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="K48:Z^3:CrZ@2![^IRyDu">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="?,F6o8i_MF/gi]V^w#pq">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="L08FPTz$9pL^zFS3$zbZ">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="prCSctX|`3Q%/EaBiz[;">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="Ip!zJi+Eez#jq^~xeNl3">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="_6cP0NZC6N^H@18dA9rY.">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="LY30egDPwtHk[?{,(Y,Z">
                                                                                                                                    <field name="value">shape.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="n1xP~6hckCYSx1h7a/#T">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="B^=,rDPclERe_lre;#Qz">
                                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="algorithmic_input_shape" id="1]d?amofnQz1(?9O-J^@2">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_value" id="?ZK/whQKFYN!Ds,uBC">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="OHF2=rK]`C{O9%t#oLNK">
                                                                                                                        <field name="value">c</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="D@1.|sOM[MPL^}qXYr;4+">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="VwEQc9^pA|IcKf=4=Y{3">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="4a93F(wrq5}=oaR8,)PL">
                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="W|m#,~fwn9S,W+hoJ@26D">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="qXMut:^[a|Q~u@2fR^tf)">
                                                                                                                                <field name="value">120</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="1nvW.N0oH@1tM(fSYk~8x">
                                                                                                                                <field name="value">70</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_input_keyboard" id="a1P;8USltHqObnn^]W6L">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_input_max_length" id="=[Xx$JYu5R;umW,5PhDW">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="maxLength">
                                                                                                                                      <block type="expression" id="[;BnCCu(^x%#M[]jTGup">
                                                                                                                                        <field name="value">c.toString().length</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_input_result_position" id="(N?XKm9nDl`dG}(A/tkS">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="resultPosition">right</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_tab_order" id=")91n;^K^7x#^6g3+GrLc">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="tabOrder">
                                                                                                                                              <block type="expression" id="OltBY8;Pz4)aX$mFgm_C">
                                                                                                                                                <field name="value">1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_stroke" id="i.`aQ,-N#4H?~7Ns/`h)">
                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_fill" id="mAkYy_Dc+bM1KYR.SP$h">
                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style" id="A,~yLY-JkEg=|{~T!2Pp">
                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_text_style_font_size" id="VsZzdze1R8t+/S+eo#fa">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fontSize">
                                                                                                                                                              <block type="expression" id="ehES?)~3_!p.mY:!kLIB">
                                                                                                                                                                <field name="value">42</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_fill" id="OG,Py,UQV(wmZa~v3ZRB">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="fill">
                                                                                                                                                                  <block type="string_value" id="LZeL_c$Oh9~W#v4@2_gXO">
                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke" id="GB4kJl#@1.@10WaM.}4E-f">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                      <block type="string_value" id="$VUWe=O2[ZCCDgv:Itk@2">
                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="N}vcT/oB]lLxV3_;F8!q">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                          <block type="expression" id="zc@1+N0w8Ic,/95,jHCDZ">
                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="if_then_block" id="w(#1O1Kq]@2:ne^C@2+Mu:">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="Z8A6n;ZE2lMDB-P!}tiS">
                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="image_shape" id="ckE]syzfYoWG-@1IZe=~D">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="93y/_{96GyPF@1=7,dK/}">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="hOl;30S%?;0azH@2|l3XC">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="]:|]2{HPt^AlC$/!U,x~">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="s@2]ch4YRcD.3l3;E#6R{">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="jV?k9uiaAB{cWR,)TDZF">
                                                                                                                            <field name="value">180</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id=":m1qJG@2S_w8Usx1.#PNC">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="p2uxM.X3JvIRt]/=BZie">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="X1,1a#Os;X,@1xE$+~r#t">
                                                                                                                                <field name="value">line.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="u:tQlf+@2R40|UFB$0M.Z">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="jDZY|KQidQ+h-uc+n5Xr">
                                                                                                                                    <field name="value">${image_path}${tem}_line.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_block" id="N4.$swR^6b+JE(+UXBh:">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="b(aQzv02SYAaT-:uV!/`">
                                                                                                                    <field name="value">$cell.row == 2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="list_shape" id="Y/aXT9@1-D{KxXN?PT6bj">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_direction" id="lK%@2?uiV6.StvRtU@1iQz">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="J`3}N6=v${66u[EFW/`/">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="su+q!At/PW28$P?fA#W}">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="nj70gyY@1h#vuJ:JSo.}S">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="L9,Ag[f8?J??Lwv5!y5g">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="^PV}0zKxkz)S#.^G9e~L">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="9|Etx7-4nn2ZeNdnaP9n">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_spacing" id="N2|?G#I,`09%iPK%NZ]L">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="spacing">
                                                                                                                                      <block type="expression" id="$Pd|I6gWXn{p[?TBqF%;">
                                                                                                                                        <field name="value">5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="items">
                                                                                                                      <block type="list_item_shape" id="Dxs|H#%uVh7I]t)H94#C">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="3-OIU/V:Elj(1C#Oh)yt">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id="s(o1KNfj2DLQt?`OE2.D">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="-$Zff;3;Hot1Pf0_jjxQ">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="/c@1FT{}mE4+k]/^s=Kgq">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="Em#Vn$nFaypi{G:}EMHI">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="otR48Yzkb9CMv$hsoI^m">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="{x/4lDdWIwIBu:v%@1Ye)">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="Gb]x4CMNZvP)TmeIqj.9">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="M?Zhob~Qmhr#J57.x9ku">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="R6]~`sJWEOE%x|IGIox7">
                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="ZHb~|qnZZ6d;^9ct@1AgC">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="OBx0L|$w0kBODHeJ-l6X">
                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="algorithmic_input_shape" id="i|QBD{Bj)?5gPfHXh/ve">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_value" id="-~=Q415K[S/63xXH.Yi}">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="`j8Vp`^@1)/)p{-?UMFiQ">
                                                                                                                                <field name="value">c</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="eIw/~dZEzfQpL#Hg3@12n">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="7@1zyR9y/$nK96@2Gp9nc!">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="gR$ba/?mb^WBIq,$Fiu}">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="W`uF5F#hr^M$ch6@1r;b]">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="Pgz5M-9!Wm+yzuLmwG)O">
                                                                                                                                        <field name="value">120</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="^.7oho$1`#op(-8vfYY,">
                                                                                                                                        <field name="value">70</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_input_keyboard" id="3nL].7C+3@2_5Q}y6CJI!">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_input_max_length" id="MAS[ZwtT+bm%~.Lx@1fTm">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="maxLength">
                                                                                                                                              <block type="expression" id="CQ($sfr41?(B6Eo9A2oC">
                                                                                                                                                <field name="value">c.toString().length</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_input_result_position" id="/H@2)^cjc6@1-5+Z}`wlab">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="resultPosition">right</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_tab_order" id="muYmoyLwj_=F+GLlg.aL">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="tabOrder">
                                                                                                                                                      <block type="expression" id="2mrwz@2pX2}$3a5_?x~95">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_stroke" id="]|#J?QjmOWzhM,9@2qwC$">
                                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_fill" id="Idr?v9(r8iFg.[IN|KWu">
                                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style" id="Lp/ME~4e.sI#VEjy8lrr">
                                                                                                                                                                <field name="base">text</field>
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_text_style_font_size" id="7/Ljs05Y0ljmSx7TZj0q">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="fontSize">
                                                                                                                                                                      <block type="expression" id=":C{(EYbm))9LGflhkUge">
                                                                                                                                                                        <field name="value">42</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style_fill" id="~NkA^1J;2:~yq{S@12n?j">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fill">
                                                                                                                                                                          <block type="string_value" id="1KR-0GG_csLATPn70$q%">
                                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_stroke" id="^W!}k-RhJJZx7k-dxNE,">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="stroke">
                                                                                                                                                                              <block type="string_value" id="+#@2TUybK7swBCYxH~dcm">
                                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="3nlAO]Z.@2(s=%fugd!?^">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                                  <block type="expression" id="?Bq-:N5o[cUf7E]`n}}A">
                                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="S?{38.!Oo`qFycoNQt[P">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="4@1)uM:[oC+|o1;wg/VR0">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id="CgEreB9m]S,jOm_rql=Q">
                                                                                    <field name="value">3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="w0QR,wtrRLnb?Shdmdbr">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="Div:ibgX`kK8H2w)PJ@18">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="]f]TW@2Vc~VA~jdUNuwb(">
                                                                                        <field name="value">500</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="resp_y" id="!_1@2UHEGp.}Kl(#/7P3t">
                                                                                        <field name="value">cy</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="tlAK~1sa#!~;Gvwxk_#~">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="9Jd@2k_k@2@1g;m@2TEFAh2l">
                                                                                            <field name="value">50</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="8|rFc_/wO;B/LT.~leo=">
                                                                                            <field name="value">150</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="65Z(!Sgz2_TvANNdA~s6">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="func_mix" id="XyXC~0qaJ%X;g$$W.Z@12" inline="true">
                                                                                                <field name="collections">1</field>
                                                                                                <field name="variable">$item</field>
                                                                                                <field name="count">3</field>
                                                                                                <field name="mixer">$item</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="-Ng}Uh:)Hy}x4Rx?,]vP">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="hDcOrf,9]vns3|f^}3}g">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="?3!{h+HAQ]t^e-v;W`ha">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="#l+#yAL]$KCj}Y_ECuw$">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="_yc_kAx4j6k`O0}9g@1Wi">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="if_then_block" id="br$if^jY?WB2S,sf#~#d">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="BC!m78J)!od|qCFi#akI">
                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="image_shape" id="!M[!v4vYWg__9evPkWBp">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="p_unl5ns)4t:3TM7VA9%">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="+uAZK7s5`cg%(L]^?#},">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="l7ftXwsuTY:QLC~f;_,5">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="kMrrvY;hyy3)AdYxbqqH">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="HMt=v@1pv9PL;p#Cb$$p[">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="7MW0@2a/D(,?e60=K7jsj">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="}xBXh%VF7!t;oT82!S#N">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="{kSQSYIR.EIJc0|KEL_$">
                                                                                                                                <field name="value">equal.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="VQK[X[;7XnJ`E;W,R+!k">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="o~[{gmGyVkD(-gpf|`PQ">
                                                                                                                                    <field name="value">${image_path}${tem}_equal.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="grid_shape" id="X7U6U@1TJ0z|{gsB_wEc0">
                                                                                <statement name="#props">
                                                                                  <block type="prop_grid_dimension" id="/Pni@2-F?(@1Xtrc}XKaqa">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="rows">
                                                                                      <block type="expression" id="^!BUl.OCKJnss]aD4hkn">
                                                                                        <field name="value">3</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="cols">
                                                                                      <block type="expression" id="iaPfvv`6)ejXWGpV;QaO">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="p8P9RUsbs9a5/]{`+]hO">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="+xV(mJ$~O$@2V979/7$E,">
                                                                                            <field name="value">600</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="resp_y" id="#X-v@1qRPvVpj,/^gHY@1X">
                                                                                            <field name="value">cy</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="$D@1ZacZs!L=QktPfS7?,">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="%kG(uI_{XB#G^ImB}|/I">
                                                                                                <field name="value">50</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="5s,Qti}^#hgUi+Hz@2H^d">
                                                                                                <field name="value">150</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="UQ4py$Jr;)|;^1148qlI">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="func_mix" id="N`dR064=h}!aWZtmV///" inline="true">
                                                                                                    <field name="collections">1</field>
                                                                                                    <field name="variable">$item</field>
                                                                                                    <field name="count">3</field>
                                                                                                    <field name="mixer">$item</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="L)Uq!!DS/BHMzxn-km)=">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id=".8(o/,oQqmmCMAWo47no">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="gKsNMmzYk_x^QTl|/Ani">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id="_TkQNG4lUV5+O%://4-d">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="M0G)u|cciGT)#hGlMwpa">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="if_then_block" id="vqJ~hY0}9mp;7Lrrg2_?">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="?fT:`f4jZVg5}Xu;/b0(">
                                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="list_shape" id="yM6$celIUFr=8YF:%NTp">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_direction" id="hD_YLi45`@2@1j[n@2TQ/JL">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="u3R3Ld]$+i(tcrI2[-AC">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="ZzE7?.|S-L3$1_E6.S@1^">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="il/Mk?@24Exjn};.dj=qn">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="PW^}:MW6XII@1K(`ngF$s">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="Kj(w=53x,M3frXe;AkA8">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="z/IEg;E00]PejveHAZHn">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="items">
                                                                                                                      <block type="list_item_shape" id="R)f2poJJs5EA40pUzTQ[">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="LNR~7kyjbB?-2`D,D)/~">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_list_item_source" id="BwVze9d5P:l0`euNCsTv">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="source">
                                                                                                                                  <block type="func_characters_of" id="-v6RE,J8@2GBtteGr}X)Y" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="/(FTFHO@1yq@1|-r`}ujI`">
                                                                                                                                        <field name="value">a@2c</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id="NgV/+`4G@1%;J)u}tP:n}">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="Lz_+Y{OSHd(7D8EqU0@15">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="A(kNDs~;:w/$qnC!ru`l">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="r?c89~Ejrzafj~j:A|7c">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="Ni#[m6A#V`L~`g@19D$MG">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="l@1`taCDV`cv41=8Vv-;E">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="z$J0v+PT9d%@10FHey}Pg">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="k,G`a@1-RC6Z,NmAG$2]N">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="HP{EK.J[Tk/v2wqQCo31">
                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="UDVA:P~QzlO-gzK@2gWI^">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="te(M5jc_mP-qRy,Ac9|2">
                                                                                                                                                <field name="value">${image_path}${tem}_${$item.data}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="if_then_block" id="HyOm.1@1OB@16uTUZK[O{G">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="v]bxitrE1l2%bQe:GdRU">
                                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="8zCN@1u+c$o}~+lGQ@14wm">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="k[Ybo~4+(uT,1Hn;jI34">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="^9:D|$f$+Z8u~$K`{g!L">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="d!AuJY[?|zmt/^k@20{xY">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="g0-EHF+PIqe)2/dM6ly7">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="($-E@2vU/]13skRW@1c57I">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="r{HD,dr6g,!3RZk7y!ZB">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="/$~R5eFrVRG!!%cpH6yv">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="0BZ,JPg3P#y[gapoj(wO">
                                                                                                                                        <field name="value">line.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="wwE7Z080n8jS?.FAnDu3">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="LXl?d+D/tsd@2XQ{YrQi$">
                                                                                                                                            <field name="value">${image_path}${tem}_line.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="if_then_block" id="E?][]bf]8#+7lBkQj6[Y">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="^@11G|G0fiV+RG@1SL;-h(">
                                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="list_shape" id="[)H|1B5Wi5}kJPyU,-Eo">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_list_direction" id="i9Z@1@27eLUnxT4@2Swe!@1X">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_position" id="sZ.WESBfX]$CC(VB$:e4">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="X6Nm(7_-GLwm;%%BPJB|">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="!Tf@1Ni[]@29:uY5R~WQVu">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="Pm36cr8x[4IPx@2WD+2El">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="oW_D3KbGj[+Q@1N5(i!;8">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="gC#+;WIHJNu,+Zow(!|P">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="items">
                                                                                                                              <block type="list_item_shape" id="|y`i@2Do}#z@1|ykR(Hd0|">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_list_align" id="YDoO{NXc+sALoW0@2N$6#">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="align">middle</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_list_item_source" id="l{G{fh4gLVJjKY%jbr7^">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="source">
                                                                                                                                          <block type="func_characters_of" id="@2,7XLhIXn/X1Wei+7sGO" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="io{0sAjp|I3kI3dF_9zr">
                                                                                                                                                <field name="value">b@2c</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <statement name="template">
                                                                                                                                  <block type="image_shape" id="9gX+%DVo?ANFiaWkI6mi">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="BZ+8)FuGV@1v!9S2=C{I`">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="7C@2KYu/%+1u=PF|0VhL%">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="9SG_01W@2F[Ns(|5JN@27k">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="|5,uLa%G[BB?UaD`wyW(">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="@110xVu#/1QAx)+TzQH9U">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="z@1#?BM8q?.tLOM9h|j">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="!X-g@2#si/y1@2L:Aj,jHK">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="mboTWq4x9S@1)QPAxj7(r">
                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="=aI3K13cX.t?vb%,RdkO">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="9SyWe/#oR6+Er]E!9moR">
                                                                                                                                                        <field name="value">${image_path}${tem}_${$item.data}.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="partial_explanation" id="L+$,5BTp/[vxf)U=?L}5">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="ASAD(8Gx@2eUsl;q;De]]">
                                                                                        <field name="value">&lt;u&gt;We find an equivalent fraction by multiplying the numerator and denominator by the same number.&lt;/u&gt;&lt;/br&gt;

  &lt;style&gt;
  .frac {margin-top: 15px;}
  table{display: inline-block;}
td{text-align: center; padding-right: 10px;}
  &lt;/style&gt;

To find the correct multiple, we divide the larger equivalent fraction. &lt;/br&gt;

Numerator: ${a@2c} : ? = ${a}&lt;/br&gt;
Denominator: ${b@2c} : ? = ${b}&lt;/br&gt;

From here, we know the correct multiple is ${c}.
&lt;/br&gt;&lt;/br&gt;
So the answer is:&lt;/br&gt;
&lt;table&gt;&lt;tr&gt;&lt;td&gt;
${print_frac(a, b)}&lt;/td&gt;&lt;td&gt;x&lt;/td&gt;&lt;td&gt;${print_frac(c, c)}&lt;/td&gt;&lt;td&gt;=&lt;/td&gt;&lt;td&gt;${print_frac(a@2c, b@2c)}
&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="Z@2;hqfm01}4vyGVl@2mAg"></block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */