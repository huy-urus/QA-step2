
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.FDP.FRAC.ORDER.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.NA.FDP.FRAC.ORDER.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "statement",
        "value": "function check_fracexist(nu, de, A){\n    for(var i=0; i<A.length; i++){\n        if(nu/de == A[i].nu/A[i].de){\n            return 1;\n        }\n    }\n    return 0;\n}\n\n\nfunction HCF(a, b){\n    var max, min, temp;\n    if(a > b) {max =a; min =b;}\n    else {max =b; min =a;}\n    while(max != min) {\n        temp = max - min;\n        if(temp >= min){\n            max =temp;\n        }\n        else {\n            max = min;\n            min = temp;\n        }\n    }\n    return max;\n}\n\nfunction HLF(a, b){\n    return a*b/HCF(a, b);\n}\n\nfunction check_maxDe(frac){\n    if(frac.length == 3){\n        if(HLF(frac[0].de, HLF(frac[1].de, HLF(frac[2].de, frac[3].de))) > 300)\n        return 1;\n    }\n    if(frac.length == 5){\n        if(HLF(frac[0].de, HLF(frac[1].de, HLF(frac[2].de, HLF(frac[3].de, frac[4].de)))) > 300)\n        return 1;\n    }\n    return 0;\n}\n\nvar frac = [\n    {nu: 0, de: 1},\n    {nu: 0, de: 1},\n    {nu: 0, de: 1},\n    {nu: 0, de: 1},\n    {nu: 0, de: 1}\n  ];"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "num",
            "value": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < num"
                },
                "do": [
                  {
                    "#type": "do_while_block",
                    "do": [
                      {
                        "#type": "variable",
                        "name": "a",
                        "value": {
                          "#type": "random_number",
                          "min": {
                            "#type": "expression",
                            "value": "1"
                          },
                          "max": {
                            "#type": "expression",
                            "value": "4"
                          }
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "b",
                        "value": {
                          "#type": "random_number",
                          "min": {
                            "#type": "expression",
                            "value": "a+1"
                          },
                          "max": {
                            "#type": "expression",
                            "value": "10"
                          }
                        }
                      }
                    ],
                    "while": {
                      "#type": "expression",
                      "value": "check_fracexist(a, b, frac) == 1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "frac[i].nu",
                    "value": {
                      "#type": "expression",
                      "value": "a/HCF(a, b)"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "frac[i].de",
                    "value": {
                      "#type": "expression",
                      "value": "b/HCF(a, b)"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              }
            ],
            "while": {
              "#type": "expression",
              "value": "check_maxDe(frac) == 1"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "5"
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "i < num"
                    },
                    "do": [
                      {
                        "#type": "do_while_block",
                        "do": [
                          {
                            "#type": "variable",
                            "name": "b",
                            "value": {
                              "#type": "random_number",
                              "min": {
                                "#type": "expression",
                                "value": "5"
                              },
                              "max": {
                                "#type": "expression",
                                "value": "25"
                              }
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "a",
                            "value": {
                              "#type": "random_number",
                              "min": {
                                "#type": "expression",
                                "value": "4"
                              },
                              "max": {
                                "#type": "expression",
                                "value": "b -1"
                              }
                            }
                          }
                        ],
                        "while": {
                          "#type": "expression",
                          "value": "check_fracexist(a, b, frac) == 1"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "frac[i].nu",
                        "value": {
                          "#type": "expression",
                          "value": "a/HCF(a, b)"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "frac[i].de",
                        "value": {
                          "#type": "expression",
                          "value": "b/HCF(a, b)"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "i",
                        "value": {
                          "#type": "expression",
                          "value": "i+1"
                        }
                      }
                    ]
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "check_maxDe(frac) == 1"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "5"
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "i < num"
                    },
                    "do": [
                      {
                        "#type": "do_while_block",
                        "do": [
                          {
                            "#type": "variable",
                            "name": "ba",
                            "value": {
                              "#type": "random_number",
                              "min": {
                                "#type": "expression",
                                "value": "15"
                              },
                              "max": {
                                "#type": "expression",
                                "value": "50"
                              }
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "b",
                            "value": {
                              "#type": "random_number",
                              "min": {
                                "#type": "expression",
                                "value": "10"
                              },
                              "max": {
                                "#type": "expression",
                                "value": "b-1"
                              }
                            }
                          }
                        ],
                        "while": {
                          "#type": "expression",
                          "value": "check_fracexist(a, b, frac) == 1"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "frac[i].nu",
                        "value": {
                          "#type": "expression",
                          "value": "a/HCF(a, b)"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "frac[i].de",
                        "value": {
                          "#type": "expression",
                          "value": "b/HCF(a, b)"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "i",
                        "value": {
                          "#type": "expression",
                          "value": "i+1"
                        }
                      }
                    ]
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "check_maxDe(frac) == 1"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "statement",
        "value": "function asc_fraction(obj){\n    var O = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];\n    for(var i = 0; i < num; i++){\n        O[i].nu = obj[i].nu;\n        O[i].de = obj[i].de;\n    }\n    for(var i = 0; i< num; i++){\n        for(var j = i; j< num; j++){\n            if(O[i].nu/O[i].de > O[j].nu/O[j].de){\n                var temp = O[i].nu;\n                O[i].nu = O[j].nu;\n                O[j].nu = temp;\n\n                var temp = O[i].de;\n                O[i].de = O[j].de;\n                O[j].de = temp;\n            }\n        }\n    }\n    return O;\n}\n\nfunction dec_fraction(obj){\n    var O = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];\n    for(var i = 0; i < num; i++){\n        O[i].nu = obj[i].nu;\n        O[i].de = obj[i].de;\n    }\n    for(var i = 0; i< num; i++){\n        for(var j = i; j< num; j++){\n            if(O[i].nu/O[i].de < O[j].nu/O[j].de){\n                var temp = O[i].nu;\n                O[i].nu = O[j].nu;\n                O[j].nu = temp;\n\n                var temp = O[i].de;\n                O[i].de = O[j].de;\n                O[j].de = temp;\n            }\n        }\n    }\n    return O;\n}"
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Arrange the fractions below in the right order."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "150"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "150"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "box.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}box.png"
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "num"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "150"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "600"
            },
            "height": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "num"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "expression",
                  "value": "frac[$cell.data].nu"
                }
              },
              {
                "#type": "variable",
                "name": "b",
                "value": {
                  "#type": "expression",
                  "value": "frac[$cell.data].de"
                }
              },
              {
                "#type": "choice_custom_shape",
                "action": "drag",
                "value": {
                  "#type": "expression",
                  "value": "a / b"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "grid_shape",
                      "#props": [
                        {
                          "#type": "prop_grid_dimension",
                          "#prop": "",
                          "rows": {
                            "#type": "expression",
                            "value": "3"
                          },
                          "cols": {
                            "#type": "expression",
                            "value": "1"
                          }
                        },
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "100"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "100"
                          }
                        },
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_grid_cell_source",
                          "#prop": "cell.source",
                          "value": {
                            "#type": "expression",
                            "value": "[0, 1, 2]"
                          }
                        },
                        {
                          "#type": "prop_grid_random",
                          "#prop": "",
                          "random": false
                        },
                        {
                          "#type": "prop_grid_show_borders",
                          "#prop": "#showBorders",
                          "value": false
                        },
                        {
                          "#type": "prop_grid_cell_template_for",
                          "variable": "$cell",
                          "condition": "$cell.row == 0",
                          "#prop": "cell.templates[]",
                          "#callback": "$cell",
                          "body": [
                            {
                              "#type": "grid_shape",
                              "#props": [
                                {
                                  "#type": "prop_grid_dimension",
                                  "#prop": "",
                                  "rows": {
                                    "#type": "expression",
                                    "value": "1"
                                  },
                                  "cols": {
                                    "#type": "expression",
                                    "value": "a > 9 ? 2 : 1"
                                  }
                                },
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_anchor",
                                  "#prop": "anchor",
                                  "x": {
                                    "#type": "expression",
                                    "value": "0.5"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "0.5"
                                  }
                                },
                                {
                                  "#type": "prop_grid_cell_size",
                                  "#prop": "cell.size",
                                  "width": {
                                    "#type": "expression",
                                    "value": "36"
                                  },
                                  "height": {
                                    "#type": "expression",
                                    "value": "37"
                                  },
                                  "spacing": {
                                    "#type": "expression",
                                    "value": "0"
                                  },
                                  "radius": {
                                    "#type": "expression",
                                    "value": "0"
                                  }
                                },
                                {
                                  "#type": "prop_grid_cell_source",
                                  "#prop": "cell.source",
                                  "value": {
                                    "#type": "expression",
                                    "value": "a > 9 ? [Math.floor(a/10), a % 10] : [a]"
                                  }
                                },
                                {
                                  "#type": "prop_grid_random",
                                  "#prop": "",
                                  "random": false
                                },
                                {
                                  "#type": "prop_grid_show_borders",
                                  "#prop": "#showBorders",
                                  "value": false
                                },
                                {
                                  "#type": "prop_grid_cell_template",
                                  "variable": "$cell",
                                  "#prop": "cell.template",
                                  "#callback": "$cell",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "$cell.centerX"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "$cell.centerY"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$cell.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$cell.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "#type": "prop_grid_cell_template_for",
                          "variable": "$cell",
                          "condition": "$cell.row == 1",
                          "#prop": "cell.templates[]",
                          "#callback": "$cell",
                          "body": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "frac.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}frac.png"
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "#type": "prop_grid_cell_template_for",
                          "variable": "$cell",
                          "condition": "$cell.row == 2",
                          "#prop": "cell.templates[]",
                          "#callback": "$cell",
                          "body": [
                            {
                              "#type": "grid_shape",
                              "#props": [
                                {
                                  "#type": "prop_grid_dimension",
                                  "#prop": "",
                                  "rows": {
                                    "#type": "expression",
                                    "value": "1"
                                  },
                                  "cols": {
                                    "#type": "expression",
                                    "value": "b > 9 ? 2 : 1"
                                  }
                                },
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_anchor",
                                  "#prop": "anchor",
                                  "x": {
                                    "#type": "expression",
                                    "value": "0.5"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "0.5"
                                  }
                                },
                                {
                                  "#type": "prop_grid_cell_size",
                                  "#prop": "cell.size",
                                  "width": {
                                    "#type": "expression",
                                    "value": "36"
                                  },
                                  "height": {
                                    "#type": "expression",
                                    "value": "37"
                                  },
                                  "spacing": {
                                    "#type": "expression",
                                    "value": "0"
                                  },
                                  "radius": {
                                    "#type": "expression",
                                    "value": "0"
                                  }
                                },
                                {
                                  "#type": "prop_grid_cell_source",
                                  "#prop": "cell.source",
                                  "value": {
                                    "#type": "expression",
                                    "value": "b > 9 ? [Math.floor(b/10), b % 10] : [b]"
                                  }
                                },
                                {
                                  "#type": "prop_grid_random",
                                  "#prop": "",
                                  "random": false
                                },
                                {
                                  "#type": "prop_grid_show_borders",
                                  "#prop": "#showBorders",
                                  "value": false
                                },
                                {
                                  "#type": "prop_grid_cell_template",
                                  "variable": "$cell",
                                  "#prop": "cell.template",
                                  "#callback": "$cell",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_position",
                                          "#prop": "",
                                          "x": {
                                            "#type": "expression",
                                            "value": "$cell.centerX"
                                          },
                                          "y": {
                                            "#type": "expression",
                                            "value": "$cell.centerY"
                                          }
                                        },
                                        {
                                          "#type": "prop_size",
                                          "#prop": "",
                                          "width": {
                                            "#type": "expression",
                                            "value": "0"
                                          },
                                          "height": {
                                            "#type": "expression",
                                            "value": "0"
                                          }
                                        },
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$cell.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$cell.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "ans",
        "value": {
          "#type": "expression",
          "value": "type == 1 ? asc_fraction(frac) : dec_fraction(frac)"
        }
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "num"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "800"
            },
            "height": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "num"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "150"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "shape.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}shape.png"
                  }
                ]
              },
              {
                "#type": "drop_shape",
                "multiple": false,
                "resultMode": "default",
                "groupName": "",
                "#props": [
                  {
                    "#type": "prop_name",
                    "#prop": "",
                    "name": "shape_name"
                  },
                  {
                    "#type": "prop_value",
                    "#prop": "",
                    "value": {
                      "#type": "expression",
                      "value": "ans[$cell.data].nu / ans[$cell.data].de"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "116"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "159"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col > 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX - $cell.width/2"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${type == 1 ? \"small\": \"lager\"}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${type == 1 ? \"small\": \"lager\"}.png"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "var listDen = []\nfor(var i =0; i< num; i++){\n    listDen[i] = frac[i].de;\n}\n\nfunction re_max(A){\n    var max = 0;\n    for(var i = 0; i< num; i++){\n        if(A[i] > max) max = A[i];\n    }\n    return max;\n}\n\nfunction check_after(z, A){\n    for(var i = 0; i< z; i++){\n        if(A[i] == A[z]) return 1;\n    }\n    return 0;\n}\n\nfunction check_divide(a, b){\n    if(a % b == 0) return 1;\n    return 0;\n}\n\nfunction check_exist(x, A){\n    for(var i=0; i<A.length; i++){\n        if(x == A[i]){\n            return 1;\n        }\n    }\n    return 0;\n}\n\nfunction print_frac(a, b){\n    return \"<table class='frac'><tr><td style='border-bottom: 2px solid #6d4115'>\" + a + \"</td></tr><tr><td>\" + b + \"</td></tr></table>\";\n}\n\nfunction get_listCannot(a, A){\n    var cannot = [];\n    for(var i = 0; i< num; i++){\n        if(check_divide(a, A[i]) == 0 && check_exist(A[i], cannot) == 0) cannot.push(A[i])\n    }\n    return cannot;\n}\n\nfunction get_listCan(a, A){\n    var can = [];\n    for(var i = 0; i< num; i++){\n        if(check_divide(a, A[i]) == 1 && a != A[i] && check_exist(A[i], can) == 0) can.push(A[i])\n    }\n    return can;\n}\n\nfunction get_min(A){\n    var min = A[0];\n    for(var i = 1; i< A.length; i++){\n        if(A[i] < min) min = A[i];\n    }\n    return min;\n}\n\nfunction delete_array(a, A){\n    for(var i = 0; i<A.length; i++){\n        if(A[i] == a){\n            for(var j = i; j<A.length-1; j++){\n                A[j] = A[j+1];\n            }\n            A.pop();\n            i = 0;\n        }\n    }\n    return A;\n}\n\nfunction print_arr(A){\n    var str = '';\n    for(var i = 0; i< A.length; i++){\n        str+=A[i];\n        if(i < A.length - 2) str+=', ';\n        if(i == A.length - 2) str+=' and ';\n    }\n    return str;\n}\n\nvar max_de = re_max(listDen);\n\nvar listCannot = get_listCannot(max_de, listDen);\nvar listCan = get_listCan(max_de, listDen);"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Focus on the denominator.</u></br></br>\n\n<style>\n  .frac {text-align: center; display: inline-block; margin: 10px}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${print_frac(frac[i].nu, frac[i].de)}${'  '}"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</br></br>\nAll the denominators are</br></br>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${check_after(i, listDen) == 0 ?  listDen[i] : ''}${' '}"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Look at the biggest denominator.</u></br>\n\n<style>\n  .frac {text-align: center; display: inline-block; margin: 10px}\n  </style>\n\n${max_de} is the biggest denominator.</br></br>\nFind the multiples.</br>\n${listCan.length != 0 ? max_de + \" can divide by \" + print_arr(listCan) : \"\"}${listCan.length != 0 ? \", but \": \"\"}${listCannot.length != 0 ? max_de + \" cannot divide by \" + print_arr(listCannot) : \"\"}.</br></br>"
        ]
      },
      {
        "#type": "statement",
        "value": "do {"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "${max_de} x ${max_de*get_min(listCannot)/HCF(max_de,get_min(listCannot))/max_de} = ${max_de*get_min(listCannot)/HCF(max_de,get_min(listCannot))}"
        ]
      },
      {
        "#type": "variable",
        "name": "max_de",
        "value": {
          "#type": "expression",
          "value": "max_de*get_min(listCannot)/HCF(max_de,get_min(listCannot))"
        }
      },
      {
        "#type": "statement",
        "value": "var arr = [];\n\nfor(var i = 0; i < listCannot.length; i++){\n  if(check_divide(max_de, listCannot[i]) == 1){\n    listCan.push(listCannot[i]);\n    arr.push(i);\n  }\n}\n\nvar cannot_temp = [];\nfor(var i = 0; i< listCannot.length; i++){\n  cannot_temp[i] = listCannot[i];\n}\n\nfor(var i = 0; i< arr.length; i++){\n  if(check_exist(cannot_temp[arr[i]], listCan) == 1){\n    listCannot = delete_array(cannot_temp[arr[i]], listCannot)\n  }\n}"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</br>\n${listCan.length != 0 ? max_de + \" can divide by \" + print_arr(listCan) : \"\"}${listCannot.length != 0 ? \", but then again, \" : \".\"}${listCannot.length != 0 ? max_de + \" cannot divide by \" + print_arr(listCannot) + \".\": \"\"}</br></br>"
        ]
      },
      {
        "#type": "statement",
        "value": "}\nwhile (listCannot.length != 0)"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<b>${max_de}</b> can divide by all denominators."
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Make each denominator equal ${max_de} without changing each fraction value.</u></br></br>\n\n<style>\n  .frac {text-align: center; display: inline-block; margin: 10px;}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${print_frac(frac[i].nu, frac[i].de)} <table class='frac'><tr><td style='height: 100px'>x</td></tr></table> ${print_frac(max_de/listDen[i], max_de/listDen[i])} <table class='frac'><tr><td style='height: 100px'>=</td></tr></table> ${print_frac(frac[i].nu*max_de/listDen[i], max_de)}</br>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "statement",
        "value": "var max_frac = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];\nfor(var i =0; i< num; i++){\n    max_frac[i].nu = frac[i].nu*max_de/listDen[i];\n    max_frac[i].de = max_de;\n}\n\nif(type == 1)  max_frac = asc_fraction(max_frac);\nelse max_frac = dec_fraction(max_frac);"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 4: Let's compare all fractions together.</u></br></br>\n<style>\n  .frac {text-align: center; display: inline-block; margin: 10px;}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${print_frac(max_frac[i].nu, max_frac[i].de)}"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i < num - 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<table class='frac'><tr><td style='height: 100px'>${type == 1 ? \"<\" : \">\"}</td></tr></table>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</br>What does that mean?</br>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${print_frac(ans[i].nu, ans[i].de)}"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i < num - 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<table class='frac'><tr><td style='height: 100px'>${type == 1 ? \"<\" : \">\"}</td></tr></table>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.NA.FDP.FRAC.ORDER.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.NA.FDP.FRAC.ORDER.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="statement" id="q%y6[Wg!_n`gQm,]daG6">
                                        <field name="value">function check_fracexist(nu, de, A){
    for(var i=0; i&lt;A.length; i++){
        if(nu/de == A[i].nu/A[i].de){
            return 1;
        }
    }
    return 0;
}


function HCF(a, b){
    var max, min, temp;
    if(a &gt; b) {max =a; min =b;}
    else {max =b; min =a;}
    while(max != min) {
        temp = max - min;
        if(temp &gt;= min){
            max =temp;
        }
        else {
            max = min;
            min = temp;
        }
    }
    return max;
}

function HLF(a, b){
    return a@2b/HCF(a, b);
}

function check_maxDe(frac){
    if(frac.length == 3){
        if(HLF(frac[0].de, HLF(frac[1].de, HLF(frac[2].de, frac[3].de))) &gt; 300)
        return 1;
    }
    if(frac.length == 5){
        if(HLF(frac[0].de, HLF(frac[1].de, HLF(frac[2].de, HLF(frac[3].de, frac[4].de)))) &gt; 300)
        return 1;
    }
    return 0;
}

var frac = [
    {nu: 0, de: 1},
    {nu: 0, de: 1},
    {nu: 0, de: 1},
    {nu: 0, de: 1},
    {nu: 0, de: 1}
  ];
                                        </field>
                                        <next>
                                          <block type="if_then_else_block" id="@1ZFBBTnIU41VLYd%sr)p">
                                            <value name="if">
                                              <block type="expression" id="8Xli+l{mXhGQ@1qSU?yMV">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="Xbo?Wim872yY3lg0O!hD">
                                                <field name="name">num</field>
                                                <value name="value">
                                                  <block type="expression" id="u+qig:Py:U(/g.M4XnZr">
                                                    <field name="value">3</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="do_while_block" id="Y7bCs~G^[5H-5mib8^})">
                                                    <statement name="do">
                                                      <block type="variable" id="WX-]#q0aJ2Rf=vjUtC9`">
                                                        <field name="name">i</field>
                                                        <value name="value">
                                                          <block type="expression" id="z)|h1(K9!fa{O@11@1N@1O~">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="while_do_block" id="|,r%l#T;1yc|4xdSf96l">
                                                            <value name="while">
                                                              <block type="expression" id="s@2-mFm@2^p^$9TT(V9O^A">
                                                                <field name="value">i &lt; num</field>
                                                              </block>
                                                            </value>
                                                            <statement name="do">
                                                              <block type="do_while_block" id="T?IEWZTdf7n0fehGX@2+c">
                                                                <statement name="do">
                                                                  <block type="variable" id="gQ5MQ(hBT!L8+obBRN~A">
                                                                    <field name="name">a</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="[tcAMYZr.6x=YFg)fV31">
                                                                        <value name="min">
                                                                          <block type="expression" id="@2Q=!s/Eiq[h/Db5P/|sS">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id=";?V5{r]]kcBoFgKvhd7D">
                                                                            <field name="value">4</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="-@2/50rN0+|^y$^Ahyhpo">
                                                                        <field name="name">b</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="jE^MD`$o|IIHAO#}ooC9">
                                                                            <value name="min">
                                                                              <block type="expression" id="LM{L./;tprS:_cJ73{DL">
                                                                                <field name="value">a+1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="NCX:~oF^/j|F{RY1oI[`">
                                                                                <field name="value">10</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <value name="while">
                                                                  <block type="expression" id="C]@2P0WQ)LsAV:{f(3I{J">
                                                                    <field name="value">check_fracexist(a, b, frac) == 1</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="Il5#[ST!SSdwVxvf[y0|">
                                                                    <field name="name">frac[i].nu</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="d=t7N480$vT4fZ8MVMh#">
                                                                        <field name="value">a/HCF(a, b)</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="`?Yo?43I^371/6NSnnL2">
                                                                        <field name="name">frac[i].de</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="o2OW:6Tu@2g!HW!1MvJJY">
                                                                            <field name="value">b/HCF(a, b)</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="/pQFCpKYEIXrn/nSU`5X">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="lPmk-$z~7^3lP5I,w4WT">
                                                                                <field name="value">i+1</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <value name="while">
                                                      <block type="expression" id="WjOEGp#cB?K^p.}h,{b#">
                                                        <field name="value">check_maxDe(frac) == 1</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id=":lInX@26GKj_E_!9XgDIp">
                                                <value name="if">
                                                  <block type="expression" id="@2-85~oE[$KKRnIk?;V:h">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="M`{`@2Ouyi^LfH@2S%@2%yo">
                                                    <field name="name">num</field>
                                                    <value name="value">
                                                      <block type="expression" id="|x,jt|aYe15QaQl(B{CZ">
                                                        <field name="value">5</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="do_while_block" id="OBWIU0cu-9yvX!)%!uRG">
                                                        <statement name="do">
                                                          <block type="variable" id="N29JHg5S%)MMf-0I]y;z">
                                                            <field name="name">i</field>
                                                            <value name="value">
                                                              <block type="expression" id=".g07?^jneHs1UV=}Fs`]">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="while_do_block" id="zJ},o#]2wg(+F@1Hk5f]G">
                                                                <value name="while">
                                                                  <block type="expression" id="%R4@10m+/MXz;TqcV7}/!">
                                                                    <field name="value">i &lt; num</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="do">
                                                                  <block type="do_while_block" id="ZR`aC+aRnHTp_,YlK`=U">
                                                                    <statement name="do">
                                                                      <block type="variable" id="B7~;9Sj__a]Gh+otMf-f">
                                                                        <field name="name">b</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="I[!^JW_Z2H9kl:w4R8zz">
                                                                            <value name="min">
                                                                              <block type="expression" id="K5Ss`bW[)jQ/u+1jq-+]">
                                                                                <field name="value">5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="Mu2BMsD.vEWmy@22Vm_-:">
                                                                                <field name="value">25</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="iw1{.3Xl_6t]qm{6nbAW">
                                                                            <field name="name">a</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="}@2.+=H0Yat)#saMdbCs;">
                                                                                <value name="min">
                                                                                  <block type="expression" id="Rh+sy6mcmsvgO3f@21n#y">
                                                                                    <field name="value">4</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="(}[MKg-deaYa((M{2v2L">
                                                                                    <field name="value">b -1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <value name="while">
                                                                      <block type="expression" id="oRFY2xeO5f#n[%m|Tiq3">
                                                                        <field name="value">check_fracexist(a, b, frac) == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="E{siJFf}/+hqr|!!56l=">
                                                                        <field name="name">frac[i].nu</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="@2Hbh@2ox5TG??aAQ20($(">
                                                                            <field name="value">a/HCF(a, b)</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="l_zEUTe`W^18r~poIrO%">
                                                                            <field name="name">frac[i].de</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="@1q+B~;D.8obu=^,4Nw@1:">
                                                                                <field name="value">b/HCF(a, b)</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="Xm3u3xLL6g7dMq^6E)2,">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="w$U@1Z;mRri!WHpj#5@1zm">
                                                                                    <field name="value">i+1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <value name="while">
                                                          <block type="expression" id="@241CDbx!XKB-5;lcGf_o">
                                                            <field name="value">check_maxDe(frac) == 1</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="V=|/1]XO=Q;a_SQNvh0A">
                                                    <field name="name">num</field>
                                                    <value name="value">
                                                      <block type="expression" id="@1R;MH]`Zz9ph?rf=oCM(">
                                                        <field name="value">5</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="do_while_block" id="1NG!CG8o3`$dV4Kw!7=D">
                                                        <statement name="do">
                                                          <block type="variable" id="B/JaRpnB5S]phnWq9/7@2">
                                                            <field name="name">i</field>
                                                            <value name="value">
                                                              <block type="expression" id="-yJjx{T@2A19Ax?j4M~]9">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="while_do_block" id="KxmK}%bw{c`w_[@2O/m4]">
                                                                <value name="while">
                                                                  <block type="expression" id="]W(?TYogtr?FzO1tcbX@2">
                                                                    <field name="value">i &lt; num</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="do">
                                                                  <block type="do_while_block" id="r#r$JZ~?.^MNU[TJ5~Om">
                                                                    <statement name="do">
                                                                      <block type="variable" id="@28X[SAl3R7UCzlq=kr`e">
                                                                        <field name="name">ba</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="[Q(i]~x#f+I3;z^DEm?Q">
                                                                            <value name="min">
                                                                              <block type="expression" id="zuRNU.FZ3xVPL8l{%4@28">
                                                                                <field name="value">15</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="-/+(mz0e;E5;]t0V#0.B">
                                                                                <field name="value">50</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="))!2#QHb5XmN1168Sg?U">
                                                                            <field name="name">b</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="~AnEdn]O7(^pSMuV:wg9">
                                                                                <value name="min">
                                                                                  <block type="expression" id="sic05C?U`7k|O$!iu|z{">
                                                                                    <field name="value">10</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="/-?ZqSVG%Gb0QHErPd-h">
                                                                                    <field name="value">b-1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <value name="while">
                                                                      <block type="expression" id="Kut(wrdHMQ#96cAn_3O^">
                                                                        <field name="value">check_fracexist(a, b, frac) == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id=")Bi4kt{zHRqI@2P%@1P|]/">
                                                                        <field name="name">frac[i].nu</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="x}tk#.Y7O`Kyr8RK:?/D">
                                                                            <field name="value">a/HCF(a, b)</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="~YvI:1Sb/eMUW-SOr2u$">
                                                                            <field name="name">frac[i].de</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="j$94/P3a3+b9+(9nICII">
                                                                                <field name="value">b/HCF(a, b)</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="kDn^,u#oFI|Wk-qX]E@1h">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="QQ.n;wtO:gT3p]PFl`;m">
                                                                                    <field name="value">i+1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <value name="while">
                                                          <block type="expression" id="=MS@2~B=@1dMx!?w]@12I+d">
                                                            <field name="value">check_maxDe(frac) == 1</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="Thkd/FG.[tAMRO]eeM.j">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id=".17M0DoH!hgZ(%YkNv`h">
                                                    <value name="min">
                                                      <block type="expression" id="ao^yj)_kr_v@1A^8}KM2j">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="K?!.}?45UyeoI$X6`wo7">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="yhPgPA$/+z2J/5$h?2[U">
                                                    <field name="value">function asc_fraction(obj){
    var O = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];
    for(var i = 0; i &lt; num; i++){
        O[i].nu = obj[i].nu;
        O[i].de = obj[i].de;
    }
    for(var i = 0; i&lt; num; i++){
        for(var j = i; j&lt; num; j++){
            if(O[i].nu/O[i].de &gt; O[j].nu/O[j].de){
                var temp = O[i].nu;
                O[i].nu = O[j].nu;
                O[j].nu = temp;

                var temp = O[i].de;
                O[i].de = O[j].de;
                O[j].de = temp;
            }
        }
    }
    return O;
}

function dec_fraction(obj){
    var O = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];
    for(var i = 0; i &lt; num; i++){
        O[i].nu = obj[i].nu;
        O[i].de = obj[i].de;
    }
    for(var i = 0; i&lt; num; i++){
        for(var j = i; j&lt; num; j++){
            if(O[i].nu/O[i].de &lt; O[j].nu/O[j].de){
                var temp = O[i].nu;
                O[i].nu = O[j].nu;
                O[j].nu = temp;

                var temp = O[i].de;
                O[i].de = O[j].de;
                O[j].de = temp;
            }
        }
    }
    return O;
}
                                                    </field>
                                                    <next>
                                                      <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                        <statement name="#props">
                                                          <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                            <field name="#prop"></field>
                                                            <value name="x">
                                                              <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                <field name="value">400</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                <field name="value">20</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                <field name="#prop">anchor</field>
                                                                <value name="x">
                                                                  <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                    <field name="value">0.5</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                    <field name="#prop"></field>
                                                                    <value name="contents">
                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                        <field name="value">Arrange the fractions below in the right order.</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                        <field name="base">text</field>
                                                                        <statement name="#props">
                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                            <field name="#prop"></field>
                                                                            <value name="fontSize">
                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                <field name="value">36</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                <field name="#prop"></field>
                                                                                <value name="fill">
                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                    <field name="value">black</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="stroke">
                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                        <field name="value">white</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="strokeThickness">
                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="image_shape" id="@1=.)IY/iQ2q?P4N9x;^H">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="21m46q27/@2JaWGOccf}H">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="QcuP0bX?g_4@231iu`GL5">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="c{7?QbNVpp/_aDbU3jS(">
                                                                    <field name="value">150</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="bn#GqdLIu0IPwD?PyJC+">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id=",O@1-HqWvy4z#llz_G=py">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="JVrIH0H:Vj]Il:-1@1Oy+">
                                                                        <field name="value">150</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_image_key" id="LCAIGF@1di+Oz3NEQM9ty">
                                                                        <field name="#prop"></field>
                                                                        <value name="key">
                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                            <field name="value">box.png</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_image_src" id="[4;ZC!z0]))]u@26~@11y{">
                                                                            <field name="#prop"></field>
                                                                            <value name="src">
                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                <field name="value">${image_path}box.png</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                        <field name="value">num</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                            <field name="value">150</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                <field name="value">600</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                <field name="value">100</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                    <field name="#prop">cell.source</field>
                                                                                    <value name="value">
                                                                                      <block type="func_array_of_number" id=".E$@1p1M|9Kul@1n@1ZV+XZ" inline="true">
                                                                                        <value name="items">
                                                                                          <block type="expression" id="e1kt`SaM{k4eQ@2xq_D.w">
                                                                                            <field name="value">num</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="from">
                                                                                          <block type="expression" id="9%L980$u`bdXM2a1oW}1">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="random">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                            <field name="#prop">#showBorders</field>
                                                                                            <field name="value">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="#prop">cell.template</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="variable" id="=fV9OB8za?X|2App_PV%">
                                                                                                    <field name="name">a</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="=h=dZz_]/zmr;w@1{@2.,C">
                                                                                                        <field name="value">frac[$cell.data].nu</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="|SBTpm/B+ZmB{BXA$=Qe">
                                                                                                        <field name="name">b</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="w6Xe}-s^ewZYVMuh88i2">
                                                                                                            <field name="value">frac[$cell.data].de</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="choice_custom_shape" id="a.#zG9we6Y-{byzUNyTt">
                                                                                                            <field name="action">drag</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="(y;jp!P)v|K{ejTM331|">
                                                                                                                <field name="value">a / b</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="template">
                                                                                                              <block type="grid_shape" id="za%@2U7,7Q;+!mmq$xs5v">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="z#g206JXIEHK_pA:X6_u">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id="~Nmr/`%O|F%o[Xgz{2}k">
                                                                                                                        <field name="value">3</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id="},$ECmKt(m2II8%fSn/A">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="fH$!w9O;mQ4kQ7TiER8=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="O}d`Q.kL]#@2#/+T4%Y?P">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="?r}uXF2Yifdy-#N%2pEc">
                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="+f1yf_b?!ga.VB_:4Vb/">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="Uy+n+c,/vrr=?{2)gceg">
                                                                                                                                <field name="value">100</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="EUuv@1R]upgIxxY9407E/">
                                                                                                                                <field name="value">100</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="@2Ej+c2G(Dm]k25RZ@1:d}">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="h^nD)YJ+blcBhn17dL0#">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="8}U+h+MZ?#%t.0v0a9HI">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="(@2yBq^w@2)Zu@2qyUpoywb">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="y3fE]b6qWDxGCgOD.!rq">
                                                                                                                                        <field name="value">[0, 1, 2]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="r8mS88os`@2E7|z`?aOuK">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id=")td6Cm,~@2@1,8zA]xf3[3">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template_for" id="/PtVd:El~cc=EZP!0?7%">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="condition">$cell.row == 0</field>
                                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="grid_shape" id="qh.,5uN-?U=oKTUxl!?s">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_grid_dimension" id="4PGm[TLX7c]jI#w@2gkdg">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="rows">
                                                                                                                                                          <block type="expression" id="vE.0_M]sewA:DBa.MG.#">
                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="cols">
                                                                                                                                                          <block type="expression" id="i7/tzJxy^t4bq4^~hOcM">
                                                                                                                                                            <field name="value">a &gt; 9 ? 2 : 1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_position" id="6AK4+sH,F5X!@1[m4.}Sr">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="VwN?jU!K{S2A5Pcv2kB[">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="S-/!5,5%hZ|=h8N3h!!^">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_anchor" id="q2%ChO@2IoZz63:wMt|fu">
                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="#[S}Yc$;COEzniXh?}d@1">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="uY/^[SWo$;Op{.[Yg0{n">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_cell_size" id="s2OLl;8942}/5r^67}DR">
                                                                                                                                                                    <field name="#prop">cell.size</field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="o}SwYNeS6+YKFitHpA,2">
                                                                                                                                                                        <field name="value">36</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="y+[VdrG;!R?!EW-@1ub3V">
                                                                                                                                                                        <field name="value">37</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="spacing">
                                                                                                                                                                      <block type="expression" id=".M3K60dIm:0DBRmsckK{">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="radius">
                                                                                                                                                                      <block type="expression" id="A4r4_MOou9._U#GA?|dj">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_grid_cell_source" id="2By!1gMrkt(hdLE+ihG[">
                                                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="$h2)Sk82lkxz-/ftyWoP">
                                                                                                                                                                            <field name="value">a &gt; 9 ? [Math.floor(a/10), a % 10] : [a]</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_grid_random" id="}B7k[XopTzzDA|0:)B.v">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_show_borders" id="N!M(eMLtHRC5dDG1d,aM">
                                                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_cell_template" id=",9NLV#c,KX.`PUhGb}}T">
                                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                                    <statement name="body">
                                                                                                                                                                                      <block type="image_shape" id="!Zh;F$Dt|cvdOMmoDUHT">
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_position" id="5CL_.]R7=vKMJ`C,%mpr">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                              <block type="expression" id="uRA|Bhf]sLFq,sx=b:14">
                                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                              <block type="expression" id="0yIUVvAz.[)qy?7sQu#h">
                                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_size" id="bNJWii:[@2udF(sPZ-=h1">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="width">
                                                                                                                                                                                                  <block type="expression" id="R^B_TwVgQ3wfwOy!?XUo">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="height">
                                                                                                                                                                                                  <block type="expression" id=")=5E(mt5$EIxHG5q55-N">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_key" id="0l6@1u;=47._$f{Sr{V;?">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                                      <block type="string_value" id="OfBe)Un1($mW8{gj!Y}T">
                                                                                                                                                                                                        <field name="value">${$cell.data}.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_image_src" id="[gNQ}[T$Q+.pGasHZ#d3">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                                          <block type="string_value" id="DB)RcScpdbFR5FDR|m9U">
                                                                                                                                                                                                            <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template_for" id="QrF2-.wp}#UMXxx5Q)}E">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="condition">$cell.row == 1</field>
                                                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="image_shape" id="EY9/@2^dRtZz@24sjeZ@28?">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="(ADWm+A?(Z]G`45v|j2U">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="tZSBM$)3^J|E!`,hU7?y">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="p|nbvX4echM!v@2`nJWen">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="(+{YwIS/q[;/6.$+Ai5t">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="VMcXXxB/}9q(i;tdPz+q">
                                                                                                                                                                    <field name="value">frac.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="uO7@1+o/Mc?6YVRsk@2@25h">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="p@2-?~sJa@1~7jv+t%|Xs+">
                                                                                                                                                                        <field name="value">${image_path}frac.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_template_for" id="O){@1LR,E0JbczrdM;UpR">
                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                        <field name="condition">$cell.row == 2</field>
                                                                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                        <statement name="body">
                                                                                                                                                          <block type="grid_shape" id="T+;/lu7B7Q{V./:2_9sf">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_grid_dimension" id="tc2BU%BQlQ]iA`{0uH`I">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="rows">
                                                                                                                                                                  <block type="expression" id="KO?qFgFa:6l$eyO0r0d/">
                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="cols">
                                                                                                                                                                  <block type="expression" id=":;VTxWpPXARlk2$]TkRm">
                                                                                                                                                                    <field name="value">b &gt; 9 ? 2 : 1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_position" id="c^:=hvDC9eWQ|G{(QA`z">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="8m5+dYy|}jw/AT=k}Xi]">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="HvCfI=P5!5(16Dn/]?@2;">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_anchor" id="]?gdt0wPhyBFj[G]P6ts">
                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="#2`oiIX#MoRN7K^W/d/s">
                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="{0w7]Uw!NQEqMIzYG}i6">
                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_grid_cell_size" id="S+8J=,G3!-RNCFU[8ojF">
                                                                                                                                                                            <field name="#prop">cell.size</field>
                                                                                                                                                                            <value name="width">
                                                                                                                                                                              <block type="expression" id="{cz()T_WUS+zVAiYQJLn">
                                                                                                                                                                                <field name="value">36</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="height">
                                                                                                                                                                              <block type="expression" id="62ok-egh2I|v:J2c,[4,">
                                                                                                                                                                                <field name="value">37</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="spacing">
                                                                                                                                                                              <block type="expression" id="@2^E1rc=bi-c/cplk]@10@2">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="radius">
                                                                                                                                                                              <block type="expression" id="!g_..bd:2~!V39J([;;f">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_cell_source" id="RzP#@2M8IOQ4!xU|.oVKl">
                                                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="r@1Wa@13kj{Si^ajT/={qp">
                                                                                                                                                                                    <field name="value">b &gt; 9 ? [Math.floor(b/10), b % 10] : [b]</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_random" id="Lu~%c/u2!Z67Y{Rg5yhM">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_grid_show_borders" id="T4Lhw1u`8Zl}[y@1.xFcp">
                                                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_grid_cell_template" id="N.xw.fJhnc3#%=;=J7]M">
                                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                                            <statement name="body">
                                                                                                                                                                                              <block type="image_shape" id="0siN%c?Qa7i5!xHh,Sl@2">
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_position" id="r``dh=OYjwu(;C=m9xZH">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                      <block type="expression" id="Q+pB2!hDT@15e@2uxauSSb">
                                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                      <block type="expression" id="@1r~@28h+7/{Uv+StGi[-1">
                                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_size" id="5z;byAn5z`f23j11{$1A">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                          <block type="expression" id="1,bxTb:M:gSlUeq372Vn">
                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                          <block type="expression" id="mW$-83_tYdP%YjILTLs`">
                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_image_key" id="v{V(gzS|JPOo(]qS6.Yi">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                                              <block type="string_value" id="fFX=0sMJ$_x-!Dw=wP79">
                                                                                                                                                                                                                <field name="value">${$cell.data}.png</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_image_src" id="zf!$Z.RD`?.!@1DDDgs+U">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                                  <block type="string_value" id="XP7l~}ec#@2dBXAB-@1x)h">
                                                                                                                                                                                                                    <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="variable" id="r!N)Q)jlbv~n^9bL7BbJ">
                                                                    <field name="name">ans</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="CX59[l{KVj;WWNA@1S[8C">
                                                                        <field name="value">type == 1 ? asc_fraction(frac) : dec_fraction(frac)</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="grid_shape" id="{sf}D{~faK@2zd`A{.8_A">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="7AueiI8gC{O7}^);Bils">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="yi[PHkyve.#FXWlbiFOh">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="IG?XD0zNn{soC4]tjG;]">
                                                                                <field name="value">num</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="+i3S`1J4H?(v/@1#)RjF8">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="AeeGAYt/eo.!.j@1o`}NB">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="gtIAx]fv;x+2tmIKtx;Y">
                                                                                    <field name="value">350</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="+#4Q@2wY+doiscu4a3ae6">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="T@1AC;PS$4n!Q}krk2L_N">
                                                                                        <field name="value">800</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="^}j):IV1l8KaH0=Aoir1">
                                                                                        <field name="value">100</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="o!?l8?-yO@1r/C^;8#r/O">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="9cA{;BZbhM1mk6fN]Yyv">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="sz3~IHgSc#U3iA7;t1wK">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="$A5lSrIfIZn7(w}qFD%!">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="func_array_of_number" id="5I2kwVbx.C,O;aet%8YB" inline="true">
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="3lYTir7p)Z!oxuezlyV.">
                                                                                                    <field name="value">num</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="from">
                                                                                                  <block type="expression" id="W,Uycf!4o+l;k_D)A/sN">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="Ll/61#Z1B1PY+S@1S_9ld">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="5LRe~uxik@1knUTi~,G-=">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="a%[b+P^GW0]l{zOkbbt-">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="[B#fbjrMrrl#t+)l{bkW">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="C@1%^#h3W:m_Y78$QW+D5">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="nu{8~GY_1Ma@1vq~:~=9k">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="KFiPN1@1N4g,F([v~(jAj">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="gSS7SeNbDnW}Z-5?wr.J">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="awz%R!FdpMZ%cls{YxEP">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="K-02qyGp,M):{To+!f~s">
                                                                                                                        <field name="value">150</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="g(Qbpd,TcD3qOR?(+:8`">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="M5kW|k=G-/U]ST:06pKL">
                                                                                                                            <field name="value">shape.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="0q_[0:D-,+Z:U!7^sf.Y">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="conE;v7-jr`bK^Zb.-~R">
                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="drop_shape" id="_4hK}pbC%wxj#W`rmgeW">
                                                                                                                <field name="multiple">FALSE</field>
                                                                                                                <field name="resultMode">default</field>
                                                                                                                <field name="groupName"></field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_name" id="25~DY#Z;8!,E`(SXba@2B">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="name">shape_name</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_value" id="SurV^`j{|PdM@1WVRg!cn">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="U|Etw#HTBY~f[A~DwTgu">
                                                                                                                            <field name="value">ans[$cell.data].nu / ans[$cell.data].de</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="4!;=cRQS$v01;dGlR57l">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="LAKu@1PSAH{0l#zGe|F4D">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="S^C)HM.~kIRF5_$gdt`k">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="F$kVR2xvrYDcN@1-z.P_P">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="wdp1Kst_wjTYK+OiR,2]">
                                                                                                                                    <field name="value">116</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="F%]t^Qg2)0lX:Kx#!%%y">
                                                                                                                                    <field name="value">159</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="3S]!!uD@1R~1o0l0`pYl$">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="C5Dk;ty+gzTdP.i$!wpQ">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="Vm7~P4/%q@2TI}WnxLaIf">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template_for" id="t8N4KOpq3fpWbUGP@1zpn">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="condition">$cell.col &gt; 0</field>
                                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="image_shape" id="JesLa7(@1}kQLi%NP^dUy">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="4,DBxS$p@25Ng@1q9Q@21X@2">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="a90mwe%yd8EidlL$q+hc">
                                                                                                                        <field name="value">$cell.centerX - $cell.width/2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="`Y0`S,Cj~@2|mx@1K0{7yE">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="Xo`X[u^u7;cjl_87hG/F">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="Tu0:cE)KJAMAf%.[V3nO">
                                                                                                                            <field name="value">${type == 1 ? "small": "lager"}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="jLjGͬ,$PiL|3@1l6_P">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="wL%+])1=bf8ZD(M?@1M7#">
                                                                                                                                <field name="value">${image_path}${type == 1 ? "small": "lager"}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="statement" id="mZ.$(nzj@27FT[W[BWmvR">
                                                                            <field name="value">var listDen = []
for(var i =0; i&lt; num; i++){
    listDen[i] = frac[i].de;
}

function re_max(A){
    var max = 0;
    for(var i = 0; i&lt; num; i++){
        if(A[i] &gt; max) max = A[i];
    }
    return max;
}

function check_after(z, A){
    for(var i = 0; i&lt; z; i++){
        if(A[i] == A[z]) return 1;
    }
    return 0;
}

function check_divide(a, b){
    if(a % b == 0) return 1;
    return 0;
}

function check_exist(x, A){
    for(var i=0; i&lt;A.length; i++){
        if(x == A[i]){
            return 1;
        }
    }
    return 0;
}

function print_frac(a, b){
    return "&lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='border-bottom: 2px solid #6d4115'&gt;" + a + "&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;" + b + "&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;";
}

function get_listCannot(a, A){
    var cannot = [];
    for(var i = 0; i&lt; num; i++){
        if(check_divide(a, A[i]) == 0 &amp;&amp; check_exist(A[i], cannot) == 0) cannot.push(A[i])
    }
    return cannot;
}

function get_listCan(a, A){
    var can = [];
    for(var i = 0; i&lt; num; i++){
        if(check_divide(a, A[i]) == 1 &amp;&amp; a != A[i] &amp;&amp; check_exist(A[i], can) == 0) can.push(A[i])
    }
    return can;
}

function get_min(A){
    var min = A[0];
    for(var i = 1; i&lt; A.length; i++){
        if(A[i] &lt; min) min = A[i];
    }
    return min;
}

function delete_array(a, A){
    for(var i = 0; i&lt;A.length; i++){
        if(A[i] == a){
            for(var j = i; j&lt;A.length-1; j++){
                A[j] = A[j+1];
            }
            A.pop();
            i = 0;
        }
    }
    return A;
}

function print_arr(A){
    var str = '';
    for(var i = 0; i&lt; A.length; i++){
        str+=A[i];
        if(i &lt; A.length - 2) str+=', ';
        if(i == A.length - 2) str+=' and ';
    }
    return str;
}

var max_de = re_max(listDen);

var listCannot = get_listCannot(max_de, listDen);
var listCan = get_listCan(max_de, listDen);
                                                                            </field>
                                                                            <next>
                                                                              <block type="partial_explanation" id="]xAavb1Q;M(@1T@2d`@2r4/" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="[gewch,nq=Qf,f{Qx}SM">
                                                                                    <field name="value">&lt;u&gt;Step 1: Focus on the denominator.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .frac {text-align: center; display: inline-block; margin: 10px}
  &lt;/style&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="[:3^k5GZP)w_PhEBQ-hQ">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="n7dQGHlZL@2{glx-UMg![">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="[ya]J(Mf/_}JjZa76D0;">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="!263SGrrKHq-ENhCgr,:">
                                                                                            <field name="value">i &lt; num</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="partial_explanation" id="$h^sRdILP`r}!3,qT-$0" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="6bp+MPg5Ji@2DGTLitywj">
                                                                                                <field name="value">${print_frac(frac[i].nu, frac[i].de)}${'  '}</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="t{#HC,Q9;9P(.3OQ-9+g">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="+}K@1SE_zuS_l|gUfxeGr">
                                                                                                    <field name="value">i+1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="WDL6=O{^QY}WWQ3D={D1" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="Le0K|O33LRKx|HVtG}]n">
                                                                                                <field name="value">&lt;/br&gt;&lt;/br&gt;
All the denominators are&lt;/br&gt;&lt;/br&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="C+2`T;)n.iEzG,}q82(#">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="}0_k44~3#RMYY7JH)`6?">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="0aBP8CF9Nud-F6)8Mvno">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="lC+hr}V0l:k!Vw#C8YZX">
                                                                                                        <field name="value">i &lt; num</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="partial_explanation" id="m]|nQY5f2V[mROyeZvg7" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="sIHoj|QyQ-AIe(6U-/.6">
                                                                                                            <field name="value">${check_after(i, listDen) == 0 ?  listDen[i] : ''}${' '}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id=":bvO^9!SQ$vj/@1ng~.]V">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id=",h?;H2WjJ70tYFZzj.D_">
                                                                                                                <field name="value">i+1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="end_partial_explanation" id="xRW((7:i:O{:S9_};2yu">
                                                                                                        <next>
                                                                                                          <block type="partial_explanation" id="Lw/|whhatKDa#mMC+9P~" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="string_value" id="j-W|$Rkb/s%pV!Btiak9">
                                                                                                                <field name="value">&lt;u&gt;Step 2: Look at the biggest denominator.&lt;/u&gt;&lt;/br&gt;

&lt;style&gt;
  .frac {text-align: center; display: inline-block; margin: 10px}
  &lt;/style&gt;

${max_de} is the biggest denominator.&lt;/br&gt;&lt;/br&gt;
Find the multiples.&lt;/br&gt;
${listCan.length != 0 ? max_de + " can divide by " + print_arr(listCan) : ""}${listCan.length != 0 ? ", but ": ""}${listCannot.length != 0 ? max_de + " cannot divide by " + print_arr(listCannot) : ""}.&lt;/br&gt;&lt;/br&gt;
                                                                                                                </field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="statement" id="u@2D_U[M{jn9H0[/YWyGV">
                                                                                                                <field name="value">do {</field>
                                                                                                                <next>
                                                                                                                  <block type="partial_explanation" id="Hx{AiCtgiMpYd5if7L:0" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="!#Rd@2NIsmfH,OcH$!R`z">
                                                                                                                        <field name="value">${max_de} x ${max_de@2get_min(listCannot)/HCF(max_de,get_min(listCannot))/max_de} = ${max_de@2get_min(listCannot)/HCF(max_de,get_min(listCannot))}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="KYyZ9VK7O6r3z^RoTo{X">
                                                                                                                        <field name="name">max_de</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="kK?;V~.G_y-g,Oy3O-y~">
                                                                                                                            <field name="value">max_de@2get_min(listCannot)/HCF(max_de,get_min(listCannot))</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="statement" id="|~P+SC$+L1PQ{/soXg50">
                                                                                                                            <field name="value">var arr = [];

for(var i = 0; i &lt; listCannot.length; i++){
  if(check_divide(max_de, listCannot[i]) == 1){
    listCan.push(listCannot[i]);
    arr.push(i);
  }
}

var cannot_temp = [];
for(var i = 0; i&lt; listCannot.length; i++){
  cannot_temp[i] = listCannot[i];
}

for(var i = 0; i&lt; arr.length; i++){
  if(check_exist(cannot_temp[arr[i]], listCan) == 1){
    listCannot = delete_array(cannot_temp[arr[i]], listCannot)
  }
}
                                                                                                                            </field>
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id="y[wwk+f(xyuc`|(a8w^z" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="p;y}^:VsN.0./?Qzn.z@1">
                                                                                                                                    <field name="value">&lt;/br&gt;
${listCan.length != 0 ? max_de + " can divide by " + print_arr(listCan) : ""}${listCannot.length != 0 ? ", but then again, " : "."}${listCannot.length != 0 ? max_de + " cannot divide by " + print_arr(listCannot) + ".": ""}&lt;/br&gt;&lt;/br&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="statement" id="`byz}V9)Qii1z+7n(7Y^">
                                                                                                                                    <field name="value">}
while (listCannot.length != 0)
                                                                                                                                    </field>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="MeQj2oNh,wa,@2o!g4V;X" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="R4@1v+:,5Q.3%c@2DYVE`i">
                                                                                                                                            <field name="value">&lt;b&gt;${max_de}&lt;/b&gt; can divide by all denominators.</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="end_partial_explanation" id="@2t(c#Fg/D@2D@19m+x0xuB">
                                                                                                                                            <next>
                                                                                                                                              <block type="partial_explanation" id="O0]tH;QfG?S^RVD[,{~=" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="(G{[lK%CM^Yi-M~Rw%!c">
                                                                                                                                                    <field name="value">&lt;u&gt;Step 3: Make each denominator equal ${max_de} without changing each fraction value.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .frac {text-align: center; display: inline-block; margin: 10px;}
  &lt;/style&gt;
                                                                                                                                                    </field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="Bh#).5#5wi.?e2hGrc;s">
                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="#39@2N{2-MXpT7ZYMv:G)">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="while_do_block" id="|RIhu.!33_W=Uv?yVBnn">
                                                                                                                                                        <value name="while">
                                                                                                                                                          <block type="expression" id="SEe!_AGS}M3H6czeBpjA">
                                                                                                                                                            <field name="value">i &lt; num</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="do">
                                                                                                                                                          <block type="partial_explanation" id="`[@2Td%agc}aNbFu]}!" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="o6#yn)Jnma@1qkrEocU!Y">
                                                                                                                                                                <field name="value">${print_frac(frac[i].nu, frac[i].de)} &lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='height: 100px'&gt;x&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt; ${print_frac(max_de/listDen[i], max_de/listDen[i])} &lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='height: 100px'&gt;=&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt; ${print_frac(frac[i].nu@2max_de/listDen[i], max_de)}&lt;/br&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="]O3.wuu.1Yncp-omAB.(">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="?57Rkb@2KS^Ng?Y@2qlQ;#">
                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="end_partial_explanation" id="FIzt8S;CFgR5@1GhFN:gk">
                                                                                                                                                            <next>
                                                                                                                                                              <block type="statement" id=",=EMlVW}0Ju1;DMhpdy/">
                                                                                                                                                                <field name="value">var max_frac = [{nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}, {nu: 0, de: 0}];
for(var i =0; i&lt; num; i++){
    max_frac[i].nu = frac[i].nu@2max_de/listDen[i];
    max_frac[i].de = max_de;
}

if(type == 1)  max_frac = asc_fraction(max_frac);
else max_frac = dec_fraction(max_frac);
                                                                                                                                                                </field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="partial_explanation" id="@1T/mdJ~OsfQvd@1jts=MP" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="cfp_7rUl(v@2w!Q~k0DIr">
                                                                                                                                                                        <field name="value">&lt;u&gt;Step 4: Let's compare all fractions together.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
  .frac {text-align: center; display: inline-block; margin: 10px;}
  &lt;/style&gt;
                                                                                                                                                                        </field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id=")paLz.}!JSHWkdgr^acm">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="cS|3zIqpXQWEM$e~?/$1">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="while_do_block" id="Or[YEL,~$`vSJR);k_ZA">
                                                                                                                                                                            <value name="while">
                                                                                                                                                                              <block type="expression" id="tPk%2)2Ohqu7vABnHrBX">
                                                                                                                                                                                <field name="value">i &lt; num</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <statement name="do">
                                                                                                                                                                              <block type="partial_explanation" id="(QEw[SH4Ng$t/Z6]aK?t" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="string_value" id="#yV^HF3Bl4!N$yR5m@2_2">
                                                                                                                                                                                    <field name="value">${print_frac(max_frac[i].nu, max_frac[i].de)}</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="if_then_block" id="7wxqZCdgJJFpQ1%@2+[-@1">
                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                      <block type="expression" id="mSmxc:CX_Ucv$J^D0v8x">
                                                                                                                                                                                        <field name="value">i &lt; num - 1</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                      <block type="partial_explanation" id="$mcALIHHMYK0nd2dcQg=" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="Z(Za3//S3.?3~,[}D+?!">
                                                                                                                                                                                            <field name="value">&lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='height: 100px'&gt;${type == 1 ? "&lt;" : "&gt;"}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="VSY?jWXL$MGd}lbQ7Z~1">
                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="lCe@1eK87CgFe.JJeXk14">
                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="partial_explanation" id="g+7BH,6uLYP1S.^{G=:h" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="string_value" id="b`Cmc}kvh|8][qg)4Jy0">
                                                                                                                                                                                    <field name="value">&lt;/br&gt;What does that mean?&lt;/br&gt;</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id=".xzzpOhjV0p%|sOE-FW@1">
                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="I8`aK1~W_[IP+n]hKg@2r">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="while_do_block" id="BXW4Sg}W}[{5S!L_6?oM">
                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                          <block type="expression" id="I?1SyTrx!.hv-7Q-vOC{">
                                                                                                                                                                                            <field name="value">i &lt; num</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                          <block type="partial_explanation" id="x^=.kaV;jU@1r[BKGeVGy" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="LHU9FzKXtMSNCUnzw740">
                                                                                                                                                                                                <field name="value">${print_frac(ans[i].nu, ans[i].de)}</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="if_then_block" id="S16Hf@1@2o:t(N7}#P_Ca}">
                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                  <block type="expression" id="tl%uN{10(xT%xE(v^o6L">
                                                                                                                                                                                                    <field name="value">i &lt; num - 1</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                  <block type="partial_explanation" id=":o6D}[m;-9O(wGSJgH[F" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="={jt7LyGX}(?8QpGq-vH">
                                                                                                                                                                                                        <field name="value">&lt;table class='frac'&gt;&lt;tr&gt;&lt;td style='height: 100px'&gt;${type == 1 ? "&lt;" : "&gt;"}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="variable" id="5Pc7$;KzI18z1LTcx?D,">
                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="expression" id="fO`JTGk5[fbnn-ZaG.U/">
                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="expression" id="~j@1,^liddf}oDg}E91^i" x="577" y="2533">
    <field name="value">frac</field>
  </block>
  <block type="expression" id="0@1;T4bld9Q_)Z+ff+ng6" x="613" y="4725">
    <field name="value">ans</field>
  </block>
  <block type="variable" id="C4rMRUV-4Dtq7gD:Z|G:" x="0" y="7210">
    <field name="name">listCannot</field>
    <value name="value">
      <block type="expression" id="TL=[lCHEF74@26fXP?JhU">
        <field name="value">delete_array(get_min(listCannot), listCannot)</field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */