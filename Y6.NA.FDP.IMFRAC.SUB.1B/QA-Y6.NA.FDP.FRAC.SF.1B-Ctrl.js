
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.FDP.FRAC.SF.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.NA.FDP.FRAC.SF.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": ""
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "5"
              },
              "max": {
                "#type": "expression",
                "value": "15"
              }
            }
          },
          {
            "#type": "variable",
            "name": "b",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "a+1"
              },
              "max": {
                "#type": "expression",
                "value": "25"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "10"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "50"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "b",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "a+1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "60"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "25"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "60"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "b",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "a+1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "75"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "type ",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "statement",
        "value": "a = a * 2;\nb = b * 2;\n\nfunction HCF(a, b){\n    var max, min, temp;\n    if(a > b) {max =a; min =b;}\n    else {max =b; min =a;}\n    while(max != min) {\n        temp = max - min;\n        if(temp >= min){\n            max =temp;\n        }\n        else {\n            max = min;\n            min = temp;\n        }\n    }\n    return max;\n}\n\nvar n1 = a / HCF(a, b);\nvar n2 = b / HCF(a, b);\n\nfunction check(x, A, num) {\n    for(var m=0; m<num; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "Num",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "Num[0]",
            "value": {
              "#type": "expression",
              "value": "{a, b}"
            }
          },
          {
            "#type": "variable",
            "name": "Num[0].a",
            "value": {
              "#type": "expression",
              "value": "n1"
            }
          },
          {
            "#type": "variable",
            "name": "Num[0].b",
            "value": {
              "#type": "expression",
              "value": "n2"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 4"
            },
            "do": [
              {
                "#type": "variable",
                "name": "Num[i]",
                "value": {
                  "#type": "expression",
                  "value": "{a, b}"
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "rda",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "-10"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "10"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "Num[i].a",
                    "value": {
                      "#type": "expression",
                      "value": "n1 + rda"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "rdb",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "-10"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "10"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "Num[i].b",
                    "value": {
                      "#type": "expression",
                      "value": "n2 + rdb"
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "Num[i].a <= 0 || Num[i].b<=0 || Num[i].a >= Num[i].b"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "20"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Which fraction is the most simple fraction?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "500"
                },
                "height": {
                  "#type": "expression",
                  "value": "140"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 0",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "3"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "100"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "140"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "[0, 1, 2]"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 0"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "a"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_position",
                                              "#prop": "",
                                              "x": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "y": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 1"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "line.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}line.png"
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 2"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "b"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_position",
                                              "#prop": "",
                                              "x": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "y": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "4"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "340"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "160"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2, 3]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": true
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "variable",
                    "name": "data",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    }
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "3"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "100"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "140"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "[0, 1, 2]"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 0"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "Num[data].a"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 1"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "line.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}line.png"
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 2"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "Num[data].b"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "Num[data].a/Num[data].b"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "box.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}box.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "n1/n2"
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 1"
            },
            "then": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "20"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "Simplify the below fraction."
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "36"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "20"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "Simplify the below fraction in its simplest form."
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "36"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "220"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "500"
                },
                "height": {
                  "#type": "expression",
                  "value": "140"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[1, 2, 3]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 0",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "3"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "100"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "140"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "[0, 1, 2]"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 0"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "a"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_position",
                                              "#prop": "",
                                              "x": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "y": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 1"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "line.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}line.png"
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 2"
                            },
                            "then": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "b"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_position",
                                              "#prop": "",
                                              "x": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "y": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "0"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "${$item.data}.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}${$item.data}.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 1",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "equal.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}equal.png"
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 2",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "3"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "100"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "140"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "[0, 1, 2]"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 0"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "shape.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}shape.png"
                                  }
                                ]
                              },
                              {
                                "#type": "choice_input_shape",
                                "#props": [
                                  {
                                    "#type": "prop_value",
                                    "#prop": "",
                                    "value": {
                                      "#type": "expression",
                                      "value": "n1"
                                    }
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_size",
                                    "#prop": "",
                                    "width": {
                                      "#type": "expression",
                                      "value": "106"
                                    },
                                    "height": {
                                      "#type": "expression",
                                      "value": "61"
                                    }
                                  },
                                  {
                                    "#type": "prop_input_keyboard",
                                    "#prop": "",
                                    "keyboard": "numbers1"
                                  },
                                  {
                                    "#type": "prop_input_max_length",
                                    "#prop": "",
                                    "maxLength": {
                                      "#type": "expression",
                                      "value": "n1.toString().length"
                                    }
                                  },
                                  {
                                    "#type": "prop_input_result_position",
                                    "#prop": "",
                                    "resultPosition": "right"
                                  },
                                  {
                                    "#type": "prop_tab_order",
                                    "#prop": "",
                                    "tabOrder": {
                                      "#type": "expression",
                                      "value": "0"
                                    }
                                  },
                                  {
                                    "#type": "prop_stroke",
                                    "#prop": "stroke"
                                  },
                                  {
                                    "#type": "prop_fill",
                                    "#prop": "fill"
                                  },
                                  {
                                    "#prop": "",
                                    "style": {
                                      "#type": "json",
                                      "base": "text",
                                      "#props": [
                                        {
                                          "#type": "prop_text_style_font_size",
                                          "#prop": "",
                                          "fontSize": {
                                            "#type": "expression",
                                            "value": "36"
                                          }
                                        },
                                        {
                                          "#type": "prop_text_style_fill",
                                          "#prop": "",
                                          "fill": "black"
                                        },
                                        {
                                          "#type": "prop_text_style_stroke",
                                          "#prop": "",
                                          "stroke": "white"
                                        },
                                        {
                                          "#type": "prop_text_style_stroke_thickness",
                                          "#prop": "",
                                          "strokeThickness": {
                                            "#type": "expression",
                                            "value": "2"
                                          }
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "#init": "algorithmic_input"
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 1"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "line.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}line.png"
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "#type": "if_then_block",
                            "if": {
                              "#type": "expression",
                              "value": "$cell.row == 2"
                            },
                            "then": [
                              {
                                "#type": "if_then_else_block",
                                "if": {
                                  "#type": "expression",
                                  "value": "type == 1"
                                },
                                "then": [
                                  {
                                    "#type": "list_shape",
                                    "#props": [
                                      {
                                        "#type": "prop_list_direction",
                                        "#prop": "",
                                        "dir": "horizontal"
                                      },
                                      {
                                        "#type": "prop_position",
                                        "#prop": "",
                                        "x": {
                                          "#type": "expression",
                                          "value": "$cell.centerX"
                                        },
                                        "y": {
                                          "#type": "expression",
                                          "value": "$cell.centerY"
                                        }
                                      },
                                      {
                                        "#type": "prop_anchor",
                                        "#prop": "anchor",
                                        "x": {
                                          "#type": "expression",
                                          "value": "0.5"
                                        },
                                        "y": {
                                          "#type": "expression",
                                          "value": "0.5"
                                        }
                                      }
                                    ],
                                    "items": [
                                      {
                                        "#type": "json",
                                        "#props": [
                                          {
                                            "#type": "prop_list_align",
                                            "#prop": "",
                                            "align": "middle"
                                          },
                                          {
                                            "#type": "prop_list_item_source",
                                            "#prop": "",
                                            "source": {
                                              "#type": "func",
                                              "name": "charactersOf",
                                              "args": [
                                                {
                                                  "#type": "expression",
                                                  "value": "b"
                                                }
                                              ]
                                            }
                                          }
                                        ],
                                        "template": {
                                          "#callback": "$item",
                                          "variable": "$item",
                                          "body": [
                                            {
                                              "#type": "image_shape",
                                              "#props": [
                                                {
                                                  "#type": "prop_position",
                                                  "#prop": "",
                                                  "x": {
                                                    "#type": "expression",
                                                    "value": "0"
                                                  },
                                                  "y": {
                                                    "#type": "expression",
                                                    "value": "0"
                                                  }
                                                },
                                                {
                                                  "#type": "prop_size",
                                                  "#prop": "",
                                                  "width": {
                                                    "#type": "expression",
                                                    "value": "0"
                                                  },
                                                  "height": {
                                                    "#type": "expression",
                                                    "value": "0"
                                                  }
                                                },
                                                {
                                                  "#type": "prop_image_key",
                                                  "#prop": "",
                                                  "key": "${$item.data}.png"
                                                },
                                                {
                                                  "#type": "prop_image_src",
                                                  "#prop": "",
                                                  "src": "${image_path}${$item.data}.png"
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                ],
                                "else": [
                                  {
                                    "#type": "image_shape",
                                    "#props": [
                                      {
                                        "#type": "prop_position",
                                        "#prop": "",
                                        "x": {
                                          "#type": "expression",
                                          "value": "$cell.centerX"
                                        },
                                        "y": {
                                          "#type": "expression",
                                          "value": "$cell.centerY"
                                        }
                                      },
                                      {
                                        "#type": "prop_image_key",
                                        "#prop": "",
                                        "key": "shape.png"
                                      },
                                      {
                                        "#type": "prop_image_src",
                                        "#prop": "",
                                        "src": "${image_path}shape.png"
                                      }
                                    ]
                                  },
                                  {
                                    "#type": "choice_input_shape",
                                    "#props": [
                                      {
                                        "#type": "prop_value",
                                        "#prop": "",
                                        "value": {
                                          "#type": "expression",
                                          "value": "n2"
                                        }
                                      },
                                      {
                                        "#type": "prop_position",
                                        "#prop": "",
                                        "x": {
                                          "#type": "expression",
                                          "value": "$cell.centerX"
                                        },
                                        "y": {
                                          "#type": "expression",
                                          "value": "$cell.centerY"
                                        }
                                      },
                                      {
                                        "#type": "prop_size",
                                        "#prop": "",
                                        "width": {
                                          "#type": "expression",
                                          "value": "106"
                                        },
                                        "height": {
                                          "#type": "expression",
                                          "value": "61"
                                        }
                                      },
                                      {
                                        "#type": "prop_input_keyboard",
                                        "#prop": "",
                                        "keyboard": "numbers1"
                                      },
                                      {
                                        "#type": "prop_input_max_length",
                                        "#prop": "",
                                        "maxLength": {
                                          "#type": "expression",
                                          "value": "n2.toString().length"
                                        }
                                      },
                                      {
                                        "#type": "prop_input_result_position",
                                        "#prop": "",
                                        "resultPosition": "right"
                                      },
                                      {
                                        "#type": "prop_tab_order",
                                        "#prop": "",
                                        "tabOrder": {
                                          "#type": "expression",
                                          "value": "0"
                                        }
                                      },
                                      {
                                        "#type": "prop_stroke",
                                        "#prop": "stroke"
                                      },
                                      {
                                        "#type": "prop_fill",
                                        "#prop": "fill"
                                      },
                                      {
                                        "#prop": "",
                                        "style": {
                                          "#type": "json",
                                          "base": "text",
                                          "#props": [
                                            {
                                              "#type": "prop_text_style_font_size",
                                              "#prop": "",
                                              "fontSize": {
                                                "#type": "expression",
                                                "value": "36"
                                              }
                                            },
                                            {
                                              "#type": "prop_text_style_fill",
                                              "#prop": "",
                                              "fill": "black"
                                            },
                                            {
                                              "#type": "prop_text_style_stroke",
                                              "#prop": "",
                                              "stroke": "white"
                                            },
                                            {
                                              "#type": "prop_text_style_stroke_thickness",
                                              "#prop": "",
                                              "strokeThickness": {
                                                "#type": "expression",
                                                "value": "2"
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "#init": "algorithmic_input"
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "load",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "left|right"
        }
      },
      {
        "#type": "statement",
        "value": "var tree = [];\n  var prime = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 47, 53, 59, 67, 71, 83, 89, 101, 107, 109, 113, 127, 131, 137, 139, 149];\n\n  for(var i = 0; ; i++){\n  }"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: First, build a factor tree</u></br></br>\n\n<div style='position: relative'>\n  <img src='@sprite.src(left)' style='position: absolute;'/>\n</div>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.NA.FDP.FRAC.SF.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.NA.FDP.FRAC.SF.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="variable" id="weElLx3.8SB.)STr377[">
                        <field name="name">loadAssets</field>
                        <value name="value">
                          <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                            <field name="link">${image_path}</field>
                            <field name="images"></field>
                          </block>
                        </value>
                        <next>
                          <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                            <statement name="#props">
                              <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                                <field name="#prop"></field>
                                <value name="key">
                                  <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                    <field name="value">bg${drop_background}.png</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                    <field name="#prop"></field>
                                    <value name="src">
                                      <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                        <field name="value">${background_path}bg${drop_background}.png</field>
                                      </block>
                                    </value>
                                  </block>
                                </next>
                              </block>
                            </statement>
                            <next>
                              <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                                <field name="name">numberOfCorrect</field>
                                <value name="value">
                                  <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                    <field name="name">numberOfIncorrect</field>
                                    <value name="value">
                                      <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                        <field name="value">0</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                        <field name="name">range</field>
                                        <value name="value">
                                          <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                            <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="@1ZFBBTnIU41VLYd%sr)p">
                                            <value name="if">
                                              <block type="expression" id="8Xli+l{mXhGQ@1qSU?yMV">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="gQ5MQ(hBT!L8+obBRN~A">
                                                <field name="name">a</field>
                                                <value name="value">
                                                  <block type="random_number" id="^o8Q^HT`IjcF,?v:w{f:">
                                                    <value name="min">
                                                      <block type="expression" id="~PULAiEB%#Oy2/`+mX+B">
                                                        <field name="value">5</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="_]2q0Qr6ax2C@1Ctn^c(r">
                                                        <field name="value">15</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="-@2/50rN0+|^y$^Ahyhpo">
                                                    <field name="name">b</field>
                                                    <value name="value">
                                                      <block type="random_number" id="o)Ld^nxb$;0?F3-+u,Ak">
                                                        <value name="min">
                                                          <block type="expression" id="f@1XyX8/^r|@1H935/-R`#">
                                                            <field name="value">a+1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="DDEm;^}$p9Jl18@1i?u@2a">
                                                            <field name="value">25</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id=":lInX@26GKj_E_!9XgDIp">
                                                <value name="if">
                                                  <block type="expression" id="@2-85~oE[$KKRnIk?;V:h">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="4:|d8S/GA|ys]_?Aftr3">
                                                    <field name="name">a</field>
                                                    <value name="value">
                                                      <block type="random_number" id="u.Gq/`5cRI+^|r4C;mHh">
                                                        <value name="min">
                                                          <block type="expression" id="Gr^h]E:togez-Q{:ww{l">
                                                            <field name="value">10</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="!4N.X1vV;gc|t`c.A{LL">
                                                            <field name="value">50</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="vZR/+f9:@1BZZbZB:aPUJ">
                                                        <field name="name">b</field>
                                                        <value name="value">
                                                          <block type="random_number" id="ky2uGKaC3X,=ZxtSkhk|">
                                                            <value name="min">
                                                              <block type="expression" id="/;_#tSK4nq1M8:d,~4=~">
                                                                <field name="value">a+1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="aU[QLnkrB,9~z{}m$rHu">
                                                                <field name="value">60</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="qiNDZKkE|LjF~!vwLtwF">
                                                    <field name="name">a</field>
                                                    <value name="value">
                                                      <block type="random_number" id="~A@19HD1[eEYv}Z@1[g_sd">
                                                        <value name="min">
                                                          <block type="expression" id="]d)AZ04vLz`Jn%j^|J3G">
                                                            <field name="value">25</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="LQ[lE1[PrWa7-+QXZuTc">
                                                            <field name="value">60</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="[e@1^`ZthBL@1Ojt3CKMc|">
                                                        <field name="name">b</field>
                                                        <value name="value">
                                                          <block type="random_number" id="#e!DS)aFvR0VrziEl3.l">
                                                            <value name="min">
                                                              <block type="expression" id="P~]$veQnd+W9Sw30NHf[">
                                                                <field name="value">a+1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="5^ub-JF+5RC:ibRzM`Ei">
                                                                <field name="value">75</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="Fp]nX,4-efFt50tTwQ`{">
                                                <field name="name">type </field>
                                                <value name="value">
                                                  <block type="random_number" id="dnQFs[gmSI?OXPfOuU`d">
                                                    <value name="min">
                                                      <block type="expression" id="#g4:a+ZUwjx:HQA9`+9t">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="!FYf#GG!!OM%8wvT|atC">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="6gY%6iyDm1c#^}s;V}xr">
                                                    <field name="value">a = a @2 2;
b = b @2 2;

function HCF(a, b){
    var max, min, temp;
    if(a &gt; b) {max =a; min =b;}
    else {max =b; min =a;}
    while(max != min) {
        temp = max - min;
        if(temp &gt;= min){
            max =temp;
        }
        else {
            max = min;
            min = temp;
        }
    }
    return max;
}

var n1 = a / HCF(a, b);
var n2 = b / HCF(a, b);

function check(x, A, num) {
    for(var m=0; m&lt;num; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}
                                                    </field>
                                                    <next>
                                                      <block type="if_then_else_block" id="`/X=/Q7jxam.BL~!XZLH">
                                                        <value name="if">
                                                          <block type="expression" id="U5EF}c8k:jz!CQo:H^/s">
                                                            <field name="value">type == 2</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="variable" id="K=OS3n72n18?`%#!M]+1">
                                                            <field name="name">Num</field>
                                                            <value name="value">
                                                              <block type="expression" id="IPFJ6-mo%2Yzr!FST;oc">
                                                                <field name="value">[]</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="my=wt5sP6K$V=^UA.re,">
                                                                <field name="name">Num[0]</field>
                                                                <value name="value">
                                                                  <block type="expression" id="Mnw(QNV)s!j.F=/FB3.O">
                                                                    <field name="value">{a, b}</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="0iizsJ0+)XakHnLbb[N=">
                                                                    <field name="name">Num[0].a</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="C@1-2)`FxtX:nVyoHtOxK">
                                                                        <field name="value">n1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="]$)15VNUgLVVmlsvJgiy">
                                                                        <field name="name">Num[0].b</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="bSU2vb9grMBJV7UF6SU?">
                                                                            <field name="value">n2</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="FS9z//@1e/LFjzO=g@1EML">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="W_lVkpF/sl!t/5DH4K$y">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="while_do_block" id="S,q}J03r@1z2$$jaOnsg9">
                                                                                <value name="while">
                                                                                  <block type="expression" id="Cvt]o#A|Xy@2VW98$Tfi$">
                                                                                    <field name="value">i &lt; 4</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="do">
                                                                                  <block type="variable" id=":XcL)c~uk4P~{?HB/s5e">
                                                                                    <field name="name">Num[i]</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="ATANz8pB70~!?d=CjLGL">
                                                                                        <field name="value">{a, b}</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="do_while_block" id="a=1/[w`94Ek)s.5x4]b(">
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="Ugl(kCjIDXcl`@24Hr?][">
                                                                                            <field name="name">rda</field>
                                                                                            <value name="value">
                                                                                              <block type="random_number" id="Ud96z/mn~k3+_#j@2!hi^">
                                                                                                <value name="min">
                                                                                                  <block type="expression" id="HhLg?.7kj+OR8nbcpT5F">
                                                                                                    <field name="value">-10</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="max">
                                                                                                  <block type="expression" id="K9/n1S88~q^WzpVcN~]z">
                                                                                                    <field name="value">10</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="`Y+#M4@1Lo(9:XGj~v/t/">
                                                                                                <field name="name">Num[i].a</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="O9X8z[2uVKi^|wg6D%l(">
                                                                                                    <field name="value">n1 + rda</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id=";,]esLxwPYG19fD/VTX?">
                                                                                                    <field name="name">rdb</field>
                                                                                                    <value name="value">
                                                                                                      <block type="random_number" id="T_M+(D`NsA7MM1Zq-iBd">
                                                                                                        <value name="min">
                                                                                                          <block type="expression" id="m6FTK=!Z?Fl]UML4P^4g">
                                                                                                            <field name="value">-10</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="max">
                                                                                                          <block type="expression" id="(%Y6,sjIX6h9.NDNMx#c">
                                                                                                            <field name="value">10</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="YGy|N@15|m~T:{w}!VtNG">
                                                                                                        <field name="name">Num[i].b</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="3K.ZJ)i)9B,9S2xP~DqP">
                                                                                                            <field name="value">n2 + rdb</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <value name="while">
                                                                                          <block type="expression" id="K!O(sy]3!X!NQcS@16pb0">
                                                                                            <field name="value">Num[i].a &lt;= 0 || Num[i].b&lt;=0 || Num[i].a &gt;= Num[i].b</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="|Ef_dfhE=qnTZ1^V=(n!">
                                                                                            <field name="name">i</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="k]|p}/P|Dl1+,W7zXgX|">
                                                                                                <field name="value">i+1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="text_shape" id="Xz^TWbu;+-wq^AbA_N1Y">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="l~=thl^`lb|,%;Wt%Oa^">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="tH{dLEh_U7owjA[tVPVm">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="#Ez!^4t[^svw_.Lh2zXo">
                                                                                            <field name="value">20</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="`!+GdW?e;P:1J$rmQ-sI">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="Y4#c?oi6=^[o5b^vc.,B">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="gr8e4L.73K,;TQ{y!JX}">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="[YYVYyKN3H64M(ncGriJ">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="yUAwDxroIjWTm`b`iRxC">
                                                                                                    <field name="value">Which fraction is the most simple fraction?</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="@1:U0?Fl{fM@1c+yjaOGC`">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="_Ch{chkkbL=-y~yRZ=^]">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id=",E4OUTcJx=$U@2:hKd7=Q">
                                                                                                            <field name="value">36</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="K{jlGnKM?f|+PnG!^8q.">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="WZ,Qv]RNuAv,jM:;VN@1L">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="`wU)jr(B5o)%@2[;BHJV9">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="qpd}5b+7De@2/E_5@14+os">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="s46+gd6UpEgoQ|Y37YE}">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="_YS.F@2$.E]$R{vbc:MM}">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="grid_shape" id="}88_)AZAby98X#b##pH!">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_grid_dimension" id="LsB_sk`;UoYA~2F$Lflv">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="rows">
                                                                                              <block type="expression" id="NsK9-kq;0e..UC8H#u!H">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="cols">
                                                                                              <block type="expression" id="wu2XoYvS9]1;jHH(sZ0N">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_position" id="dk06OnzVk{/IvEWZU~b+">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="kJH8wPr44%owy,sQf6^T">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="4!!LCtl?,BvWkGumzf[$">
                                                                                                    <field name="value">150</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="KZhvN+d!)wlbe4y_Q9=7">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="h-gFq+@1M0Fc5lBoA1y7@1">
                                                                                                        <field name="value">500</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="]v+]RyO?!W}5j`Rt,.w6">
                                                                                                        <field name="value">140</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_anchor" id="g82IzVje~`P(zgN1:8~K">
                                                                                                        <field name="#prop">anchor</field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="-WQp``,B+BsP|},TlX:i">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="C$A`Kc,y90Ox1aC58,ER">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_source" id="~3rcHa_5L@2;Rg~aTm%-1">
                                                                                                            <field name="#prop">cell.source</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="Lv@26DIU}jHaZR1v8@2@2=q">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_random" id="rKbtQ=EzPo1x%Y(pYk{g">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="random">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_show_borders" id="{;A[!Xr.8Ny8TWKt;!">
                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                    <field name="value">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_template_for" id="}(5B2-Rgm]LP7P/HmQTr">
                                                                                                                        <field name="variable">$cell</field>
                                                                                                                        <field name="condition">$cell.col == 0</field>
                                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                        <statement name="body">
                                                                                                                          <block type="grid_shape" id="DqacsliRg}K/Xjbs/3?B">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_grid_dimension" id="FWYT:JWKi6Xt3fPFCDa@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="rows">
                                                                                                                                  <block type="expression" id="@1Uzl;shxNuuEY~Yvk$s?">
                                                                                                                                    <field name="value">3</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="cols">
                                                                                                                                  <block type="expression" id="5e3!Y2w,_E|tG{wXUCly">
                                                                                                                                    <field name="value">1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_position" id="#+UgN-8utQIOf0nWGNUW">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="{jKY/:{+-hTk)3r/BxXV">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="NUmDT67Hu13QX30#Bu1{">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_size" id="lk~0_w)a%vJyi3kVzgp2">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="width">
                                                                                                                                          <block type="expression" id="/TJ(@1wEkyL/2B=)5B.J3">
                                                                                                                                            <field name="value">100</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="height">
                                                                                                                                          <block type="expression" id=":`(M8imP]qg+P!,1uY6v">
                                                                                                                                            <field name="value">140</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_anchor" id="^R-kcxse^~9y^g+azWAJ">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="j:w_Lf+.Hg}r8|A_(_Er">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="1%-=4!=fa3_|uIc~5va#">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_source" id="+9|TR{]=2}Sz;j_%;sA@2">
                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="NI?.h:bi4E$pIFSQjuu5">
                                                                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_random" id="Pmet`fcEkbJ2yMKaTBl?">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_show_borders" id="1pB{@2HJ]ueP-]}q6,$!;">
                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_cell_template" id="kVfVy1z!-%C}w}Vspw@27">
                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                            <statement name="body">
                                                                                                                                                              <block type="if_then_block" id="z$RT](H(I`5]/!t#1G8i">
                                                                                                                                                                <value name="if">
                                                                                                                                                                  <block type="expression" id="eo?:sC#bNF_5aL@1cSBWx">
                                                                                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="then">
                                                                                                                                                                  <block type="list_shape" id="24ffLpdRaZ3n%I,aP7-R">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_list_direction" id="AL)g+hU5(RMTCw6|2!;k">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_position" id="j~Ah|WYVx@2?YE]#;o-:@1">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id=")!?=@2+-XrY4rTO=Kgc|e">
                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="XwTLUQ[VHF~Nnhvn)D1g">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_anchor" id="gIPD|HCK_6^0-:Wl7j?H">
                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id=":?gshI{;t5nyhOJ/1tyD">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="uQLv!i@2--FS?8(MgVOu3">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="items">
                                                                                                                                                                      <block type="list_item_shape" id="^/$D{MHle0^~Mll/@1tbw">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_list_align" id="69fcyaFaTn2~TR3M`KVi">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_list_item_source" id=",E8}Hs,mRriIUAF=weA=">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="source">
                                                                                                                                                                                  <block type="func_characters_of" id=";?+Bt9?UJ?zX|R;9naU}" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="brF=N?K#g-5aL-r8Wq+p">
                                                                                                                                                                                        <field name="value">a</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <statement name="template">
                                                                                                                                                                          <block type="image_shape" id="p6+)TvX^BwX~YBlU9h]$">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_position" id="Jw:t%HN(30Bf}8_Ad{2P">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="kQRS[mjlAhxQ:fASf]eY">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="jb@2!?G]}8@1B)@27?IcXzY">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_size" id="gtGNkPhx0|j!O+n?QsD?">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="width">
                                                                                                                                                                                      <block type="expression" id="NiM`tp/UHw}wh3e=j@13L">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="height">
                                                                                                                                                                                      <block type="expression" id="Utu@1,UbfK;]5PM8AYzPv">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_key" id="Kkf~cv01RtC3f$PVXxfY">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="OF^uB8LO)lrZ1b3Xd{C;">
                                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id=".#76FG9pil:utLsEP6RY">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id="cNxXnCdzaRaiIY$ZLAXz">
                                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="if_then_block" id="a|t[q^ZR=~nq^^+wAQ[9">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id="!pLg[sre@1gbw@1sk_-uV(">
                                                                                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="image_shape" id="{O+1PH1nF6)dc3(p,CY7">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="DK/%U4@1j)Y.wE%s-Zi8k">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="5S-G`Vvkw@1.;OxneGY;k">
                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="7UzkVC@2lf(CjdK,z5NAD">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_key" id="+(Tf@2X94G7a35%Du!L(y">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="key">
                                                                                                                                                                                  <block type="string_value" id="qRA%oYuY4CTsgkY.NogO">
                                                                                                                                                                                    <field name="value">line.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_src" id=")TPD,kYedi:9fgz9P!@28">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                      <block type="string_value" id=")9e]bS3^b8-g69@12,8I$">
                                                                                                                                                                                        <field name="value">${image_path}line.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="if_then_block" id="2s]RSAXHAe7t|XP!Z}I/">
                                                                                                                                                                        <value name="if">
                                                                                                                                                                          <block type="expression" id="%#,0H.z#Q}nXE@1K+MA7o">
                                                                                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="then">
                                                                                                                                                                          <block type="list_shape" id="c9w0b77tI#Y`@1)cicT^P">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_list_direction" id="-Ud,~65zmGl8X3M#v-zC">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_position" id="mSa[J){#Er8Qs0gb�b">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="0)y-v!D+9/fY@2cTWkl?w">
                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="!^uwx`U8jh;7U6lh|=/l">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_anchor" id="bH~z$^YdVx;[5Cq#ZM0)">
                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="Z#a4r^4Ea^n/cnAS+pdC">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="Dgd~1DVyHGu)558AZHU-">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <statement name="items">
                                                                                                                                                                              <block type="list_item_shape" id=";d0ge3zC7(y(p_E@1{Ym-">
                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                  <block type="prop_list_align" id="t{1xqCs$ivWtuh1G-Q=^">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_list_item_source" id="(9K.9n53HKC#(R_kM,e:">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="source">
                                                                                                                                                                                          <block type="func_characters_of" id="@2KB/.y@1U?M0@2};ol0qVF" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="]~QN)A(V#}7`h:E@1!yQ]">
                                                                                                                                                                                                <field name="value">b</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <statement name="template">
                                                                                                                                                                                  <block type="image_shape" id="TEK2u:{R]:QlTgbv?0/n">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_position" id="G:mfat/(odC3Nggl7ptl">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="gQ3|7gu=zMD73S6zzJu.">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="g(CF6tY~l[(~4`54`]6o">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_size" id="E$Y@2nGFrr1B1nU,F@2%cp">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="width">
                                                                                                                                                                                              <block type="expression" id="ql?B@1rEY;nuA8L3k$]3E">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="height">
                                                                                                                                                                                              <block type="expression" id="/Yn0mV@2U-7E1XSf:5W;U">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_key" id="yh)3c/S[FMb(b2!MMN+R">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                  <block type="string_value" id="vN.SjI0Wd7`}#(pYPJ/!">
                                                                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_src" id="EJ@13l8E,FlU+?Li10{7n">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                      <block type="string_value" id="qjkB.qo@2YKMmW2M0pkQ,">
                                                                                                                                                                                                        <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="grid_shape" id="5`rzBJGQ^yD,zaopzhIX">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_grid_dimension" id="KcwAzK)[Lcm,;.HC;q[E">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="rows">
                                                                                                  <block type="expression" id="?VpRo31(9FQYE1?:yq#B">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="cols">
                                                                                                  <block type="expression" id="BDwBQ`jmicYO$}hhA8o0">
                                                                                                    <field name="value">4</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id=".iN+oo@2D7.%~hsEJJ|pN">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="H=CLnP(B@1U8q1InQpXjg">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="rbKnM4$_D6sO^IvD[Asr">
                                                                                                        <field name="value">340</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="dbRgsk/;6g_6!;+B18L6">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="J/XVIJ_Fb1VV@2Q+WLn{,">
                                                                                                            <field name="value">780</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="et)GY(]bwPHDO`c}~hOs">
                                                                                                            <field name="value">160</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="CSiVnuu5c{w}X1GVB5rR">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="y(ocRJ}C9V{ply,?]ZaX">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="io7we^6WV,n�.nm=s^">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_source" id="cTY##yaQiBJy)#Ot~qLH">
                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="?`.K3T[f,Uf)W5w6A]8p">
                                                                                                                    <field name="value">[0, 1, 2, 3]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_random" id="!M`))x_b#|LZ@2O{Me!(a">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="random">TRUE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_show_borders" id="@2.@2,Q]YVwe=B!+DW/mQN">
                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                        <field name="value">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_template" id="/5L([QLGIzC-@2y48A29V">
                                                                                                                            <field name="variable">$cell</field>
                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                            <statement name="body">
                                                                                                                              <block type="variable" id="Vl4X}]2gF,u}~N.0uBAe">
                                                                                                                                <field name="name">data</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="{`o_ylJ^485/.@2)!jJtn">
                                                                                                                                    <field name="value">$cell.data</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="grid_shape" id="l4gy,T6]Wh~/Lj3B/RdF">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_grid_dimension" id="`T+2=bp7J9ox4hq]6IQL">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="rows">
                                                                                                                                          <block type="expression" id=":FRRtf1^VUbD)cu@2;?J~">
                                                                                                                                            <field name="value">3</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="cols">
                                                                                                                                          <block type="expression" id="f~LSdFLS{d=X]xA1yk}U">
                                                                                                                                            <field name="value">1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_position" id="Z,]M-/d]U1vY[opn^.2K">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="+5QAH4Fm%e|/YYxihTWT">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="A]X(oBzPk#pl8?HR]U8n">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_size" id="#blPWMfSl9YS6Mnui9hA">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="width">
                                                                                                                                                  <block type="expression" id="GtrGl}l_iS-`MC7a`24S">
                                                                                                                                                    <field name="value">100</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="height">
                                                                                                                                                  <block type="expression" id=".{b6jx@2~BsRKxzWqHID$">
                                                                                                                                                    <field name="value">140</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_anchor" id="LW(VR#U?F(2lCT!@2@2Q}3">
                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="cuq@2O%~?t#omPnG{G2$$">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="d;XPb-N__%-N]V@27mWeP">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_source" id="]I9I7bI,$F`prnEg9%zS">
                                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="sMuZ}P[LgLPE@2C@1aphA;">
                                                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_random" id="/-d04DSorjCSB[HTQ%QS">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_show_borders" id="p}=r9a^!SyiEPS$pb?xS">
                                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_cell_template" id="G]qyxkY1W3T.Hz6Pc=FB">
                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                    <statement name="body">
                                                                                                                                                                      <block type="if_then_block" id="_6yHV+eREUe;FaeR?]1B">
                                                                                                                                                                        <value name="if">
                                                                                                                                                                          <block type="expression" id="|PG(9mc.FMTX1JFMdh5%">
                                                                                                                                                                            <field name="value">$cell.row == 0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="then">
                                                                                                                                                                          <block type="list_shape" id="xth;YF^qi_6mHU,PC@1Xd">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_list_direction" id="|5/v@1c)F#pKi-Hz7tbN{">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_position" id="0%`_q)TXL[40Sgz0?O7E">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="qE7+yBTe:uh{u:4Cu(Ci">
                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="uUOp{1YuPG[yYVQ#G~a8">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_anchor" id="M)iLELaxX?IP(,1Rr}Xs">
                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="c-7Hn.pw{IVqM%!Q-G|a">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="gq@1xBN^T.1:vfjk-P|5i">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <statement name="items">
                                                                                                                                                                              <block type="list_item_shape" id="GF57PEirB+yL#aO0Xer?">
                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                  <block type="prop_list_align" id="pBi)F$!xe}w2%O4^RD`|">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_list_item_source" id="e_d@2}iXfo%qH_dvJezC7">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="source">
                                                                                                                                                                                          <block type="func_characters_of" id="HQs,05b|!rY$BX775PAg" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="/[B!@2Anp}tmrr3kqjhK+">
                                                                                                                                                                                                <field name="value">Num[data].a</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <statement name="template">
                                                                                                                                                                                  <block type="image_shape" id="KmUtTE;].CC1{vs%7N$c">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_image_key" id="FCf.2e3yT1ryfAHoIVEk">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="rBA7}!d,Ac2(uI{fuYhx">
                                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="+Jk|)Kh:nEnhc7j,RBmQ">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id="-..)prOO|b-jktX08ffN">
                                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="if_then_block" id="XPM9{i5_ZKu}r0|-?x.=">
                                                                                                                                                                            <value name="if">
                                                                                                                                                                              <block type="expression" id="q27:rYV2oPO[E-p4!?bl">
                                                                                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <statement name="then">
                                                                                                                                                                              <block type="image_shape" id="h$O^O$QE2!b/A7u9$77v">
                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                  <block type="prop_position" id="Vc=jl}gd.Fv4Y$}1:UYM">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="N@2ALHNEC?a#.yE#(l,6C">
                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="eR:-K@1)%3K=f]Cb.RxPu">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_key" id="0igVEboz:.Y#M3BiQ!E!">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="3-i_%eWkYU_x5R~gUL+f">
                                                                                                                                                                                            <field name="value">line.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="-:d|@1qhz0$qE`gBU.)uD">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id="UC@2rUd8y{#u+cO0YG}Oq">
                                                                                                                                                                                                <field name="value">${image_path}line.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="if_then_block" id="a_yD0:EC16UKdN2DjX%1">
                                                                                                                                                                                <value name="if">
                                                                                                                                                                                  <block type="expression" id="ddif:1J.B|c?517O=?}/">
                                                                                                                                                                                    <field name="value">$cell.row == 2</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                  <block type="list_shape" id="X{4gyg5w,L[4C@1ZflC)s">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_list_direction" id="):Z^R:mlDQ4L}9BnuhtM">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_position" id="ih5R9ou):7=3~2|dw~4H">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                              <block type="expression" id="n[bM$=vjS]LA(L`m?3q_">
                                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                              <block type="expression" id="S7{]F0PkiGm,k%]zPXAw">
                                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_anchor" id="jdom5G!DtI!6UMKz@2edN">
                                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                  <block type="expression" id="LF177qe]eZwDK]kI;Xh1">
                                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                  <block type="expression" id="5QQHe=KwqtnkR-jGpD9N">
                                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <statement name="items">
                                                                                                                                                                                      <block type="list_item_shape" id="WNLJ]kU3u4NY|NUpTW|m">
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_list_align" id="0XDNe]RvC6DQ`tvdC?/v">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_list_item_source" id="W6hHaN_/bj@1XQTN0NS@2$">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="source">
                                                                                                                                                                                                  <block type="func_characters_of" id="@19!=zy(yeAAu2VsJ;^.8" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="expression" id="M~HZ.R7!tD)6}COJVHp7">
                                                                                                                                                                                                        <field name="value">Num[data].b</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <statement name="template">
                                                                                                                                                                                          <block type="image_shape" id="S~-$hAOMk^[^h{O|M+er">
                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                              <block type="prop_image_key" id="i5IOH~Fpuy.G6dN_aLa%">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                  <block type="string_value" id="IOT9@2xs@2q+Tuc|!$5V,c">
                                                                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_src" id="SweO1v7vI4l[q{}vrs+(">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                      <block type="string_value" id=":d9?yv~m7N_7%v%r@2e#X">
                                                                                                                                                                                                        <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="choice_custom_shape" id="mB,57bFqxn:{oVm.w[1V">
                                                                                                                                        <field name="action">click-one</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id=",{-YK+as%=?(bQ`{30cA">
                                                                                                                                            <field name="value">Num[data].a/Num[data].b</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="template">
                                                                                                                                          <block type="image_shape" id="gW/w/d=,_o]ZcgOzim4}">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="^KgpH5k#jPm(dR~Bi!|k">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="U+T1we$A4`B@2L!w@1c4uY">
                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="e4BWVu6{oTDn|1#v,#8I">
                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id=".m.MJL,o`z[cNct)8]{J">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="t=M7Oe;:HqjSvG0P/%%$">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id="~Wg!I|v_LO.VWcOFRq?}">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="8AskUZY:3{U)?y]!W1|R">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                                                            <field name="value">box.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="4%Z)QBSu}^_u.z4`/+qG">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="func_add_answer" id="=sUNFm70KVNQO4Yfb`d_" inline="true">
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="ee9PN]$V#!)Erz(@13eG{">
                                                                                                    <field name="value">n1/n2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <statement name="else">
                                                          <block type="if_then_else_block" id="+Db5,K0]VitU9ZQKh$_-">
                                                            <value name="if">
                                                              <block type="expression" id="lf6BT@15%tK]]s#GMd6?E">
                                                                <field name="value">type == 1</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                        <field name="value">20</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                <field name="value">Simplify the below fraction.</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                        <field name="value">36</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </statement>
                                                            <statement name="else">
                                                              <block type="text_shape" id="=HV@1:KploRIgdZ.hy^Q{">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="trQ},uRf@1^B^I;8[9;l@2">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="wkabZ#S?zB,O=hX}X%J}">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="5UsV[)/]CDZdU7[H~,7k">
                                                                        <field name="value">20</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="eK==YYFw:q(V(sC_uG^g">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="2J4hsa+?sLvw:=l4ZH0c">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id=")c98^nNnr-75w2IgnfJ+">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="RW|K]G%:#smg!W^tRmBW">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="~~{WTkr`|#vTcg52RHT+">
                                                                                <field name="value">Simplify the below fraction in its simplest form.</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="|I6jMWZNG!Ou!ood`ndR">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="t`/+8w^FPk]E,Cx?CmCl">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id=":O@2`[0/nQo+U6vu)A5,S">
                                                                                        <field name="value">36</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="B^vS:fjxFzQmaY=OD/(P">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="6R0?El?B8q(zLW!KRKKn">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="S)pTA[sF]D$%V.=f1{S5">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="b7ssGp;sDXqV5vGgPV.$">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="Qr-p@2Wq%^0Iaz_oIMT47">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="]S0TXtCUoIQGt#$Xl=FI">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id=".$]up;g.RdV@2{v3`pW3}">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="Xv/0_Pkk6-48y#@2~Qkjn">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="F@2$1@2j4Hyuh3QLy;GP6G">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="xd3`D2cC.RymZMZdy6d1">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                            <field name="value">220</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="sfI;e#%l;3mZUGiL6^Pi">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="GHZ]T)k#OypH$nEj0Y1q">
                                                                                <field name="value">500</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="?m`9JO-K@2yQIJ]ruD!]A">
                                                                                <field name="value">140</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="|;{5)0(y,y1_2~t(@1/(e">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="6t3SD(t4FMn9FO0%nJC:">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="|(B@1gV`^uk%1vHjZ]BWk">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_source" id="svM542u;BufZUHO~l2uc">
                                                                                    <field name="#prop">cell.source</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="odoO?V2Do@1b-B?D[W=">
                                                                                        <field name="value">[1, 2, 3]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_random" id=";MoMf59z/2b.Sr5RoU8{">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="random">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_show_borders" id="tmib/xh6?[L@18xn3!.]@1">
                                                                                            <field name="#prop">#showBorders</field>
                                                                                            <field name="value">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template_for" id="f@1S8cJDn?`/1d6KzCBx+">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="condition">$cell.col == 0</field>
                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="grid_shape" id="za%@2U7,7Q;+!mmq$xs5v">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_grid_dimension" id="z#g206JXIEHK_pA:X6_u">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="rows">
                                                                                                          <block type="expression" id="~Nmr/`%O|F%o[Xgz{2}k">
                                                                                                            <field name="value">3</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="cols">
                                                                                                          <block type="expression" id="},$ECmKt(m2II8%fSn/A">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_position" id="fH$!w9O;mQ4kQ7TiER8=">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="O}d`Q.kL]#@2#/+T4%Y?P">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="?r}uXF2Yifdy-#N%2pEc">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="+f1yf_b?!ga.VB_:4Vb/">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="Uy+n+c,/vrr=?{2)gceg">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="EUuv@1R]upgIxxY9407E/">
                                                                                                                    <field name="value">140</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="@2Ej+c2G(Dm]k25RZ@1:d}">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="h^nD)YJ+blcBhn17dL0#">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="8}U+h+MZ?#%t.0v0a9HI">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_source" id="(@2yBq^w@2)Zu@2qyUpoywb">
                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="y3fE]b6qWDxGCgOD.!rq">
                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_random" id="r8mS88os`@2E7|z`?aOuK">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="random">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_show_borders" id=")td6Cm,~@2@1,8zA]xf3[3">
                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_template" id="rxG]Mw1=p6sIF5n/g(0L">
                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                    <statement name="body">
                                                                                                                                      <block type="if_then_block" id="@2iU=7wF6=qCV.DstZ6j3">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="fuNDuR+0uLv#.i26VW(U">
                                                                                                                                            <field name="value">$cell.row == 0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="list_shape" id="mnaF-Nz)IMs~aL/vNTGk">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_list_direction" id="e_vJTdY!@2?q2.KC?@1c}L">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="$_X4@2`Wj9zY)drt0d0C{">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="ekIo8H,:`87D/9w:ih}m">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="?;x1c#f{-!Xp$X_6i%G!">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_anchor" id="P0;kqkOd4[RITn4~}!X9">
                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="`w{V=nl)|IW@1up=A[|30">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="^/kVrvhs5z{kBD@2=%)(u">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <statement name="items">
                                                                                                                                              <block type="list_item_shape" id="O}8(TGk}#oTGsc19dgf[">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_list_align" id="$9{:WL6T~Ya7Y~|m/Zm)">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_list_item_source" id="0E-4RiRaOU$Y`6r@2847v">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="source">
                                                                                                                                                          <block type="func_characters_of" id="5_fF@2I1Nj^-S`~UGCh@1:" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="}L+)[LLKMbpN;slb]z8e">
                                                                                                                                                                <field name="value">a</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="template">
                                                                                                                                                  <block type="image_shape" id="!Zh;F$Dt|cvdOMmoDUHT">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="5CL_.]R7=vKMJ`C,%mpr">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="uRA|Bhf]sLFq,sx=b:14">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="0yIUVvAz.[)qy?7sQu#h">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="bNJWii:[@2udF(sPZ-=h1">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="R^B_TwVgQ3wfwOy!?XUo">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id=")=5E(mt5$EIxHG5q55-N">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="0l6@1u;=47._$f{Sr{V;?">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="OfBe)Un1($mW8{gj!Y}T">
                                                                                                                                                                    <field name="value">${$item.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="[gNQ}[T$Q+.pGasHZ#d3">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="DB)RcScpdbFR5FDR|m9U">
                                                                                                                                                                        <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="if_then_block" id="$hgIlm4uopZeIQ`%u:^Q">
                                                                                                                                            <value name="if">
                                                                                                                                              <block type="expression" id="6`VsxT}nr8)T_,k%7.-U">
                                                                                                                                                <field name="value">$cell.row == 1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="then">
                                                                                                                                              <block type="image_shape" id="EY9/@2^dRtZz@24sjeZ@28?">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id="(ADWm+A?(Z]G`45v|j2U">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="tZSBM$)3^J|E!`,hU7?y">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="p|nbvX4echM!v@2`nJWen">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="(+{YwIS/q[;/6.$+Ai5t">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="VMcXXxB/}9q(i;tdPz+q">
                                                                                                                                                            <field name="value">line.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="uO7@1+o/Mc?6YVRsk@2@25h">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="p@2-?~sJa@1~7jv+t%|Xs+">
                                                                                                                                                                <field name="value">${image_path}line.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="if_then_block" id="OShS{EhObYlsM0LAR;zN">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="A_zwL)ft5-wM2/Op-rh;">
                                                                                                                                                    <field name="value">$cell.row == 2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="list_shape" id="E0aR2wSlp0JaQ^2@2KU1s">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_list_direction" id="{DJ4~_H3JvSfU@2;Ve++_">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_position" id="H5+%u$Blfp.G#ZPg}@1F.">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="rsiHLQw@1:76u;E4GeM@2-">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="a[htQF..3e-#7ck{uF2-">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_anchor" id="Yum)Xr=jMAd;@23[;?@1!R">
                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="JS},.#xyk[v26FxL+=ql">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="xVM1P%gu:c)!)!SS)aZS">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="items">
                                                                                                                                                      <block type="list_item_shape" id="wG//@1f|Ix{kd@1w^7kJqJ">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_list_align" id="A6h0WFz0h$tXePaXEPK@1">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_list_item_source" id="$x|qPM$F.zx,,P.Os(+`">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="source">
                                                                                                                                                                  <block type="func_characters_of" id="H@1,:rRhKY#y0H]FHh`?p" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="D$4)Et-jKqUzRC}CjfL4">
                                                                                                                                                                        <field name="value">b</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="template">
                                                                                                                                                          <block type="image_shape" id="eryE@1hcyFB5Dz9~.oltb">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="b)!Q+PlI}LoVsOr{G5u|">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="XT9WyIo)%Z^ggw[LkNPq">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="r(U~,;#}lnQs%O]i3K5s">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="xKWXINK^kB%^)NP@1,1Ny">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="@2y]%;?L(3k!o}K=-M6;2">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="MEzD9h+Op;6`,a3q`V[T">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="RScd)h)EoJ@1iD{IKXy_m">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id=":yKOf+IMKlJ6Q@1Wi.2yM">
                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="!soW@1qNjB.$`nTF)xqPI">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="6i6C1`:C:g)_]-42(U?j">
                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template_for" id="0,TkUU4c@2~$5aud4XY^x">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="condition">$cell.col == 1</field>
                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="/xpI4C=hM}2dLa]^96N:">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="9R|7-W,4YXm;Z=5J^s}z">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="3A{h(dYe#]n#%O4q$%iV">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="3mGbH%4`I`=zsM%/xIvI">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="W}s)T`(Q@2?+d4Anq{r2N">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="UYr1|.Iu=%;Ps2B~x#">
                                                                                                                    <field name="value">equal.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="7vQuR}x_045sk@2y4{$e/">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="!#:8lV}8IVGlrW7goL+T">
                                                                                                                        <field name="value">${image_path}equal.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template_for" id="E;g@2qG4.~w/z0UoxIQWL">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="condition">$cell.col == 2</field>
                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="rows">
                                                                                                                  <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                    <field name="value">3</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="cols">
                                                                                                                  <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                    <field name="value">1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="!P!d`XcnrZxX|-7?h.l8">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="?-^mL.lH=qaL1[2h8Ci8">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="dP!NWz:#MEizzS;,[l5Z">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                            <field name="value">100</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                            <field name="value">140</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_template" id="Bthn1FuL6/+ai.Z.1:Ur">
                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                            <statement name="body">
                                                                                                                                              <block type="if_then_block" id="+=u8VtT8s)R!(=07~u.j">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="0PBv.YL;@1T+Zcg~3P!f%">
                                                                                                                                                    <field name="value">$cell.row == 0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="image_shape" id="`I-sHWyw84@1lkdM6AaA^">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="A(S_J5o?|jmgy#),{9|V">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="TJ#OiweCAOWEP4{4`BvT">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="Ov[WGAGN?vУgimWX{">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="!O@1+rn;I=d2P]^)Y=yHl">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="OK5$sGi`/-7O}y{`S{FF">
                                                                                                                                                                <field name="value">shape.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="xmTW|^TsnY|RCsCJht1F">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="z6C@2iqkxlI9jvCN2m!4?">
                                                                                                                                                                    <field name="value">${image_path}shape.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="algorithmic_input_shape" id="72WTWABkm`[Drvusj.Ye">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_value" id="IPbAEU28/R-~ic,ce=uU">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id=";Z?]o^ln]s|U6Lt}?kuq">
                                                                                                                                                                <field name="value">n1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_position" id="=]en+z+0Siuyr,e@1haK=">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="u$9J5FLDw%SX%[q)x`]m">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="Ge3;hQV,GtIJS#b}tPJJ">
                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="2NJdt4:6$[+gjUzps`M+">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="+zY(!tKjFC~Mb,o(nRBv">
                                                                                                                                                                        <field name="value">106</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="-tg$uM|FFHN-gjg!J)Vl">
                                                                                                                                                                        <field name="value">61</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_input_keyboard" id="f/K=Xp=Nq{NMG0+F`x#@2">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_input_max_length" id="+upn/Fx|,x8QxmHkx,PS">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="maxLength">
                                                                                                                                                                              <block type="expression" id="YyCWL[ccrMRHTIn=uoZb">
                                                                                                                                                                                <field name="value">n1.toString().length</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_input_result_position" id="Qu[Vn=OUd!770yVV@2?~^">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="resultPosition">right</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_tab_order" id="w+@2yDIK_?7gK3zYCdG59">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="tabOrder">
                                                                                                                                                                                      <block type="expression" id="RhOJvG%:Se^@1x%s7S!:c">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_stroke" id=")s5SaU?.4S3BPg:=Y?7T">
                                                                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_fill" id="P{9]i{Ilw{8$Du8#tb?t">
                                                                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style" id="ic}c8R=`:06A)ZFIC4p?">
                                                                                                                                                                                                <field name="base">text</field>
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_text_style_font_size" id="RAM]nG.zRSoOs4JeED66">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="fontSize">
                                                                                                                                                                                                      <block type="expression" id="s-hcRe4_4l`+_I,.K!.h">
                                                                                                                                                                                                        <field name="value">36</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style_fill" id="aePOXJ%)b8gro0R`G,`}">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="fill">
                                                                                                                                                                                                          <block type="string_value" id="qbKj|_wAQYd7bP;nx|lq">
                                                                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style_stroke" id="(A[QTHCcq]%zPX;IQPgK">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="stroke">
                                                                                                                                                                                                              <block type="string_value" id="SyZb|hP~$:9N?8/u(rlT">
                                                                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="~.N2BdD4cA`=HZ}=}QKk">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                                                                  <block type="expression" id="=U{bR(zoW_;5]lF$)WM2">
                                                                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="if_then_block" id="u7tivOlukN0(6Av0HB@2?">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="F;=V#s7J:JkAh4VYkZsi">
                                                                                                                                                        <field name="value">$cell.row == 1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="image_shape" id="{9?Z``WIMEZB@1i8p{0%f">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="ff@1oJHy@2$2FV-r07TOUl">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="+6KYVV%+a@181#J_!@2ehd">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="jAo^qvf%[1!Xrx7Fd9Hz">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="%S[ea$5C5k4˴#sSjV">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="@149d48{?0gc[y,e-Wj@2X">
                                                                                                                                                                    <field name="value">line.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="~1QijG`s/tI`kXIpXZt~">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="y=BfAe(8zBe-GwroQH[d">
                                                                                                                                                                        <field name="value">${image_path}line.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="if_then_block" id="srN@1XjKd,0{)_b}4E{o?">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="%o!C!b.ukA)A@2UTp!!fV">
                                                                                                                                                            <field name="value">$cell.row == 2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="if_then_else_block" id="TUzbht+)Ab(_AOtH$G%f">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="3+b8m)EC./btBY-9nn">
                                                                                                                                                                <field name="value">type == 1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="list_shape" id="ywSA.QU~Xiivc2q:B{jJ">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_list_direction" id="yX=y{wnKz.K{0|dv8Akg">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <field name="dir">horizontal</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_position" id="ZZ5wUdHK!nx6M:v]~#Wp">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="L`i?RYpjh]^SlW]}AYe9">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="Um0pqN$!hfr]sA^m@1c#A">
                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="lS,!i:4LV~%_6HS@2ixhA">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="G(S=I4oPIsC?-Zs;S{qE">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="[!JBp_Aq.?A^WywM8/vj">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <statement name="items">
                                                                                                                                                                  <block type="list_item_shape" id=",#UEi^Q8:J}Y!)xpti1I">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_list_align" id="4;?Xeion@1:sk{TlCz4+q">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="align">middle</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_list_item_source" id="[6cY~Y(9frE./5e5F?}D">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="source">
                                                                                                                                                                              <block type="func_characters_of" id="aviL)f{yj)?V18apEaFG" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="#1Ka9iJHv)Qyl1.sEG@2]">
                                                                                                                                                                                    <field name="value">b</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="template">
                                                                                                                                                                      <block type="image_shape" id="HSSNtPx:{[)WIY!ie1,U">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="T5y4J]FFY#F7ds_big`H">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="]!XvG_nR!Y-Xq-svM@2gy">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="bN([]e[4s=_CKhn6%VnT">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="=YyuC9j+}4XTwd4%s.s}">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id=":LTk4/I_[VZXxyMmxs4F">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id="?jCCY0/=Mse[QiynyBMD">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_key" id="iik1@1E~R4!XOVO(B4]..">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                      <block type="string_value" id="N7U+Fy4vE_Ci42c@14uC?">
                                                                                                                                                                                        <field name="value">${$item.data}.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_src" id="k[]hdN^Axl@1nBy!uJu3c">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                          <block type="string_value" id="tt=DXFo.O3AC~l{TPpM1">
                                                                                                                                                                                            <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="image_shape" id="[B{S3rI@1uOp/mLeSQ@236">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_position" id="KdM{p3FR#$cFCejf[ClB">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="xO%YC_i}tNz~zKe)a2r}">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="|nBK{oufNkI)|R7_Zz^Y">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="%5)Hpl0z-]MSb.nu0C.o">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="#L4~QXo/i6Z}@1hh_jndW">
                                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="Br8E8LD;q:2Ne0?VzB,_">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="y2AL+=5lP/W8un!CbI1_">
                                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="algorithmic_input_shape" id="Rf6z;}O@1j=`jnHb:@1)(1">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_value" id=":hs5m(Qz%T-@1AcH!{=2n">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="PQ7Fu8?%M@22TikSzz3e`">
                                                                                                                                                                            <field name="value">n2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_position" id="f;^NIFlZH7n:3Wz;EzxY">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="?@28R}mfTCD@1jKes8gj#@1">
                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="b:`%qbTDfJrZ-/r16xR|">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="=Z=3p@2{Z.8L}yqunQFr7">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id="jZ=Ew/f-;L6Euxq!qG[O">
                                                                                                                                                                                    <field name="value">106</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id="Q))(pydoJwDd%!Fko5aG">
                                                                                                                                                                                    <field name="value">61</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_input_keyboard" id="v!Rl^Y$@1wAPWoZA_f)@1w">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_input_max_length" id="VFB@2)^7xY]{vYrcE|]^n">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="maxLength">
                                                                                                                                                                                          <block type="expression" id="V~_n,PuKEetlV,m9GEdn">
                                                                                                                                                                                            <field name="value">n2.toString().length</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_input_result_position" id="~{(=P,(Fk.`$p6@1WQ7wA">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <field name="resultPosition">right</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_tab_order" id="AeCR0:d,Hmzw_]Z0]Tw~">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="tabOrder">
                                                                                                                                                                                                  <block type="expression" id="|cS-,YTb$=/t(+z[oGOT">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_stroke" id="6rj|G);f6QE+ncjohC}7">
                                                                                                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_fill" id="7p!5I:E`tSTc6jNgla5k">
                                                                                                                                                                                                        <field name="#prop">fill</field>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style" id="t@13!Kuiu8R`hT/`q84G{">
                                                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                              <block type="prop_text_style_font_size" id=",ajkBL:OVUPuzPhmE~z4">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                                                  <block type="expression" id="Wr28:]K%#:Y9MN1GPjiA">
                                                                                                                                                                                                                    <field name="value">36</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_style_fill" id="FWW|X%4^-YsX/YVHQZ0M">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                                                      <block type="string_value" id="wZ]9_)DesT9-4YxvP5S(">
                                                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_text_style_stroke" id="(.;iiRI,1v+UJHH[PZm=">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="stroke">
                                                                                                                                                                                                                          <block type="string_value" id="jzfH(a(q@1$$[$36bL@2t5">
                                                                                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="](jg,uXBF+TGNB_IkM;g">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                                                                                              <block type="expression" id="G~ISu}!94,7w@10,hq~?j">
                                                                                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="variable" id="g^fNA7r|uf`{=(W5Q4eL">
                                                            <field name="name">load</field>
                                                            <value name="value">
                                                              <block type="custom_image_list" id="c5T3AfJ]#3u7]?Nmpv]1">
                                                                <field name="link">${image_path}</field>
                                                                <field name="images">left|right</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="statement" id="+KUy-S2-6N^^kF7mBn.~">
                                                                <field name="value">var tree = [];
  var prime = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 47, 53, 59, 67, 71, 83, 89, 101, 107, 109, 113, 127, 131, 137, 139, 149];

  for(var i = 0; ; i++){
  }
                                                                </field>
                                                                <next>
                                                                  <block type="partial_explanation" id="efIy2ccGPEkInp-i7SOV" inline="true">
                                                                    <value name="value">
                                                                      <block type="string_value" id="Qgc#j|2fUnpT]B:VgI=Y">
                                                                        <field name="value">&lt;u&gt;Step 1: First, build a factor tree&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;div style='position: relative'&gt;
  &lt;img src='@1sprite.src(left)' style='position: absolute;'/&gt;
&lt;/div&gt;
                                                                        </field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="variable" id="yhAvR[@1.KE#]+A_=0,|]" x="544" y="11368">
    <field name="name">tree</field>
    <value name="value">
      <block type="expression" id="MX;,+FGbube9DwhusSRV">
        <field name="value">[]</field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */