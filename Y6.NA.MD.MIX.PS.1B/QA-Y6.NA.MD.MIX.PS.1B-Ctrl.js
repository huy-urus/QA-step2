
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.MD.MIX.PS.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.NA.MD.MIX.PS.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "type",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          },
          {
            "#type": "variable",
            "name": "X",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "3"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "type",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "3"
                  }
                }
              },
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "type == 3"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "4"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "expression",
                      "value": "X*100"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "2"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "6"
                      }
                    }
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "type",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "3"
                  }
                }
              },
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "type == 3"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "3"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "6"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "expression",
                      "value": "X*100"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "5"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "9"
                      }
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_item",
            "value": {
              "#type": "string_array",
              "items": "grapes|apples|oranges"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 2"
            },
            "then": [
              {
                "#type": "variable",
                "name": "list_item",
                "value": {
                  "#type": "string_array",
                  "items": "milk"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "list_item",
                "value": {
                  "#type": "string_array",
                  "items": "chocolate|white chocolate|chips"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "unit",
        "value": {
          "#type": "string_array",
          "items": "kg|litre|g"
        }
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "list_item"
          }
        }
      },
      {
        "#type": "statement",
        "value": "function randomJS(a, b) {\n    return Math.floor(Math.random() * (b - a + 1)) + a;\n}\n\nfunction re_price(item){\n    if(item == 'grapes') return randomJS(30, 70)*10;\n    if(item == 'apples') return randomJS(30, 50)*10;\n    if(item == 'oranges') return randomJS(23, 40)*10;\n    if(item == 'milk'){\n        var ran = randomJS(0, 3);\n        var prince = [100, 120, 150, 200];\n        return prince[ran];\n    }\n    if(item == 'chocolate') return randomJS(4, 9)*50;\n    if(item == 'white chocolate') return randomJS(4, 9)*50;\n    if(item == 'chips') return randomJS(18, 50)*10;\n    reutrn -1;\n}"
      },
      {
        "#type": "variable",
        "name": "item_price",
        "value": {
          "#type": "expression",
          "value": "re_price(item)/100"
        }
      },
      {
        "#type": "variable",
        "name": "price",
        "value": {
          "#type": "func",
          "name": "formatNumber",
          "args": [
            "money",
            {
              "#type": "expression",
              "value": "0"
            },
            {
              "#type": "expression",
              "value": "item_price"
            }
          ]
        }
      },
      {
        "#type": "variable",
        "name": "answer",
        "value": {
          "#type": "func",
          "name": "formatNumber",
          "args": [
            "money",
            {
              "#type": "expression",
              "value": "0"
            },
            {
              "#type": "expression",
              "value": "(type == 3 ? X/100 : X)*item_price"
            }
          ]
        }
      },
      {
        "#type": "variable",
        "name": "cost",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Find the cost of ${type == 3 ? X: X} ${unit[type-1]}${type == 2 ? 's':''} of ${item}"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "number",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "5"
          },
          "max": {
            "#type": "expression",
            "value": "8"
          }
        }
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "600"
            },
            "y": {
              "#type": "expression",
              "value": "250"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "cost${cost}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}cost${cost}.png"
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "630"
            },
            "y": {
              "#type": "expression",
              "value": "250"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "150"
            },
            "height": {
              "#type": "expression",
              "value": "60"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY+3"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "$${price}/${(type == 3 ? '100':'') + unit[type-1]}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "36"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "arr",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "7"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i >= 0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i > 7 - number"
            },
            "then": [
              {
                "#type": "variable",
                "name": "arr[i]",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "arr[i]",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "2"
            },
            "cols": {
              "#type": "expression",
              "value": "4"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "320"
            },
            "y": {
              "#type": "expression",
              "value": "180"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "350"
            },
            "height": {
              "#type": "expression",
              "value": "180"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "arr"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "$cell.data != 0"
                },
                "then": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "$cell.width"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${item}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${item}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "10"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "Answer:"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "dola.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}dola.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "150"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "answer"
                      }
                    },
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "150"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "74"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "answer.toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "bottom"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "42"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Learn the key details.</u></br></br>\n${X} kg of ${item}.</br></br>\n$${price} per kilogram.</br></br>\n\n<center>\n<div style='position: relative; width: 400px;'>\n  <table style='position: absolute;'><tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 8"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "arr[i] == 1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><img src='@sprite.src(${item}.png)' style='max-width: 70px;'/></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "i % 4 == 3"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr><tr>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>\n</table>\n<img src='@sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/>\n<span style='position: absolute; top: 60px; left: 380px; color: black'>$${price}/kg</span>\n</div>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Find the price.</u></br></br>\n\nSince per kilogram of ${item}, it costs $${price}. There is ${X} kg of ${item}.\n</br></br>\n$${price} x ${X} = $${answer}"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Learn the key details.</u></br></br>\n${X} litre${X > 1 ? 's':''} of ${item}.</br></br>\n$${price} per litre.</br></br>\n\n<center>\n<div style='position: relative; width: 400px;'>\n  <table style='position: absolute;'><tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 8"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "arr[i] == 1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><img src='@sprite.src(${item}.png)' style='max-width: 70px;'/></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "i % 4 == 3"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr><tr>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>\n</table>\n<img src='@sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/>\n<span style='position: absolute; top: 60px; left: 380px; color: black'>$${price}/litre</span>\n</div>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Find the price.</u></br></br>\n\nSince per litre of ${item}, it costs $${price}. There is ${X} listre${X > 1 ? 's':''} of ${item}.\n</br></br>\n$${price} x ${X} = $${answer}"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Learn the key details.</u></br></br>\n${X} g of ${item}.</br></br>\n$${price} per 100 grams.</br></br>\n\n<center>\n<div style='position: relative; width: 400px;'>\n  <table style='position: absolute;'><tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 8"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "arr[i] == 1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><img src='@sprite.src(${item}.png)' style='max-width: 70px;'/></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "i % 4 == 3"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr><tr>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>\n</table>\n<img src='@sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/>\n<span style='position: absolute; top: 60px; left: 380px; color: black'>$${price}/100g</span>\n</div>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Find the price.</u></br></br>\n\nSince per 100 grams of ${item}, it costs $${price}. There is ${X} g of ${item}.\n</br></br>\n$${price} x ${X/100} = $${answer}"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="-167" y="-118">
    <field name="name">Y6.NA.MD.MIX.PS.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.NA.MD.MIX.PS.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="CO/ca5)E2gy]^#b_nhDZ">
                                        <value name="if">
                                          <block type="expression" id="Z2^C|U{{6;5Q-:1t806[">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="80S~,mzj!|G}v|f4UD[S">
                                            <field name="name">type</field>
                                            <value name="value">
                                              <block type="random_number" id="~rBeIDpKYLOC|C:Ki.c#">
                                                <value name="min">
                                                  <block type="expression" id="1E.LLrbyY/0MOcfoegM?">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="TSxaYH_/dlH,5-4w03%Z">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="9jJ_Nb#Ry+0H@2PELbQK$">
                                                <field name="name">X</field>
                                                <value name="value">
                                                  <block type="random_number" id="|S_BFH65I?Nc(`OPs-A,">
                                                    <value name="min">
                                                      <block type="expression" id="P{dUwr#pIXpZmHP_f2}J">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="E@1`IvsCTS(;D#VnDu52=">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="yjN2kftZeMZ5;!wS5X3f">
                                            <value name="if">
                                              <block type="expression" id="/Kz/?n,4E7$ALMJAkNFo">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="s;ukhsFQ/~@1Gvc:Ryf#N">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id="^O)]4[,Fj1cgp@2Q1R:XV">
                                                    <value name="min">
                                                      <block type="expression" id="N-9kO|9Hax)}rx2!Z@1W1">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="[?;;clhPt@1RL!r,}{Myc">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_else_block" id="dwMJmngZ]f,8P[n2[=/s">
                                                    <value name="if">
                                                      <block type="expression" id="x?7fIDYMeDgS.jX=bS)S">
                                                        <field name="value">type == 3</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="pcqcHpM@1du1={R0f7hB1">
                                                        <field name="name">X</field>
                                                        <value name="value">
                                                          <block type="random_number" id="4GfUP$SA,@1|c0o#FDLw+">
                                                            <value name="min">
                                                              <block type="expression" id="/@1%;-b;{a5:-PYwZ7yvm">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="?|CO9r{D3NdiNb|2@2NC#">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="H`t]zYZB0]CMT`_{5fc$">
                                                            <field name="name">X</field>
                                                            <value name="value">
                                                              <block type="expression" id="{lw]`sy2onFPef^UQIoK">
                                                                <field name="value">X@2100</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="%X5pOlIt[{^[Fs2}6yKv">
                                                        <field name="name">X</field>
                                                        <value name="value">
                                                          <block type="random_number" id="owrK-R@2:tOBbE`tF??$y">
                                                            <value name="min">
                                                              <block type="expression" id="6nq(ETQ6Oc6[1;|o3}`=">
                                                                <field name="value">2</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="3k`}t}qjEZ{mq8{$#S1f">
                                                                <field name="value">6</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="dlOd-0iW+LY!)|=R@1uVW">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id=")oy|8jMR@2y8g}s+UW^{;">
                                                    <value name="min">
                                                      <block type="expression" id="06}kgjt^;V0A4c:77v)_">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="E$h+#SndoR@2@2,[zDaP,d">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_else_block" id="5bAh@1X^ENM@2E_Jhepyt]">
                                                    <value name="if">
                                                      <block type="expression" id="~=Hi3TDUzHkZdIs?}F98">
                                                        <field name="value">type == 3</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="VY4LcJjYeXmwFt^gInNU">
                                                        <field name="name">X</field>
                                                        <value name="value">
                                                          <block type="random_number" id="=E{JXc!amlvLa!-7TlyG">
                                                            <value name="min">
                                                              <block type="expression" id="::V2Z=,Q;r(tNHZ3dI0H">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="=b5$i_hK,Ni_qdF^|AB=">
                                                                <field name="value">6</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id=":FseU`J@1Ej:upcd%_`P?">
                                                            <field name="name">X</field>
                                                            <value name="value">
                                                              <block type="expression" id="hUjT}wPz2t/gEdeN[P=s">
                                                                <field name="value">X@2100</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="|nmCliktEy$mmoV@2RT@1N">
                                                        <field name="name">X</field>
                                                        <value name="value">
                                                          <block type="random_number" id="z22N[/ZJ=tE95hKVO.xO">
                                                            <value name="min">
                                                              <block type="expression" id="M0mH)}2;tGOCNp${18PQ">
                                                                <field name="value">5</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="I=#Fw}UZ{Ekj^8-W8Eb:">
                                                                <field name="value">9</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="if_then_else_block" id="PrG8GZlq7jYzr0K1ob0;">
                                            <value name="if">
                                              <block type="expression" id="2`kORg42/0-k-T~J,YEv">
                                                <field name="value">type == 1</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="qXxcPq2-QkMmqIC~k(tp">
                                                <field name="name">list_item</field>
                                                <value name="value">
                                                  <block type="string_array" id="-O/4Q(wX?wgI[JG[.6;G">
                                                    <field name="items">grapes|apples|oranges</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id="@295aiy-$-~O8XKO1$5hq">
                                                <value name="if">
                                                  <block type="expression" id="H6}_PCyz3H|([}^B5lJO">
                                                    <field name="value">type == 2</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="v}}$m2Xe_cdk^FzW3hEV">
                                                    <field name="name">list_item</field>
                                                    <value name="value">
                                                      <block type="string_array" id="0xn%(8jx4)BZAK#JBiQ,">
                                                        <field name="items">milk</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="|wsZ1J{X+_kl?#M@2x/dU">
                                                    <field name="name">list_item</field>
                                                    <value name="value">
                                                      <block type="string_array" id="m#Vx4AVx#AnKCck[CP(y">
                                                        <field name="items">chocolate|white chocolate|chips</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="{~cQ{5lV/dZ2u?mI7wtn">
                                                <field name="name">unit</field>
                                                <value name="value">
                                                  <block type="string_array" id="w@2(+Yj[:N%@2)O77$KrXo">
                                                    <field name="items">kg|litre|g</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="Vm8o,^YHr[j$_CDnqxdz">
                                                    <field name="name">item</field>
                                                    <value name="value">
                                                      <block type="random_one" id="}Dp0}o3@2ZZ(f{!Z@2s|-b">
                                                        <value name="items">
                                                          <block type="expression" id="{R~Hh@1P9Jh]^U%#e%`tO">
                                                            <field name="value">list_item</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="statement" id="-0LH.SiTzo3gJ[^GxX,K">
                                                        <field name="value">function randomJS(a, b) {
    return Math.floor(Math.random() @2 (b - a + 1)) + a;
}

function re_price(item){
    if(item == 'grapes') return randomJS(30, 70)@210;
    if(item == 'apples') return randomJS(30, 50)@210;
    if(item == 'oranges') return randomJS(23, 40)@210;
    if(item == 'milk'){
        var ran = randomJS(0, 3);
        var prince = [100, 120, 150, 200];
        return prince[ran];
    }
    if(item == 'chocolate') return randomJS(4, 9)@250;
    if(item == 'white chocolate') return randomJS(4, 9)@250;
    if(item == 'chips') return randomJS(18, 50)@210;
    reutrn -1;
}
                                                        </field>
                                                        <next>
                                                          <block type="variable" id="t2H7jO{;Q;6AbpvD@1k=U">
                                                            <field name="name">item_price</field>
                                                            <value name="value">
                                                              <block type="expression" id="kr?GGft@1{Z(95da95]Nj">
                                                                <field name="value">re_price(item)/100</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="U:@1:9CwL|W5O]vK!WeR:">
                                                                <field name="name">price</field>
                                                                <value name="value">
                                                                  <block type="func_format_number" id="s@19oCu8.`~ugPMc#=QX5" inline="true">
                                                                    <field name="type">money</field>
                                                                    <value name="leadingZeroes">
                                                                      <block type="expression" id="@2p}:iRk7]f^~NgJwjNH_">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="value">
                                                                      <block type="expression" id="+Qrc%I]4PoFKo00O2yQJ">
                                                                        <field name="value">item_price</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="]snYP$nrcm!pA9l1oL[k">
                                                                    <field name="name">answer</field>
                                                                    <value name="value">
                                                                      <block type="func_format_number" id="jL]2.B|7vN1mi!AE9-%L" inline="true">
                                                                        <field name="type">money</field>
                                                                        <value name="leadingZeroes">
                                                                          <block type="expression" id="f|Y4,3s~lAfS�6Nlw,">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="value">
                                                                          <block type="expression" id="JjlByXa]vNICzP`lIf~j">
                                                                            <field name="value">(type == 3 ? X/100 : X)@2item_price</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="8~vV]yUmj-N]J:sFO)54">
                                                                        <field name="name">cost</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="6Nr=jqU0)D$+I%}g{T1|">
                                                                            <value name="min">
                                                                              <block type="expression" id="+(Rg9]|iljmA+XXbBzKR">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="bDF$69$:I5g5;8-$J16L">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                    <field name="value">20</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                            <field name="value">Find the cost of ${type == 3 ? X: X} ${unit[type-1]}${type == 2 ? 's':''} of ${item}</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                    <field name="value">36</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="variable" id="-S17px,,5+Bj!EZLd,W@2">
                                                                                <field name="name">number</field>
                                                                                <value name="value">
                                                                                  <block type="random_number" id="r+PYwx7G{z3B+?o6EwVr">
                                                                                    <value name="min">
                                                                                      <block type="expression" id="w{2QU,g)JUpWAHI{Zh]?">
                                                                                        <field name="value">5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="max">
                                                                                      <block type="expression" id="yW,6+g)I9uz-yAfpD1%l">
                                                                                        <field name="value">8</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="image_shape" id="1#yuDdRz$8LO{i)HPDl^">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="^,|(=$0|ECZ0jMsr9h0f">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="bRAytrapQigehx.$hs=!">
                                                                                            <field name="value">600</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="iJ{+AkWZ^[$$@1O~=hO.;">
                                                                                            <field name="value">250</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="r)@1C.)VARZ_/vk?jh),Q">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="]WXjT9n,6Y8^HyU(BYCs">
                                                                                                <field name="value">cost${cost}.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="gE{ZNLBa%i%`fKkOC0yA">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id=".~R0z]7h@1|~~W/ExNA]B">
                                                                                                    <field name="value">${image_path}cost${cost}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="grid_shape" id="{aSVYyeZ=CkF`|JgEnAi">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_grid_dimension" id="!p}y[_gvs4mAf;l.Opcl">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="rows">
                                                                                              <block type="expression" id="3%4-FU]|g[8QIh9_s{)K">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="cols">
                                                                                              <block type="expression" id="0B]uh3,#)o%{@21.b#m%b">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_position" id="r.XaP$3GOn)M7ZRwuy]W">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="TV66jH$[D][`GPG}/JiV">
                                                                                                    <field name="value">630</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="o7h2E;I,bgK88tW+}CMg">
                                                                                                    <field name="value">250</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="O%8t^p#-|GUZPxv[hO}n">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="E1SE/s6P(iP:0ToZ^wyw">
                                                                                                        <field name="value">150</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="w|JnQW+MIWFsI7XVe9hb">
                                                                                                        <field name="value">60</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_anchor" id="9@2z1Bf`+eaB^c?ejDN}N">
                                                                                                        <field name="#prop">anchor</field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="^R6b3S`B~dc@17(R$7q?x">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="M.p{byy;p8EH@2pYm}50{">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_source" id="fR]fk|%2!_3V2|:iq{vg">
                                                                                                            <field name="#prop">cell.source</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="@19N=28g0gpYea[)xb:7Z">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_random" id="F6bPm,igU?Jx~.D+HW+s">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="random">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_show_borders" id="Z1{%#~=ByhCu::Yb$sZ3">
                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                    <field name="value">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_template" id="!)(x6[8{h)Gp:[VK]9$:">
                                                                                                                        <field name="variable">$cell</field>
                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                        <statement name="body">
                                                                                                                          <block type="text_shape" id="Ak@2T^qD1NV.GEE6xPJ+j">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="KqQ8dPO:z#$@14]njCME3">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="RM8w|FBP)A~c^rs_Me~a">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="0.{NA~9O6lH^lo3ayw@1`">
                                                                                                                                    <field name="value">$cell.centerY+3</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_contents" id="J4Gho5(Shw_}BAqY|Pyo">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="contents">
                                                                                                                                      <block type="string_value" id="NV+IV=Ax%K}9}ygph9+v">
                                                                                                                                        <field name="value">$${price}/${(type == 3 ? '100':'') + unit[type-1]}</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style" id="xGUlCCE11cg9S?jF#b#9">
                                                                                                                                        <field name="base">text</field>
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_text_style_font_size" id=";3E6Fz-jM8RI#:ACP{j=">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fontSize">
                                                                                                                                              <block type="expression" id="77N@2B?K)+b~Ehw:l/KU0">
                                                                                                                                                <field name="value">36</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_fill" id="5;x8XB^PB(}UR`t@1,F8T">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fill">
                                                                                                                                                  <block type="string_value" id="pqV86Xi4,!M{|;xMyq~%">
                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="variable" id="h]^q,[QuXB^N:%Xv?j.w">
                                                                                            <field name="name">arr</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="ET-u=v=~]Sm}Ck?q_P-c">
                                                                                                <field name="value">[]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="T`YE,j|D-6yq/s/M(jyh">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="6{i[)?98h`rV|1Ah|#VC">
                                                                                                    <field name="value">7</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="%hl@2BVT15OAX^8z@1skv=">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="FjCGf1j6-nbN3Z~mN[m|">
                                                                                                        <field name="value">i &gt;= 0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="if_then_else_block" id="x0qBdB)sqa_^zF@2.oX1X">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="lc!!B+a:mu/SpxF)!H(Z">
                                                                                                            <field name="value">i &gt; 7 - number</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="variable" id="~LSDHjiQgy%^BJS|#Y#r">
                                                                                                            <field name="name">arr[i]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="F]6CAg@2�c+MDW1_]J+">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <statement name="else">
                                                                                                          <block type="variable" id="0I^VF@2?uhdywl?m_)%Y-">
                                                                                                            <field name="name">arr[i]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="S,#~2R6hLs{C:[3u~h{g">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="J;wgG]{G}+S}])f(DRU%">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="PSJeDKRp8+eT3WtvYbMY">
                                                                                                                <field name="value">i-1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="rows">
                                                                                                              <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="cols">
                                                                                                              <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                <field name="value">4</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                    <field name="value">320</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                    <field name="value">180</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                        <field name="value">350</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                        <field name="value">180</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                <field name="value">arr</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                        <statement name="body">
                                                                                                                                          <block type="if_then_block" id="7Hv$d`EDoi=6i8^C`AsK">
                                                                                                                                            <value name="if">
                                                                                                                                              <block type="expression" id="wGmOyiz_zv(oe+-pE]yi">
                                                                                                                                                <field name="value">$cell.data != 0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="then">
                                                                                                                                              <block type="image_shape" id="StM}Vo@1Hj-r+6f!A+f[$">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id="+b]{ZD-f3$R0!_V~|Hz9">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="@13(CjiFG/!H}:xB%xJ/%">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="x%Q3IM;BH.fDyOf:JCYq">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_max_size" id="[#ZO=}Moe@2k=x5lb#V4=">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="maxWidth">
                                                                                                                                                          <block type="expression" id="-K@2)pT,f4)h2$q[J;z(m">
                                                                                                                                                            <field name="value">$cell.width</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="maxHeight">
                                                                                                                                                          <block type="expression" id="N@2hQPMdE%Y(d5dmG=uVN">
                                                                                                                                                            <field name="value">$cell.height</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="]#^^puisa]I}:?47#J67">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="KD!P-.)qESXaijF-uR_%">
                                                                                                                                                                <field name="value">${item}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="DVjTL@17aF#jrq}36/%aX">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="{Ls}YWDnLla4mI{CE.XR">
                                                                                                                                                                    <field name="value">${image_path}${item}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="list_shape" id="Jgz)3fu4Q%@2#K_9|ERhp">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_direction" id="Eo.$m!iZ3S}0fFwu@1jmY">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="dir">horizontal</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="V#4@2=dU2?B3P[_8Riy=z">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="YvZbpO-@1L([8XHcD$Xbh">
                                                                                                                        <field name="value">400</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="D@1tx-osHi[LLpd6ipmzR">
                                                                                                                        <field name="value">350</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="Y}eeBGc@1UGH%)(xr=oGF">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="arAR?W_P}{:@23Wr8D2Iq">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="be}%XS,TVjW}2diUkKR%">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_spacing" id="c@1,{cJFL~il?69}gFq,b">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="spacing">
                                                                                                                              <block type="expression" id="Fr@1sEPB=@13VG(@2s0/+8[">
                                                                                                                                <field name="value">10</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="items">
                                                                                                              <block type="list_item_shape" id="DE@2qFb4A219tBEW3`=8b">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_align" id="nVKWE(v+a2V3{ItE#2/K">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="align">middle</field>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="template">
                                                                                                                  <block type="text_shape" id="WvJH;8bK;eZX7NiBD~L{">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_contents" id="Tk:k_I$/YP7tvaTrDu=N">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="Qk5b9jv/m9Z)ZgcCzoOG">
                                                                                                                            <field name="value">Answer:</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="~6g!x@1?[oD6[7Z.-%EQ]">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="WVI5;^??@2/Y#Ktv1.Y[C">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="r+{6lsar?l2.p(]c,z7G">
                                                                                                                                    <field name="value">36</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="`ev^/=D27hp}erZ)aK)@1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id=".[Mlp)ao@2iC-HSK7AQVk">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="-P-EwwBerl-r|WWvSI@2+">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="(C-8HNkpB~nrD+muOFqc">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="cvlL=b+TKqpFlgpw5w!n">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id=");Ln.4gb8;d3,@2eiMsAA">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="list_item_shape" id="nvMS]oq/+wgOzM=P{OWT">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_align" id="{-zZsf)g7w#z@1w+p^)F8">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="align">middle</field>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="template">
                                                                                                                      <block type="image_shape" id="AM71p:EJ#[?a#@1wV-(86">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="2,,:$oZ$vfY~/E1S#RzK">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="D7?bN18S(6g;]W-${FMe">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="fV0l7N7Lh}}mygeiJ1)S">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="3F%@10f#ZzXBX.rZ4ZJi=">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="Un5nTcF?^T#$c];{}}!K">
                                                                                                                                    <field name="value">dola.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="S$H%HG3:qx%1rSwPH{6k">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="NO@2^4Z6@1/9a;Y3(ONAs9">
                                                                                                                                        <field name="value">${image_path}dola.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="list_item_shape" id="R(eI]+]%_U|$grMxXz0v">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="y/M(=^H^$5yMe(P}xZ@17">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id="wk!.X-04KS?6{n#2PVw(">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="52+36uno/Ya}[0aCp;i9">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="1^gJ-ns#!1C{^(ojk:E0">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="nt}hRU@286qLBD6-Q=qw1">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="u5zop8(]mIv,bS�vqi">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="-5llF8c-szB+_p]z{Lkm">
                                                                                                                                        <field name="value">150</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="gu?4@2{Kwi|cWOe#2YzY]">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="Y!g+sD6z:pyQY(CK9s9!">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="ycLX#4lEBo!gyKICwvD}">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                                        <field name="value">answer</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                                                <field name="value">150</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                                                <field name="value">74</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="maxLength">
                                                                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                                        <field name="value">answer.toString().length</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="tabOrder">
                                                                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                                                <field name="value">42</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_block" id="KWR6pO+0Xp?{nx=,}G?j">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="J_y=NPBCPLVO=oX`!Bcg">
                                                                                                                    <field name="value">type == 1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="partial_explanation" id="u_LFQNc}li8r9~w1zHN?" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="heXS{E=)NO?EOC(9Tj0.">
                                                                                                                        <field name="value">&lt;u&gt;Step 1: Learn the key details.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
${X} kg of ${item}.&lt;/br&gt;&lt;/br&gt;
$${price} per kilogram.&lt;/br&gt;&lt;/br&gt;

&lt;center&gt;
&lt;div style='position: relative; width: 400px;'&gt;
  &lt;table style='position: absolute;'&gt;&lt;tr&gt;
                                                                                                                        </field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="=y^7^Uos.yI^R0h,(Zms">
                                                                                                                        <field name="name">i</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="r@1=9E%v!`.VY@11f=,4o,">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="while_do_block" id="Va++ej6;|D9Xb~c_DBc;">
                                                                                                                            <value name="while">
                                                                                                                              <block type="expression" id="zt,W:;nw`4Cm_9bN=R5@1">
                                                                                                                                <field name="value">i &lt; 8</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="do">
                                                                                                                              <block type="if_then_block" id=")c{m6m^#0H_@29n[i)?8F">
                                                                                                                                <value name="if">
                                                                                                                                  <block type="expression" id="4JTvHrCAc0w_XxVk-^p`">
                                                                                                                                    <field name="value">arr[i] == 1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="then">
                                                                                                                                  <block type="partial_explanation" id="bkL00qezMTvThK=rl%S8" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="M6oQJ(VroJa`Q$MO^GBe">
                                                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}.png)' style='max-width: 70px;'/&gt;&lt;/td&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="if_then_block" id="O|xH@2}AR]OFdP]T${UcC">
                                                                                                                                    <value name="if">
                                                                                                                                      <block type="expression" id="TC?y8A`-WZ4%s?/mNuDZ">
                                                                                                                                        <field name="value">i % 4 == 3</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="then">
                                                                                                                                      <block type="partial_explanation" id="s!3]T(E:Z@1wcxpt2/cO5" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="ex9}mmK/t^[VEdsB47un">
                                                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="xT(2s$ihCFT$,!c_as$?">
                                                                                                                                        <field name="name">i</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="AbhNkT5F^uXBAMACRhs!">
                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id="FC,F|435$lrZ19$]R}uu" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="Msj!2_N37A`=M%u/ii?A">
                                                                                                                                    <field name="value">&lt;/tr&gt;
&lt;/table&gt;
&lt;img src='@1sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/&gt;
&lt;span style='position: absolute; top: 60px; left: 380px; color: black'&gt;$${price}/kg&lt;/span&gt;
&lt;/div&gt;
&lt;/center&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="end_partial_explanation" id="SRVB[OOXR26e|`/df[^1">
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id=".fJh~@2nmF(HJF|HR;zpc" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="!6BadjTA9u!8Sk(auAf@2">
                                                                                                                                            <field name="value">&lt;u&gt;Step 2: Find the price.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

Since per kilogram of ${item}, it costs $${price}. There is ${X} kg of ${item}.
&lt;/br&gt;&lt;/br&gt;
$${price} x ${X} = $${answer}
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="end_partial_explanation" id=",mRn,)d=dhyx~[nlDA.a"></block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="if_then_block" id="Su0yACfK$0gqlpJ{CPM8">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id=",6M(]uqA{cCW@2Rf`r$=W">
                                                                                                                        <field name="value">type == 2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="partial_explanation" id="U8NC#Z(-TK@1!x6{Le8yL" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="hh(@2K}sa8l.?_F,zgSDC">
                                                                                                                            <field name="value">&lt;u&gt;Step 1: Learn the key details.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
${X} litre${X &gt; 1 ? 's':''} of ${item}.&lt;/br&gt;&lt;/br&gt;
$${price} per litre.&lt;/br&gt;&lt;/br&gt;

&lt;center&gt;
&lt;div style='position: relative; width: 400px;'&gt;
  &lt;table style='position: absolute;'&gt;&lt;tr&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="v1SE6g/9oW?@16CG=Y+Hd">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="h]6Pqb!mINM|lv)?/u](">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="while_do_block" id="hr~f7!/#aWp+J]gp`mFQ">
                                                                                                                                <value name="while">
                                                                                                                                  <block type="expression" id="!KLY2tqSp3@265_UEKKN9">
                                                                                                                                    <field name="value">i &lt; 8</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="do">
                                                                                                                                  <block type="if_then_block" id="YN0o3L2HvM=#}1$GmuHr">
                                                                                                                                    <value name="if">
                                                                                                                                      <block type="expression" id="jZ}:W/bmHy$tHQxlnjk#">
                                                                                                                                        <field name="value">arr[i] == 1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="then">
                                                                                                                                      <block type="partial_explanation" id="]Is8JZWES3)0{`Ixw,dD" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="O=Rv4h}Y)EII|qnI,$">
                                                                                                                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}.png)' style='max-width: 70px;'/&gt;&lt;/td&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="if_then_block" id="57-LcA[@2[h5n+@1ZcQvF.">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="Yirsf-EeT3VgHy6/`E{(">
                                                                                                                                            <field name="value">i % 4 == 3</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="partial_explanation" id="ZeYUA-%!9YVGhJPx,{lb" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="|c[@1T:N%`[V2]Tn[rV8Q">
                                                                                                                                                <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="#1en-H$ttT]@1jaDQE{3#">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="3M62d8:3FD)(nXLEWV]3">
                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="partial_explanation" id="YyEtn?~YsZ=[@2Pz#lt8K" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="c]DV$:}O6~8G4mLnEYIE">
                                                                                                                                        <field name="value">&lt;/tr&gt;
&lt;/table&gt;
&lt;img src='@1sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/&gt;
&lt;span style='position: absolute; top: 60px; left: 380px; color: black'&gt;$${price}/litre&lt;/span&gt;
&lt;/div&gt;
&lt;/center&gt;
                                                                                                                                        </field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="end_partial_explanation" id="zmmH/M.B`SQ(j,U-S5Po">
                                                                                                                                        <next>
                                                                                                                                          <block type="partial_explanation" id="z_rn3l`,8Z+3w[$L(gA6" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="f2EnD4Srp/[Z9X#lV=WE">
                                                                                                                                                <field name="value">&lt;u&gt;Step 2: Find the price.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

Since per litre of ${item}, it costs $${price}. There is ${X} listre${X &gt; 1 ? 's':''} of ${item}.
&lt;/br&gt;&lt;/br&gt;
$${price} x ${X} = $${answer}
                                                                                                                                                </field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="end_partial_explanation" id="AsoPxWHq}`ji4gb+zl%g"></block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="if_then_block" id="UMdcA[Uq}`UTkxy2.=`u">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="w4n.j4QZu@1?FKzR!U3UZ">
                                                                                                                            <field name="value">type == 3</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="partial_explanation" id=":QrVt9_Z+dyKJ~}A{.Bg" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="c7j7)?4FwV^.p?Ea/jHX">
                                                                                                                                <field name="value">&lt;u&gt;Step 1: Learn the key details.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
${X} g of ${item}.&lt;/br&gt;&lt;/br&gt;
$${price} per 100 grams.&lt;/br&gt;&lt;/br&gt;

&lt;center&gt;
&lt;div style='position: relative; width: 400px;'&gt;
  &lt;table style='position: absolute;'&gt;&lt;tr&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="z2R_KI{rk.?jz/Od?0=#">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="T9!U@1H-1?v#OiUN4B6zX">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="6A]jjR9c0PZudn;lxSe1">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="R|.y48mV#o6]);%5n,c`">
                                                                                                                                        <field name="value">i &lt; 8</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="if_then_block" id="r7zz:F)(jVrKcogl#.Vb">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="APq-dzT~qE-Cca_X0M@24">
                                                                                                                                            <field name="value">arr[i] == 1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="partial_explanation" id="=Wb-+(y,3vq]1IM%te.k" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="uUmlT7i$sH(x]tWETBk]">
                                                                                                                                                <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}.png)' style='max-width: 70px;'/&gt;&lt;/td&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="if_then_block" id=";Z`dYxiuNZqMI]S#toC@1">
                                                                                                                                            <value name="if">
                                                                                                                                              <block type="expression" id="cb3u/fmx[xHZ(J@1WHd7j">
                                                                                                                                                <field name="value">i % 4 == 3</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="then">
                                                                                                                                              <block type="partial_explanation" id="+O9p@2_ms`27lMACb678N" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="M4.NyT!?$JH@2$XAfTn=)">
                                                                                                                                                    <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="Agl62d_fp`CB3H+O!U.U">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="xX4Ai+}JSdtlLS|e[e#7">
                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="u@2!nG|Qfi/9n#K=!P8`B" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="w8QEFWkG=p/bC${P{Nxz">
                                                                                                                                            <field name="value">&lt;/tr&gt;
&lt;/table&gt;
&lt;img src='@1sprite.src(cost${cost}.png)' style='position: absolute; top: 50px; left: 280px;'/&gt;
&lt;span style='position: absolute; top: 60px; left: 380px; color: black'&gt;$${price}/100g&lt;/span&gt;
&lt;/div&gt;
&lt;/center&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="end_partial_explanation" id="-V+po-~u4p^cnNYiqz|v">
                                                                                                                                            <next>
                                                                                                                                              <block type="partial_explanation" id="A`4s{Kvw7uE-zADGVwc]" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="^bZvK4#5@1nR}{~t6TQX@1">
                                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Find the price.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

Since per 100 grams of ${item}, it costs $${price}. There is ${X} g of ${item}.
&lt;/br&gt;&lt;/br&gt;
$${price} x ${X/100} = $${answer}
                                                                                                                                                    </field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="end_partial_explanation" id="HwtP]zlS:Z:G#hy@1E}QF"></block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */